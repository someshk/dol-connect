/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

public class NwAccessIfc {
	
	static ConnectivityManager connMgr;
	
	public static boolean isNetworkAvailable(Context appContext) {
		boolean result = false;
		
		// Context context = getApplicationContext();
		String srvcName = Context.CONNECTIVITY_SERVICE;
		
		connMgr = (ConnectivityManager)appContext.getSystemService(srvcName);
		NetworkInfo nwInfo = connMgr.getActiveNetworkInfo();
		
		if(nwInfo!=null) {
			// No network found
			result = true;
		}
		else {
			// No network found
		}
		
		return result;
	}
	
	public static boolean isNetworkConnected(Context appContext) {
		boolean result = false;
		
		// Context context = getApplicationContext();
		String srvcName = Context.CONNECTIVITY_SERVICE;
		
		connMgr = (ConnectivityManager)appContext.getSystemService(srvcName);
		NetworkInfo nwInfo = connMgr.getActiveNetworkInfo();
		
		if(nwInfo!=null) {
			// Network is available - Is it connected?
			if(nwInfo.isConnected()) {
				result = true;
			}
		}
		else {
			// No network found
		}
		
		return result;
	}
	
	public static int getNwType(Context appContext) {
		
		int result = 10;
		// Context context = getApplicationContext();
		String srvcName = Context.CONNECTIVITY_SERVICE;
		
		connMgr = (ConnectivityManager)appContext.getSystemService(srvcName);
		NetworkInfo nwInfo = connMgr.getActiveNetworkInfo();
		
		if(nwInfo!=null) {
			// Network is available - Get the Type of the network
			result =  nwInfo.getType();
		}
		return result;
	}

	public static boolean isEVDOrWiFiDataActive(Context appContext) {
		
		boolean result = false;
		int nwType;
		
		// Context context = getApplicationContext();
		String srvcName = Context.CONNECTIVITY_SERVICE;
		
		connMgr = (ConnectivityManager)appContext.getSystemService(srvcName);
		NetworkInfo nwInfo = connMgr.getActiveNetworkInfo();
		
		if(nwInfo!=null) {
			// Network is available - Get the Type of the network
			if(nwInfo.isConnected()) {
				nwType = nwInfo.getType();
				
				if(nwType==ConnectivityManager.TYPE_WIFI) {
					result = true;
				}
				else if(nwType==ConnectivityManager.TYPE_MOBILE) {
					int nwSubType = nwInfo.getSubtype();
					if(nwSubType==TelephonyManager.NETWORK_TYPE_EVDO_0 ||
							nwSubType==TelephonyManager.NETWORK_TYPE_EVDO_A) {
						result = true;
					}
				}
			}
		}
		return result;
	}
	
	public static boolean isDeviceRoaming(Context appContext) {
		boolean result = false;
		
		// Context context = getApplicationContext();
		String srvcName = Context.CONNECTIVITY_SERVICE;
		
		connMgr = (ConnectivityManager)appContext.getSystemService(srvcName);
		NetworkInfo nwInfo = connMgr.getActiveNetworkInfo();
		
		if(nwInfo!=null) {
			// Network is available - Get the Type of the network
			if(nwInfo.isRoaming()) {
				result = false;
			}
		}
		return result;
	}
	
	public static boolean isBackGroundDataAllowed() {
		return connMgr.getBackgroundDataSetting();
	}
}
