/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.utils;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobispectra.android.apps.dolconnect.R;

public class AppRater {
    private final static String APP_TITLE = "DOL Connect";
    private final static String APP_PNAME = "com.mobispectra.android.apps.dolconnect";
    
    private final static int DAYS_UNTIL_PROMPT = 30;
    private final static int LAUNCHES_UNTIL_PROMPT = 5;
    
    public static void app_launched(Context mContext) {
      
    	SharedPreferences prefs = mContext.getSharedPreferences(mContext.getResources().getString(R.string.apprater), 0);
        if (prefs.getBoolean(mContext.getResources().getString(R.string.dont_show_again), false)) { return ; }
        
        SharedPreferences.Editor editor = prefs.edit();
        
        // Increment launch counter
        long launch_count = prefs.getLong(mContext.getResources().getString(R.string.launch_count), 0) + 1;
        editor.putLong(mContext.getResources().getString(R.string.launch_count), launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong(mContext.getResources().getString(R.string.date_first_launch), 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong(mContext.getResources().getString(R.string.date_first_launch), date_firstLaunch);
        }
        
        // Show App rated only if the Network is available
        if(NwAccessIfc.isNetworkConnected(mContext)){
	        // Wait at least n days before opening
	        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
	            if (System.currentTimeMillis() >= date_firstLaunch + 
	                    (DAYS_UNTIL_PROMPT * 1000)) {
	                showRateDialog(mContext, editor);
	            }
	        }
    	}
        
        editor.commit();
    }   
    
    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setTitle(mContext.getResources().getString(R.string.rate)  +" "+ APP_TITLE);

        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setPadding(20, 20, 20, 20);
        
        TextView tv = new TextView(mContext);
        tv.setText(mContext.getResources().getString(R.string.enjoy) +" "+ APP_TITLE +" "+ mContext.getResources().getString(R.string.rate_it));
        tv.setTextColor(mContext.getResources().getColor(R.color.app_rater_text));
        tv.setWidth(300);
        tv.setPadding(4, 0, 4, 15);
        ll.addView(tv);
        
        Button b1 = new Button(mContext);
        b1.setText(mContext.getResources().getString(R.string.rate) +" "+ APP_TITLE);
        b1.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
                if (editor != null) {
                    editor.putBoolean(mContext.getResources().getString(R.string.dont_show_again), true);
                    editor.commit();
                }
                dialog.dismiss();
               
            }
        });        
        ll.addView(b1);

        Button b2 = new Button(mContext);
        b2.setText(mContext.getResources().getString(R.string.remind_me_later));
        b2.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ll.addView(b2);

        Button b3 = new Button(mContext);
        b3.setText(mContext.getResources().getString(R.string.no_thanks));
        b3.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    editor.putBoolean(mContext.getResources().getString(R.string.dont_show_again), true);
                    editor.commit();
                }
                dialog.dismiss();
            }
        });
        ll.addView(b3);

        dialog.setContentView(ll);        
        dialog.show();        
    }
}