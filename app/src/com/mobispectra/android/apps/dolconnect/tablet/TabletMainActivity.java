/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.tablet;

import java.util.ArrayList;
import java.util.Locale;

import static android.provider.BaseColumns._ID;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

import com.mobispectra.android.apps.dolconnect.AppConstants;
import com.mobispectra.android.apps.dolconnect.DOLTwitterFeedActivity;
import com.mobispectra.android.apps.dolconnect.OSHAResultFragment;
import com.mobispectra.android.apps.dolconnect.OnResultSelectedListener;
import com.mobispectra.android.apps.dolconnect.R;
import com.mobispectra.android.apps.dolconnect.SettingsActivity;
import com.mobispectra.android.apps.dolconnect.StatesProvider;
import com.mobispectra.android.apps.dolconnect.WHDResultFragment;
import com.mobispectra.android.apps.dolconnect.BaseActivity.RecentSearchAdapter;
import com.mobispectra.android.apps.dolconnect.BaseActivity.StopsListAdapter;
import com.mobispectra.android.apps.dolconnect.db.DOLDB;
import com.mobispectra.android.apps.dolconnect.db.GeographyDataDBIfc;
import com.mobispectra.android.apps.dolconnect.db.OSHADBIfc;
import com.mobispectra.android.apps.dolconnect.db.RecentSearchDBIfc;
import com.mobispectra.android.apps.dolconnect.db.WHDDBIfc;
import com.mobispectra.android.apps.dolconnect.maps.MapFragment;
import com.mobispectra.android.apps.dolconnect.maps.OSHAMapActivityJSInterface;
import com.mobispectra.android.apps.utils.AppRater;
import com.mobispectra.android.apps.utils.Eula;
import com.mobispectra.android.apps.utils.NwAccessIfc;

import static com.mobispectra.android.apps.dolconnect.BaseActivity.*;
import static com.mobispectra.android.apps.dolconnect.db.DbConstants.*;

public class TabletMainActivity extends BaseMultiPaneActivity implements OnResultSelectedListener {
	public static final int DIALOG_SEARCH_VIOLATIONS = 0;
	public static final int DIALOG_WELCOME_MESSAGE = 1;
	private static final int DIALOG_RECENT_SEARCH = 2;
	private DOLDB myDbHelper;
	private Fragment mFragment;
	private String mSearchString;
	private String mSearchBy;
	private String mCategory;
	private String mAgency;
	private boolean mDataAvailable = false;
	ArrayList<String> mAddressList = new ArrayList<String>();
	private MapFragment mMapFragment;
	private AutoCompleteTextView mAutoCompleteTextView;
	private Cursor mCursor;
	private StopsListAdapter mAdapter;
	private SimpleAdapter mSimpleAdapter;
	private RadioButton mOSHARadio;
	private RadioButton mWHDRadio;
	private Spinner mSearchBySpinner;;
	private Spinner mCategorySpinner;
	private TextView mTextView;
	private Cursor mRecentSearchCursor;
	private RecentSearchAdapter mRecentSearchAdapter;
	private final String[] FROM = { _ID, SEARCH_BY, SEARCH_STRING,
			SEARCH_CATEGORY, SEARCH_AGENCY };
	private final int[] TO = { R.id.id, R.id.search_by, R.id.search_string,
			R.id.search_category, R.id.search_agency };
	private boolean mSearch;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tablet_main);
		mTextView = (TextView) findViewById(R.id.search_textview);
	
		if (savedInstanceState == null) {
			AppRater.app_launched(this);
		}
		
		// Show End User License Agreement
    	Eula.show(this) ;
    	
		if(savedInstanceState != null){
			mDataAvailable = savedInstanceState.getBoolean(AppConstants.DATA_AVAILABLE);
		}
		
		// Create DB
		myDbHelper = new DOLDB(this);
		if (myDbHelper != null) {
			myDbHelper.getWritableDatabase();
		}
		
		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setTitle(R.string.app_name);
			// actionBar.setDisplayUseLogoEnabled(true);
		}
		
		if (!mDataAvailable) {
			mTextView.setVisibility(View.VISIBLE);
		}
		
		if (mMapFragment == null) {
			mAddressList.add("dummy");
			mMapFragment = new MapFragment(mAddressList,getString(R.string.agency_osha));
			getSupportFragmentManager().beginTransaction().add(
					R.id.fragment_container_osha_map, mMapFragment).commit();
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(AppConstants.DATA_AVAILABLE, true);
	}

	@Override
	public FragmentReplaceInfo onSubstituteFragmentForActivityLaunch(String activityClassName) {
		if (OSHAMapActivityJSInterface.class.getName().equals(activityClassName)) {
			getSupportFragmentManager().popBackStack();
			findViewById(R.id.fragment_container_osha_map).setBackgroundColor(0);
			return new FragmentReplaceInfo(MapFragment.class,
					"OSHAMap", R.id.fragment_container_osha_map);
		}
		
		return null;
	}

	@Override
	public void onNextSelected(ArrayList<String> addressList) {
		Log.d(AppConstants.TAG, "TabletMainActivity: + onNextSelected");
		mAddressList = addressList;
		mMapFragment.upDateAddressDataset(addressList);
	}

	@Override
	public void onResultListItemClicked(String address) {
		mMapFragment.popUpSelectedMarker(address);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.action_bar_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		
    	case R.id.search:
    		showDialog(DIALOG_SEARCH_VIOLATIONS);
    		break;
    	case R.id.recent_search:
    		showDialog(DIALOG_RECENT_SEARCH);
    		break;
    	case R.id.dol_faq:
    		startActivity(new Intent(TabletMainActivity.this,FAQMultiPaneActivity.class));
    		break;
    	case R.id.about_us:
    		startActivity(new Intent(TabletMainActivity.this,SettingsActivity.class));
    		break;
    	case R.id.twitter:
    		startActivity(new Intent(TabletMainActivity.this,DOLTwitterFeedActivity.class));
    	    break;
		}
    	return false;
	}
	
	
	 @Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_SEARCH_VIOLATIONS:
			LayoutInflater inflater = LayoutInflater.from(this);
			View layout = inflater.inflate(R.layout.activity_search, null);
			Button submit_button = (Button) layout.findViewById(R.id.submit_button);
			Button cancel_button = (Button) layout.findViewById(R.id.cancel_button);
			getSearchValues(layout);
			submit_button.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(mSearchBy.equals(getString(R.string.state))){
						// Get the String from AutoCompleteTextView
						mSearchString = GeographyDataDBIfc.getStateCode(mAutoCompleteTextView.getText().toString());
					}else{
						// Get the String from AutoCompleteTextView
						mSearchString = mAutoCompleteTextView.getText().toString();
					}
					
					Log.d(AppConstants.TAG,"TabletMainActivity: SearchString : "+ mSearchString);
					if (NwAccessIfc.isNetworkConnected(getApplicationContext())) {
						mSearch = true;
						startFragment(mSearchBy, mSearchString,mCategory, mAgency);
					} else {
						// Inform User that the Device is not connected to the Network
						Toast.makeText(getApplicationContext(),getString(R.string.error_no_network_coverage),
										Toast.LENGTH_LONG).show();
					}	
				}
			});
			cancel_button.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					removeDialog(DIALOG_SEARCH_VIOLATIONS);	
				}
			});
			return new AlertDialog.Builder(TabletMainActivity.this)
					.setTitle(R.string.search_violations).setView(layout)
					.setIcon(R.drawable.dol_app_icon_app_launcher).create();
		case DIALOG_WELCOME_MESSAGE:
			LayoutInflater dialoginflater = LayoutInflater.from(this);
			View dialogLayout = dialoginflater.inflate(R.layout.dialog_web_view, null);
			final WebView webView = (WebView) dialogLayout.findViewById(R.id.dialog_webview);
			webView.loadUrl("file:///android_asset/welcome.html");
			return new AlertDialog.Builder(TabletMainActivity.this).setTitle(
					R.string.welcome_title).setView(dialogLayout).setIcon(
					R.drawable.dol_app_icon_app_launcher).setPositiveButton(R.string.welcome_enjoy,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int whichButton) {
							
						}
					}).create();
		case DIALOG_RECENT_SEARCH:
			LayoutInflater recentSerachInflater = LayoutInflater.from(this);
			View recentSearchLayout = recentSerachInflater.inflate(R.layout.activity_recent_search, null);
			showRecentSearchData(recentSearchLayout);
			return new AlertDialog.Builder(TabletMainActivity.this)
					.setTitle(R.string.recent_search).setView(recentSearchLayout)
					.setIcon(R.drawable.dol_app_icon_app_launcher).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int whichButton) {
	                    	removeDialog(DIALOG_RECENT_SEARCH);
	                    }
	                }).create();
		}
		return null;
	}
	 
	/**
	 * Get the selected values from user in search dialog
	 * @param View view
	 */
	private void getSearchValues(View view) {
		mSearchBySpinner = (Spinner) view.findViewById(R.id.search_by_spinner);
		mAutoCompleteTextView = (AutoCompleteTextView) view.findViewById(R.id.search_box);
		mCategorySpinner = (Spinner) view.findViewById(R.id.category_spinner);
		mOSHARadio = (RadioButton) view.findViewById(R.id.osha_radio);
		mWHDRadio = (RadioButton) view.findViewById(R.id.whd_radio);
		mOSHARadio.setOnClickListener(agency_radio_listener);
		mWHDRadio.setOnClickListener(agency_radio_listener);

		// Array Adapter for category spinner
		ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter
				.createFromResource(this, R.array.category,
						android.R.layout.simple_spinner_item);
		categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mCategorySpinner.setAdapter(categoryAdapter);

		// Handle Spinner Item Selection events
		mCategorySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						switch (position) {
						case 0:
							mCategory = FOOD_SERVICE;
							break;
						case 1:
							mCategory = HOSPITALITY;
							break;
						case 2:
							mCategory = RETAIL;
							break;
						}
						Log.d(AppConstants.TAG, "TabletMainActivity: Category : "+ mCategory);
					}

					public void onNothingSelected(AdapterView<?> parent) {
						// showToast("From: unselected");
					}
				});

		// Array Adapter for searchBy spinner
		ArrayAdapter<CharSequence> searchByAdapter = ArrayAdapter
				.createFromResource(this, R.array.searchBy,
						android.R.layout.simple_spinner_item);
		searchByAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSearchBySpinner.setAdapter(searchByAdapter);

		// Handle Spinner Item Selection events
		mSearchBySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent,View view, int position, long id) {
						mSearchBy = (String) parent.getItemAtPosition(position);
						
						// Change the hint of AutoCompleteTextView 
						if (mSearchBy.equals(getString(R.string.state))) {
							mAutoCompleteTextView.setHint(R.string.search_by_state);
							// Set state suggestion adapter for AutoCompleteTextView
							mAutoCompleteTextView.setAdapter(mAdapter);
						} else if (mSearchBy.equals(getString(R.string.establishment_name))) {
							mAutoCompleteTextView.setHint(R.string.search_by_establishment);
							mAutoCompleteTextView.setAdapter(mSimpleAdapter);
						} else if (mSearchBy.equals(getString(R.string.city))) {
							mAutoCompleteTextView.setHint(R.string.search_by_city);
							mAutoCompleteTextView.setAdapter(mSimpleAdapter);
						}
						Log.d(AppConstants.TAG, "TabletMainActivity: SearchBy : "+ mSearchBy);
					}

					public void onNothingSelected(AdapterView<?> parent) {
						// showToast("From: unselected");
					}
				});

		CursorLoader cursorLoader = new CursorLoader(TabletMainActivity.this,
				StatesProvider.CONTENT_URI, new String[] { _ID, STATE_NAME,
						STATE_CODE }, null, null, null);
		mCursor = cursorLoader.loadInBackground();
		

		if (mCursor != null) {
			startManagingCursor(mCursor);
			// Adapter for showing suggestions
			mAdapter = new StopsListAdapter(this, mCursor);
		}

		mAutoCompleteTextView.setOnEditorActionListener(onSearch);
	}

	// OnEditorAction Listener for AutoCompleteTextView
	private TextView.OnEditorActionListener onSearch =

	new TextView.OnEditorActionListener() {
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (event == null || event.getAction() == KeyEvent.ACTION_UP) {
				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
			}
			return (true);
		}
	};
		
	// On click listener for radio button 
	private OnClickListener agency_radio_listener = new OnClickListener() {
		public void onClick(View v) {
			// Perform action on clicks
			RadioButton rb = (RadioButton) v;
			mAgency = (String) rb.getText();
			Log.d(AppConstants.TAG, "TabletMainActivity : Agency = " + mAgency);
		}
	};
	
	// On click listener for recent search in dialog
	private OnItemClickListener recent_list_click_listener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
			// Check for network connectivity
			if (NwAccessIfc.isNetworkConnected(getApplicationContext())) {
				
				// Dismiss dialog of Recent search
				removeDialog(DIALOG_RECENT_SEARCH);
				
				// Make Left Fragment text invisible
				mTextView.setVisibility(View.GONE);
				TextView searchByTextView = (TextView) v.findViewById(R.id.search_by);
				String searchBy = (String) searchByTextView.getText();
				TextView searchStringTextView = (TextView) v.findViewById(R.id.search_string);
				String searchString = (String) searchStringTextView.getText();
				TextView searchCategoryTextView = (TextView) v.findViewById(R.id.search_category);
				String category = (String) searchCategoryTextView.getText();
				TextView searchAgencyTextView = (TextView) v.findViewById(R.id.search_agency);
				String agency = (String) searchAgencyTextView.getText();

				// Delete the earlier data from DB
				if (agency.equals(getString(R.string.agency_osha))) {
					OSHADBIfc.deleteOSHAData(getApplicationContext());
				} else if (agency.equals(getString(R.string.agency_whd))) {
					WHDDBIfc.deleteWHDData(getApplicationContext());
				}
				
				// As data is coming from Recent Search,set Search variable to false 
				mSearch = false;
				startFragment(searchBy, searchString, category, agency);
			} else {
				// Inform User that the Device is not connected to the Network
				Toast.makeText(getApplicationContext(),
						getString(R.string.error_no_network_coverage),
						Toast.LENGTH_LONG).show();
			}
		}

	};
		
	/**
	 * Start the required fragment on the basis of Agency selected by the user to get the violations
	 * @param searchBy
	 * @param searchText
	 * @param category
	 * @param agency
	 */
	public void startFragment(String searchBy, String searchText,String category, String agency) {
		String filterValue = null;
		String searchOption = null;
		String method = null;
		String searchString = null;
		if (searchText == null && agency != null) {
			Toast.makeText(getApplicationContext(),
					R.string.enter_text_for_search, Toast.LENGTH_LONG).show();
		}
		if (agency == null && !(searchText == null)) {
			Toast.makeText(getApplicationContext(),
					R.string.select_agency_for_results, Toast.LENGTH_LONG)
					.show();
		}

		if (agency != null && !(searchText == null)) {
			mTextView.setVisibility(View.GONE);
			if (mSearch) {
				removeDialog(DIALOG_SEARCH_VIOLATIONS);
			}
			if (agency.equals(getString(R.string.agency_osha))) {
				OSHADBIfc.deleteOSHAData(getApplicationContext());
				searchString = searchText.toUpperCase(Locale.US).trim();
				// Get searchOption
				searchOption = getSearchOptionForOSHA(searchBy);
				filterValue = searchOption + "'"+ searchString.replaceAll("'", "''") + "'";
				method = "Compliance/" + agency + "/" + category;
				
				mFragment = new OSHAResultFragment(searchString, searchBy,
						category, agency, method, filterValue);
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragment_container_osha_results, mFragment).commit();
				
				mMapFragment = new MapFragment(mAddressList,agency);
				getSupportFragmentManager().beginTransaction().add(
					R.id.fragment_container_osha_map, mMapFragment).commit();
				
			} else if (agency.equals(getString(R.string.agency_whd))) {
				WHDDBIfc.deleteWHDData(getApplicationContext());
				searchString = searchText.trim();
				if (searchBy.equals("State")) {
					searchString = searchString.toUpperCase(Locale.US);
				}
				// Get searchOption
				searchOption = getSearchOptionForWHD(searchBy);
				filterValue = searchOption + "'"+ searchString.replaceAll("'", "''") + "'";
				method = "Compliance/" + agency + "/" + category;
				
				mFragment = new WHDResultFragment(searchString, searchBy,
						category, agency, method, filterValue);
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragment_container_osha_results, mFragment).commit();
				mMapFragment = new MapFragment(mAddressList,agency);
				getSupportFragmentManager().beginTransaction().add(
					R.id.fragment_container_osha_map, mMapFragment).commit();
			}
		
		} else if (agency == null && (searchText == null)) {
			Toast.makeText(getApplicationContext(),
					R.string.enter_text_for_search, Toast.LENGTH_LONG).show();
		}
	}
	
	/**
	 * Get the SearchOption for particular SearchBy value for OSHA
	 * 
	 * @param searchBy
	 * 
	 * @return searchOption used to form the filter value to get content from
	 *         webAPI
	 */
	public String getSearchOptionForOSHA(String searchBy) {
		String searchOption = null;
		if (searchBy.equals(getString(R.string.state))) {
			searchOption = "site_state eq ";
		} else if (searchBy.equals(getString(R.string.establishment_name))) {
			searchOption = "estab_name eq ";
		} else if (searchBy.equals(getString(R.string.city))) {
			searchOption = "site_city eq ";
		}
		return searchOption;
	}

	/**
	 * Get the SearchOption for particular SearchBy value for WHD
	 * 
	 * @param searchBy
	 * 
	 * @return searchOption used to form the filter value to get content from
	 *         webAPI
	 */
	public String getSearchOptionForWHD(String searchBy) {
		String searchOption = null;
		if (searchBy.equals(getString(R.string.state))) {
			searchOption = "st_cd eq ";
		} else if (searchBy.equals(getString(R.string.establishment_name))) {
			searchOption = "trade_nm eq ";
		} else if (searchBy.equals(getString(R.string.city))) {
			searchOption = "city_nm eq ";
		}
		return searchOption;
	}
	/**
	 * Show Recent Search Data in ListView in dialog
	 * @param View view
	 */
	private void showRecentSearchData(View view) {
		ListView listView = (ListView) view.findViewById(R.id.recent_search_listview);
		TextView noRecentSearchTextView = (TextView) view.findViewById(R.id.no_recent_search_textview);
		
		// Get the cursor of recent searches
		mRecentSearchCursor = RecentSearchDBIfc.getRecentSearches(getApplicationContext());

		// Check if Cursor is null or not
		if (mRecentSearchCursor != null) {
			startManagingCursor(mRecentSearchCursor);
			// Form the adapter
			mRecentSearchAdapter = new RecentSearchAdapter(getApplicationContext(),
					R.layout.list_item_recent_search, mRecentSearchCursor,FROM, TO);
			
			// Set adapter with list view
			listView.setAdapter(mRecentSearchAdapter);
			
			// On click listener for ListtView
			listView.setOnItemClickListener(recent_list_click_listener);
		}else{
			// Set text on No Recent Search Text View
			noRecentSearchTextView.setVisibility(View.VISIBLE);
			noRecentSearchTextView.setText(getString(R.string.no_recent_search));
		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if (mCursor != null) {
			stopManagingCursor(mCursor);
		}
		if (mRecentSearchCursor != null) {
			stopManagingCursor(mRecentSearchCursor);
		}
		if(myDbHelper != null){
			myDbHelper.close();
		}
	}
}