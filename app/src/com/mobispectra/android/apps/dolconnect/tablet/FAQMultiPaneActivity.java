/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.tablet;

import android.app.ActionBar;
import android.app.FragmentBreadCrumbs;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;

import com.mobispectra.android.apps.dolconnect.FAQSubTopicFragment;
import com.mobispectra.android.apps.dolconnect.FAQSubTopicsActivity;
import com.mobispectra.android.apps.dolconnect.FAQTopicQuestionsActivity;
import com.mobispectra.android.apps.dolconnect.FAQTopicQuestionsFragment;
import com.mobispectra.android.apps.dolconnect.R;

public class FAQMultiPaneActivity extends BaseMultiPaneActivity implements
		View.OnClickListener, FragmentManager.OnBackStackChangedListener {

	private FragmentManager mFragmentManager;
	private FragmentBreadCrumbs mFragmentBreadCrumbs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_faq_multi_pane);
		
		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setTitle(R.string.dol_faq);
		}

		mFragmentManager = getSupportFragmentManager();
		mFragmentBreadCrumbs = (FragmentBreadCrumbs) findViewById(R.id.breadcrumbs);
		mFragmentBreadCrumbs.setActivity(this);
		mFragmentManager.addOnBackStackChangedListener(this);

		updateBreadCrumb();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		ViewGroup detailContainer = (ViewGroup) findViewById(R.id.fragment_container_faq_subtopic);
		if (detailContainer != null && detailContainer.getChildCount() > 0) {
			findViewById(R.id.fragment_container_faq_subtopic)
					.setBackgroundColor(0);
		}
	}

	@Override
	public FragmentReplaceInfo onSubstituteFragmentForActivityLaunch(
			String activityClassName) {
		if (FAQSubTopicsActivity.class.getName().equals(activityClassName)) {
			getSupportFragmentManager().popBackStack();
			findViewById(R.id.fragment_container_faq_subtopic)
					.setBackgroundColor(0);
			return new FragmentReplaceInfo(FAQSubTopicFragment.class,
					"subtopic", R.id.fragment_container_faq_subtopic);
		} else if (FAQTopicQuestionsActivity.class.getName().equals(
				activityClassName)) {
			findViewById(R.id.fragment_container_faq_subtopic)
					.setBackgroundColor(0);
			return new FragmentReplaceInfo(FAQTopicQuestionsFragment.class,
					"questions", R.id.fragment_container_faq_subtopic);
		}
		return null;
	}

	@Override
	protected void onBeforeCommitReplaceFragment(FragmentManager fm,
			FragmentTransaction ft, Fragment fragment) {
		super.onBeforeCommitReplaceFragment(fm, ft, fragment);
		if (fragment instanceof FAQTopicQuestionsFragment) {
			ft.addToBackStack(null);
		} else if (fragment instanceof FAQSubTopicFragment) {
			fm.popBackStack();
		}
		updateBreadCrumb();
	}

	/**
	 * Handler for the breadcrumb parent.
	 */
	public void onClick(View view) {
		mFragmentManager.popBackStack();
	}

	public void onBackStackChanged() {
		updateBreadCrumb();
	}

	public void updateBreadCrumb() {
		final String title = getString(R.string.sub_topic);
		final String detailTitle = getString(R.string.questions);

		if (mFragmentManager.getBackStackEntryCount() >= 1) {
			mFragmentBreadCrumbs.setParentTitle(title, title, this);
			mFragmentBreadCrumbs.setTitle(detailTitle, detailTitle);
		} else {
			mFragmentBreadCrumbs.setParentTitle(null, null, null);
			mFragmentBreadCrumbs.setTitle(title, title);
		}
	}

}
