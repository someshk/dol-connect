/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect;

import com.mobispectra.android.apps.dolconnect.maps.WHDMapListItemActivityJSInterface;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class WHDDetailActivity extends BaseActivity{
	private String mTradeName;
	private String mStreetAddress;
	private String mState;
	private String mCity;
	private String mZipCode;
	private String mAddress;
	private String mFindingsStartDate;
	private String mFindingsEndDate;
	private String mFlsaViolationCount;
	private String mChildLaborViolation;
	private String mChildLaborViolationcount;
	private String mNaicCode;
	private String mNaicCodeDescription;
	private String mRepeatViolator;
	private String mFlsa15a3BwAtpAmt;
	private String mFlsaOtBwAtpAmt;
	private String mFlsaEeBwAtpCnt;
	private String mFlsaBwAtpAmt;
	private String mFlsaMwBwAtpCnt;
	private String mFlsaCmpAssdAmt;
	private String mFlsaClCmpAssdAmt;
	private double mLattitude;
	private double mLongitude;
	private TextView addressTextView;
	private TextView tradeNameTextView;
	private TextView startDateTextView;
	private TextView endDateTextView;
	private TextView flsaViolationTextView;
	private TextView childLaborTextView;
	private TextView childLaborViolationTextView;
	private TextView naicCodeTextView;
	private TextView naicCodeDescriptionTextView;
	private TextView repeatViolatorTextView;
	private TextView flsa15a3BwAtpAmtTextView;
	private TextView flsaOtBwAtpAmtTextView;
	private TextView flsaEeBwAtpCntTextView;
	private TextView flsaBwAtpAmtTextView;
	private TextView flsaMwBwAtpCntTextView;
	private TextView flsaCmpAssdAmtTextView;
	private TextView flsaClCmpAssdAmtTextView;
	private TextView mTitleBarTextView;
	private StringBuilder mShareMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_whd_detail);
		
		// Extract the Extra Data passed to this activity
		Intent intent = getIntent();
		Bundle indexBundle = intent.getExtras();
		if (indexBundle != null) {
			mTradeName = indexBundle.getString("TRADE_NAME");
			mAddress = indexBundle.getString("ADDRESS");
			mFindingsStartDate = indexBundle.getString("FINDINGS_START_DATE");
			mFindingsEndDate = indexBundle.getString("FINDINGS_END_DATE");
			mFlsaViolationCount = indexBundle.getString("FLSA_VIOLATION_COUNT");
			mChildLaborViolation = indexBundle.getString("CHILD_LABOR_VIOLATION");
			mChildLaborViolationcount  = indexBundle.getString("CHILD_LABOR_VIOLATION_COUNT");
			mNaicCode = indexBundle.getString("NAIC_CODE");
			mNaicCodeDescription = indexBundle.getString("NAIC_CODE_DESCRIPTION");
			mRepeatViolator = indexBundle.getString("REPEAT_VIOLATOR");
			mFlsa15a3BwAtpAmt = indexBundle.getString("FLSA_15a3_BW_ATP_AMT");
			mFlsaOtBwAtpAmt = indexBundle.getString("FLSA_OT_BW_ATP_AMT");
			mFlsaEeBwAtpCnt= indexBundle.getString("FLSA_EE_BW_ATP_CNT");
			mFlsaBwAtpAmt = indexBundle.getString("FLSA_BW_ATP_AMT");
			mFlsaMwBwAtpCnt  = indexBundle.getString("FLSA_MW_BW_ATP_CNT");
			mFlsaCmpAssdAmt = indexBundle.getString("FLSA_CMP_ASSD_AMT");
			mFlsaClCmpAssdAmt = indexBundle.getString("FLSA_CL_CMP_ASSD_AMT");
			mLattitude = indexBundle.getDouble("LATTITUDE");
			mLongitude = indexBundle.getDouble("LONGITUDE");
			
		}
		
		tradeNameTextView =(TextView) findViewById(R.id.trade_name);
		addressTextView =(TextView) findViewById(R.id.address);
		startDateTextView =(TextView) findViewById(R.id.findings_start_date);
		endDateTextView =(TextView) findViewById(R.id.findings_end_date);
		flsaViolationTextView =(TextView) findViewById(R.id.flsa_violation_count);
		childLaborTextView =(TextView) findViewById(R.id.child_labor_violation);
		childLaborViolationTextView =(TextView) findViewById(R.id.child_labor_violation_count);
		naicCodeTextView =(TextView) findViewById(R.id.naic_code);
		naicCodeDescriptionTextView =(TextView) findViewById(R.id.naic_code_description);
		repeatViolatorTextView =(TextView) findViewById(R.id.repeat_violator);
		flsa15a3BwAtpAmtTextView =(TextView) findViewById(R.id.flsa15a3_bw_atp_amt);
		flsaOtBwAtpAmtTextView =(TextView) findViewById(R.id.flsa_ot_bw_atp_amt);
		flsaEeBwAtpCntTextView =(TextView) findViewById(R.id.flsa_ee_bw_atp_cnt);
		flsaBwAtpAmtTextView =(TextView) findViewById(R.id.flsa_bw_atp_amt);
		flsaMwBwAtpCntTextView =(TextView) findViewById(R.id.flsa_mw_bw_atp_cnt);
		flsaCmpAssdAmtTextView =(TextView) findViewById(R.id.flsa_cmp_assd_amt);
		flsaClCmpAssdAmtTextView =(TextView) findViewById(R.id.flsa_cl_cmp_assd_amt);
		
		// Set titleBar text
		mTitleBarTextView = (TextView) findViewById(R.id.titlebar_text);
		mTitleBarTextView.setText(R.string.whd_violations_detail);
		
	
		tradeNameTextView.setText(mTradeName);
		addressTextView.setText(mAddress );
		startDateTextView.setText(getString(R.string.findings_start_date)+" "+mFindingsStartDate);
		endDateTextView.setText(getString(R.string.findings_end_date)+" "+ mFindingsEndDate);
		flsaViolationTextView.setText(getString(R.string.flsa_violation_count)+" "+mFlsaViolationCount);
		childLaborTextView.setText(getString(R.string.minors_found_employed)+" "+mChildLaborViolation);
		childLaborViolationTextView .setText(getString(R.string.child_labor_count)+" "+mChildLaborViolationcount);
		naicCodeTextView.setText(getString(R.string.naic_code)+" "+mNaicCode);
		naicCodeDescriptionTextView.setText(getString(R.string.naic_code_description)+" "+mNaicCodeDescription);
		repeatViolatorTextView .setText(getString(R.string.repeat_violator)+" "+mRepeatViolator);
		flsa15a3BwAtpAmtTextView.setText(getString(R.string.flsa15a3_bw_atp_amt)+" "+ mFlsa15a3BwAtpAmt);
		flsaOtBwAtpAmtTextView.setText(getString(R.string.flsa_ot_bw_atp_amt)+" "+mFlsaOtBwAtpAmt);
		flsaEeBwAtpCntTextView.setText(getString(R.string.flsa_ee_bw_atp_cnt)+" "+mFlsaEeBwAtpCnt);
		flsaBwAtpAmtTextView .setText(getString(R.string.flsa_bw_atp_amt)+" "+mFlsaBwAtpAmt);
		flsaMwBwAtpCntTextView.setText(getString(R.string.flsa_mw_bw_atp_cnt)+" "+ 	mFlsaMwBwAtpCnt);
		flsaCmpAssdAmtTextView .setText(getString(R.string.flsa_cmp_assd_amt)+" "+	mFlsaCmpAssdAmt );
		flsaClCmpAssdAmtTextView.setText(getString(R.string.flsa_cl_cmp_assd_amt)+" "+	mFlsaClCmpAssdAmt);
		
		mShareMessage = new StringBuilder();
		mShareMessage.append("\n" +mTradeName).append("\n" + mAddress).append("\n" + getString(R.string.findings_start_date)+" "+mFindingsStartDate)
		.append("\n"+getString(R.string.findings_end_date)+" "+ mFindingsEndDate).append("\n"+getString(R.string.flsa_violation_count)+" "+mFlsaViolationCount)
		.append("\n"+getString(R.string.minors_found_employed)+" "+mChildLaborViolation).append("\n"+getString(R.string.child_labor_count)+" "+mChildLaborViolationcount)
		.append("\n"+getString(R.string.naic_code)+" "+mNaicCode).append("\n"+getString(R.string.naic_code_description)+" "+mNaicCodeDescription)
		.append("\n"+getString(R.string.repeat_violator)+" "+mRepeatViolator).append("\n"+getString(R.string.flsa15a3_bw_atp_amt)+" "+ mFlsa15a3BwAtpAmt)
		.append("\n"+getString(R.string.flsa_ot_bw_atp_amt)+" "+mFlsaOtBwAtpAmt).append("\n"+getString(R.string.flsa_ee_bw_atp_cnt)+" "+mFlsaEeBwAtpCnt)
		.append("\n"+getString(R.string.flsa_bw_atp_amt)+" "+mFlsaBwAtpAmt).append("\n"+getString(R.string.flsa_mw_bw_atp_cnt)+" "+ 	mFlsaMwBwAtpCnt)
		.append("\n"+getString(R.string.flsa_cmp_assd_amt)+" "+	mFlsaCmpAssdAmt).append("\n"+getString(R.string.flsa_cl_cmp_assd_amt)+" "+	mFlsaClCmpAssdAmt);
	
		
	}
	
	/**
	 * Handle map click
	 * @param v
	 */
	public void onMapClick(View v){
		Intent intent = new Intent(WHDDetailActivity.this,WHDMapListItemActivityJSInterface.class);
		intent.putExtra("TRADE_NAME",mTradeName);
		intent.putExtra("FINDINGS_START_DATE",mFindingsStartDate);
		intent.putExtra("FINDINGS_END_DATE",mFindingsEndDate);
		intent.putExtra("FLSA_VIOLATION_COUNT",mFlsaViolationCount);
		intent.putExtra("CHILD_LABOR_VIOLATION",mChildLaborViolation);
		intent.putExtra("CHILD_LABOR_VIOLATION_COUNT",mChildLaborViolationcount);
		intent.putExtra("NAIC_CODE",mNaicCode);
		intent.putExtra("NAIC_CODE_DESCRIPTION",mNaicCodeDescription);
		intent.putExtra("REPEAT_VIOLATOR",mRepeatViolator);
		intent.putExtra("FLSA_15a3_BW_ATP_AMT",mFlsa15a3BwAtpAmt);
		intent.putExtra("FLSA_OT_BW_ATP_AMT",mFlsaOtBwAtpAmt);
		intent.putExtra("FLSA_EE_BW_ATP_CNT",mFlsaEeBwAtpCnt);
		intent.putExtra("FLSA_BW_ATP_AMT",mFlsaBwAtpAmt);
		intent.putExtra("FLSA_MW_BW_ATP_CNT",mFlsaMwBwAtpCnt);
		intent.putExtra("FLSA_CMP_ASSD_AMT",mFlsaCmpAssdAmt);
		intent.putExtra("FLSA_CL_CMP_ASSD_AMT",mFlsaClCmpAssdAmt);
		intent.putExtra("LATTITUDE", mLattitude);
		intent.putExtra("LONGITUDE", mLongitude );
		intent.putExtra("ADDRESS", mAddress);
		startActivity(intent);
	}
	
	/**
	 * Handle share click
	 * @param v
	 */
	public void onShareClick(View v){
		Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
		shareIntent.putExtra(Intent.EXTRA_SUBJECT,getResources().getString(R.string.share_whd_subject));
		shareIntent.putExtra(Intent.EXTRA_TEXT,String.format(getString(R.string.share_whd_message)+  mShareMessage.toString()));
		shareIntent.setType("text/plain");
		startActivity(Intent.createChooser(shareIntent,
				getString(R.string.share_whd_violation)));
	}
}
