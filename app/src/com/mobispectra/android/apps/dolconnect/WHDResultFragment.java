/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.android.maps.GeoPoint;
import com.mobispectra.android.apps.dolconnect.BaseFragmentActivity.MyDialogFragment;
import com.mobispectra.android.apps.dolconnect.api.DOLDataContext;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequest;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequestCallback;
import com.mobispectra.android.apps.dolconnect.data.WHDData;
import com.mobispectra.android.apps.dolconnect.data.WHDData.WHDItem;
import com.mobispectra.android.apps.dolconnect.db.GeographyDataDBIfc;
import com.mobispectra.android.apps.dolconnect.db.RecentSearchDBIfc;
import com.mobispectra.android.apps.dolconnect.db.WHDDBIfc;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import static com.mobispectra.android.apps.dolconnect.BaseActivity.*;

public class WHDResultFragment extends Fragment implements DOLDataRequestCallback {
	private ListView mListView;
	private WHDData mWHDData = new WHDData();
	private ArrayList<WHDItem> mWHDItemList;
	private MyAdapter myAdapter;
	ArrayList<String> mAddressList = new ArrayList<String>();
	private WHDItem mWHDItem;
	private String mSearchString;
	private String mMethod;
	private TextView mNoResultTextView;
	private TextView mSearchResultTextView;
	private String mFilterValue;
	private String mSearchBy;
	private String mCategory;
	private String mAgency;
	private ViewGroup mRootView;
	private MyDialogFragment fragment;
	private Button mNextButton;
	private Button mPrevButton;
	private int mSkipResults = 0;
	private int mShownIndex = 1;
	private int mItemIndex = 0;
	private int mResultSetCounter = 100;
	private int mDisplayedResults = 0;
	private boolean mShowNext;
	private boolean mShowPrevious;
	public OnResultSelectedListener onResultSelectedListener;
	private boolean mRunning;
	private int mTotalResults = 0;
	private boolean mTaskRunning;

	public WHDResultFragment(String searchString, String searchBy,String category,
			String agency, String method,String filterValue) {
		mSearchString = searchString;
		mSearchBy = searchBy;
		mCategory = category;
		mAgency = agency;
		mMethod = method;
		mFilterValue = filterValue;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = (ViewGroup) inflater.inflate(R.layout.activity_osha_food, null);
		mListView = (ListView) mRootView.findViewById(R.id.food_listview);
		mNoResultTextView = (TextView) mRootView.findViewById(R.id.noresult_textview);
		mSearchResultTextView = (TextView) mRootView.findViewById(R.id.search_result_textview);
		mNextButton = (Button) mRootView.findViewById(R.id.get_next_results);
		mPrevButton = (Button) mRootView.findViewById(R.id.get_previous_results);
		mListView .setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mListView .setCacheColorHint(Color.TRANSPARENT);
		
		if (getRetainInstance()) {
			displayWHDItems(mWHDData);
		}else{
			setRetainInstance(true);
			getNextResults(mResultSetCounter, mSkipResults );
		}
		// Handles list item click
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				mListView.setItemChecked(position, true);
				mWHDItem = mWHDItemList.get(position);
				Intent intent = new Intent(getActivity(),WHDDetailActivity.class);
				intent.putExtra("TRADE_NAME",mWHDItem.getTrade_nm());
				intent.putExtra("STREET_ADDRESS",mWHDItem.getStreet_addr_1_txt());
				intent.putExtra("STATE",mWHDItem.getSt_cd());
				intent.putExtra("CITY",mWHDItem.getCity_nm());
				intent.putExtra("ZIP_CODE",mWHDItem.getZip_cd());
				intent.putExtra("FINDINGS_START_DATE",mWHDItem.getFindings_start_date());
				intent.putExtra("FINDINGS_END_DATE",mWHDItem.getFindings_end_date());
				intent.putExtra("FLSA_VIOLATION_COUNT",mWHDItem.getFlsa_violtn_cnt());
				intent.putExtra("CHILD_LABOR_VIOLATION",mWHDItem.getFlsa_cl_minor_cnt());
				intent.putExtra("CHILD_LABOR_VIOLATION_COUNT",mWHDItem.getFlsa_cl_violtn_cnt());
				intent.putExtra("NAIC_CODE",mWHDItem.getNaic_cd());
				intent.putExtra("NAIC_CODE_DESCRIPTION",mWHDItem.getNaics_code_description());
				intent.putExtra("REPEAT_VIOLATOR",mWHDItem.getFlsa_repeat_violator());
				intent.putExtra("FLSA_15a3_BW_ATP_AMT",mWHDItem.getFlsa_15a3_bw_atp_amt());
				intent.putExtra("FLSA_OT_BW_ATP_AMT",mWHDItem.getFlsa_ot_bw_atp_am());
				intent.putExtra("FLSA_EE_BW_ATP_CNT",mWHDItem.getFlsa_ee_atp_cnt());
				intent.putExtra("FLSA_BW_ATP_AMT",mWHDItem.getFlsa_bw_atp_amt());
				intent.putExtra("FLSA_MW_BW_ATP_CNT",mWHDItem.getFlsa_mw_bw_atp_amt());
				intent.putExtra("FLSA_CMP_ASSD_AMT",mWHDItem.getFlsa_cmp_assd_amt());
				intent.putExtra("FLSA_CL_CMP_ASSD_AMT",mWHDItem.getFlsa_cl_cmp_assd_amt());
			    //	startActivity(intent);
				String address = mWHDItem.getStreet_addr_1_txt() + "," + mWHDItem.getCity_nm()
				+ "," + mWHDItem.getSt_cd() + "," + mWHDItem.getZip_cd();
				Log.d(AppConstants.TAG,"WHDResultFragment: Address"+address);
				onResultSelectedListener.onResultListItemClicked(address);
			}
		});
		
	    // OnClick Listener for next button
		mNextButton.setOnClickListener(new OnClickListener() {		

			@Override
			public void onClick(View v) {	

				Log.d(AppConstants.TAG,"WHDResultFragment : + onNextClick : Index = " + mShownIndex);
				mShowPrevious = true;
				mWHDData = WHDDBIfc.getWHDItemsFromDB(getActivity(), (mShownIndex + mDisplayedResults));
				
				// Display data
				if (mWHDData != null) {
					displayWHDItems(mWHDData);
					handleButtonVisibilty();
					mShownIndex = mShownIndex + mDisplayedResults;
				} else {
					getNextResults(mResultSetCounter, mSkipResults);
				}
				// Disable the next button if Async Task is in progress
				if (mTaskRunning) {
					mNextButton.setEnabled(false);
				}
				Log.d(AppConstants.TAG,"WHDResultFragment : - onNextClick : Index = " + mShownIndex);
			}
		});
		
		// OnClick Listener for previous button
		mPrevButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(AppConstants.TAG,"WHDResultFragment : + onPrevClick : Index = " + mShownIndex);
				mShownIndex = mShownIndex - mDisplayedResults;
				if(mShownIndex == 1){
					mShowPrevious = false;
				}
				mWHDData = WHDDBIfc.getWHDItemsFromDB(getActivity(), mShownIndex);
				
				// Display data
				if (mWHDData != null) {
					displayWHDItems(mWHDData);
					handleButtonVisibilty();
				}
				Log.d(AppConstants.TAG,"WHDResultFragment : - onPrevClick : Index = " + mShownIndex);
			}
		});
		return mRootView;
	}
	
	private void getNextResults(int results,int skip){
		Log.d(AppConstants.TAG,"WHDResultFragment : + getNextResults");
		Log.d(AppConstants.TAG,"WHDResultFragment : getNextResults : SkipValue = " + skip);
		// Instantiate context object
		DOLDataContext context = new DOLDataContext(API_KEY, SHARED_SECRET,API_HOST, API_URI);

		// Instantiate new request object. Pass the context var that contains all the API key info
		// Set this as the callback responder
		DOLDataRequest request = new DOLDataRequest(this, context);

		// API method to call
		// String method = "Compliance/WHD/foodService";

		// Hashmap to store the arguments
		HashMap<String, String> args = new HashMap<String, String>(3);

		// Populate the args into the HashMap
		args.put("top", Integer.toString(results));
		args.put("select","trade_nm,street_addr_1_txt,city_nm,st_cd,zip_cd,naic_cd,"
					+ "naics_code_description,findings_start_date,findings_end_date,"
					+ "flsa_violtn_cnt,flsa_repeat_violator,flsa_bw_atp_amt,flsa_ee_atp_cnt,"
					+ "flsa_mw_bw_atp_amt,flsa_ot_bw_atp_amt,flsa_15a3_bw_atp_amt,flsa_cmp_assd_amt,"
					+ "flsa_cl_violtn_cnt,flsa_cl_minor_cnt,flsa_cl_cmp_assd_amt");
		args.put("filter",mFilterValue);
		args.put("skip", Integer.toString(skip));

		// Display progress bar dialog
		fragment = MyDialogFragment.newInstance(PROGRESS_BAR_DIALOG_ID);
		fragment.show(getFragmentManager(), "AlertDialog");
		
		// Call the API method
		request.callAPIMethod(mMethod, args);
		Log.d(AppConstants.TAG,"WHDResultFragment : - getNextResults");
	}
	
	// Callback method called when error occurs
	public void DOLDataErrorCallback(String error) {
		// Dismiss progress bar dialog
		if (fragment.isResumed() && fragment.isAdded()) {
			fragment.dismiss();
		}
		// Show error on dialog
		AlertDialog alertDialog;
		alertDialog = new AlertDialog.Builder(getActivity()).create();
		alertDialog.setTitle("Error");
		alertDialog.setMessage(error);
		alertDialog.show();
	}

	// Callback method called when results are returned and parsed
	@SuppressWarnings("unchecked")
	public void DOLDataResultsCallback(List<Map<String, String>> results) {
		Log.d(AppConstants.TAG,"WHDResultFrgament : DOLDataRequestCallback : No. of Results = " + results.size());
		if (results.size() != 0) {
			String state = null;
			String category = null;
			mTotalResults = mTotalResults + results.size();
			
			if (mCategory.equals(BaseActivity.FOOD_SERVICE)) {
				category = getString(R.string.food_service);
			} else if (mCategory.equals(BaseActivity.HOSPITALITY)) {
				category = getString(R.string.hospitality);
			} else if (mCategory.equals(BaseActivity.RETAIL)) {
				category = getString(R.string.retail);
			}
			
			// If searchBy is state then display State Name
			if(mSearchBy.equals(getString(R.string.state))){
				state = GeographyDataDBIfc.getStateName(mSearchString);
				// Set text for Result text view
				mSearchResultTextView.setText(mAgency + " " + category + " "+ getString(R.string.violations) 
						+ " "+ getString(R.string.violations_for) + " " + state);
			} else {
				// Set text for Result text view
				mSearchResultTextView.setText(mAgency + " " + category + " "+ getString(R.string.violations) + " "
						+ getString(R.string.violations_for) + " "+ mSearchString);
			}
			
			// add recent search data to DB
			RecentSearchDBIfc.addRecentSerachData(getActivity(),
					mSearchBy, mSearchString, mCategory, mAgency);
			
			mTaskRunning = true;
			// Start the AsyncTask to process the received data
			new ProcessData().execute(results);
		} else if (mNextButton.isShown()) {
			// If no results are available to show and next button is showing then make it invisible
			mNextButton.setVisibility(View.INVISIBLE);
			
			// Dismiss progress bar dialog
			if (fragment.isResumed() && fragment.isAdded()) {
				try {
					fragment.dismiss();
				} catch (Exception e) {
					Log.e(AppConstants.TAG, "WHDResultFragment : DOLDataRequestCallback : Exception in dialog : "+ e);
				}
			}
			
			// Display no result dialog
			fragment = MyDialogFragment.newInstance(NO_RESULT_DIALOG);
			fragment.show(getFragmentManager(), "No Result Dialog");
		} else {
			// Dismiss progress bar dialog
			if (fragment.isResumed() && fragment.isAdded()) {
				try {
					fragment.dismiss();
				} catch (Exception e) {
					Log.e(AppConstants.TAG, "WHDResultFragment : DOLDataRequestCallback : Exception in dialog : "+ e);
				}
			}
			Toast.makeText(getActivity(), getString(R.string.no_result_warning), Toast.LENGTH_LONG).show();
			mNoResultTextView.setVisibility(View.VISIBLE);
			mNoResultTextView.setText(R.string.no_result_warning);
		}

	}

	// Async task to do the geoCoding and insert data in DB
	private class ProcessData extends AsyncTask<List<Map<String, String>>, WHDData, Void> {
		WHDData whdData;
		
		@Override
		protected Void doInBackground(List<Map<String, String>>... results) {
			String address = null;
			whdData = new WHDData();
			Geocoder geoCoder = null;
			if (getActivity() != null) {
				geoCoder = new Geocoder(getActivity(), Locale.getDefault());
			}
			
			// Iterate thru List of results. Add each field to the display List
			for (Map<String, String> m : results[0]) {
				Log.d(AppConstants.TAG," ProcessData : doInBackground :Running  = " + mRunning);
				if (mRunning) {
					break;
				} else {
					mItemIndex++;
					GeoPoint point = null;
					address = m.get("street_addr_1_txt") + ","+ m.get("city_nm") + " ," 
							+ m.get("st_cd") + " ,"+ m.get("zip_cd");
					try {
						Log.d(AppConstants.TAG,"WHDResultFrgament : ProcessData : Address : "+ address);

						// Use GeoCoding to get the GeoPoints for every Address
						List<Address> addresses = geoCoder.getFromLocationName(address, 5);
						if (addresses.size() > 0) {
							point = new GeoPoint((int) (addresses.get(0).getLatitude() * 1E6), 
									(int) (addresses.get(0).getLongitude() * 1E6));

							Log.d(AppConstants.TAG,"WHDResultFrgament : ProcessData : Point : "+ point);
						} else {
							Log.e(AppConstants.TAG,"WHDResultFrgament : ProcessData : "
										+ "Unable to get the GeoPoints for = "+ address);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					// Create WHDItem object
					mWHDItem = whdData.new WHDItem(mItemIndex, m.get("trade_nm"), m.get("street_addr_1_txt")
							, m.get("city_nm"), m.get("st_cd"), m.get("zip_cd"), m.get("naic_cd")
							, m.get("naics_code_description"),getReadableDate(m.get("findings_start_date")),
							getReadableDate(m.get("findings_end_date")), m.get("flsa_violtn_cnt")
							, m.get("flsa_repeat_violator"), m.get("flsa_bw_atp_amt"), m.get("flsa_ee_atp_cnt")
							, m.get("flsa_mw_bw_atp_amt"), m.get("flsa_ot_bw_atp_amt"), m.get("flsa_15a3_bw_atp_amt")
							, m.get("flsa_cmp_assd_amt"), m.get("flsa_cl_violtn_cnt"), m.get("flsa_cl_minor_cnt")
							, m.get("flsa_cl_cmp_assd_amt"), point);
					

					// Add the WHDItem to WHDData object for maintaining the ArrayList
					whdData.addWHDData(mWHDItem);

					if (mItemIndex % 10 == 0) {
						if (mRunning) {
							break;
						} else {
							if (getActivity() != null) {
								// Add data to the DB
								WHDDBIfc.addWHDData(getActivity(), whdData);
								publishProgress(whdData);
								whdData = new WHDData();
							}
						}

					}
				}
			}
			if (getActivity() != null) {
				if (mTotalResults % 10 != 0) {
					// Add data to the DB
					WHDDBIfc.addWHDData(getActivity(), whdData);
					publishProgress(whdData);
				}
			}
			return null;

		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			Log.d(AppConstants.TAG,"ProcessData : OnCancelled()");
			mRunning = true;
			
			// Dismiss progress bar dialog
			if (fragment.isResumed() && fragment.isAdded()) {
				try {
					fragment.dismiss();
				} catch (Exception e) {
					Log.e(AppConstants.TAG, "ProcessData: onCancelled : Exception in dialog : "+ e);
				}
			}
			
			
		}
		
		@Override
		protected synchronized void onProgressUpdate(WHDData... values) {
			super.onProgressUpdate(values);
			// Dismiss progress bar dialog
			if (fragment.isResumed() && fragment.isAdded()) {
				try {
					fragment.dismiss();
				} catch (Exception e) {
					Log.e(AppConstants.TAG, "ProcessData: onProgressUpdate : Exception in dialog : "+ e);
				}
			}
			if (mItemIndex <= (11 + mSkipResults)) {
				mNextButton.setVisibility(View.INVISIBLE);
				// Display data
				if (values[0] != null) {
					displayWHDItems(values[0]);
				}
				mShowNext = true;
			} else if(mItemIndex > (11 + mSkipResults)){
				mNextButton.setVisibility(View.VISIBLE);
			}
			mNextButton.setEnabled(true);
			mListView.setTextFilterEnabled(true);
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			// False value indicate that AsyncTask is over and not running now
			mTaskRunning = false;
			handleButtonVisibilty();
			mSkipResults = mSkipResults + mResultSetCounter;
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			onResultSelectedListener = (OnResultSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement  onOSHAResultSelectedListener");
		}
	}
	
	private class MyAdapter extends ArrayAdapter<WHDItem> {
		private LayoutInflater mLayoutInflater;

		public MyAdapter(Context context, int textViewResourceId,
				ArrayList<WHDItem> mWHDItemList) {
			super(context, textViewResourceId);
			mLayoutInflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = mLayoutInflater.inflate(R.layout.list_item_whd,parent, false);

				holder = new ViewHolder();
				holder.index = (TextView) convertView.findViewById(R.id.index);
				holder.trade_name = (TextView) convertView.findViewById(R.id.trade_name);
				holder.trade_address = (TextView) convertView.findViewById(R.id.trade_address);
				holder.finding_dates = (TextView) convertView.findViewById(R.id.finding_dates);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			WHDItem mWHDItem = getItem(position);
			holder.trade_name.setText(mWHDItem.getTrade_nm());
			holder.trade_address.setText(mWHDItem.getStreet_addr_1_txt() + " "
					+ mWHDItem.getCity_nm() + " " + mWHDItem.getSt_cd() + " "
					+ mWHDItem.getZip_cd());
			holder.finding_dates.setText(mWHDItem.getFindings_start_date()
					+ " To " + mWHDItem.getFindings_end_date());
			holder.index.setText(Integer.toString(mWHDItem.getIndex()));
			return convertView;
		}

	}

	private static class ViewHolder {
		TextView index;
		TextView trade_name;
		TextView trade_address;
		TextView finding_dates;
	}

	// Display the data in array adapter
	public synchronized void displayWHDItems(WHDData whaData) {
		Log.d(AppConstants.TAG, "WHDResultFrgament : + displayWHDItems");
		mWHDData = whaData;
		mAddressList = new ArrayList<String>();
		String address = null;

		if (mWHDData != null) {
			mWHDItemList = mWHDData.getWHDData();
		}
		if (mWHDItemList != null && mWHDItemList.isEmpty() == false) {
			mDisplayedResults = mWHDItemList.size();
			if (myAdapter == null && getActivity() != null) {
				myAdapter = new MyAdapter(getActivity(),
						R.layout.list_item_osha, mWHDItemList);
			}
			myAdapter.clear();
			for (WHDItem item : mWHDItemList) {
				myAdapter.add(item);
				address = item.getStreet_addr_1_txt() + "," + item.getCity_nm()
						+ "," + item.getSt_cd() + "," + item.getZip_cd();
				
				if (item.getPoint() != null) {
					// Add address to arrayList
					mAddressList.add(address);
				}
				// HashMap to store Address as key and WHDItem as value
				WHDAddressMap.put(address, item);
			}
			if (myAdapter.isEmpty()) {
				Toast.makeText(getActivity(), R.string.fatal_error, Toast.LENGTH_SHORT).show();
			} else {
				mListView.setAdapter(myAdapter);
			}
		}
		// Inform the map fragment of changed data
		onResultSelectedListener.onNextSelected(mAddressList);
		Log.d(AppConstants.TAG, "WHDResultFrgament : - displayWHDItems");
	}
	
	/** 
	 * Handle visibility of next and previous button on the basis of results
	 */
	private void handleButtonVisibilty() {
		if (mShowNext && mDisplayedResults == 10) {
			mNextButton.setEnabled(true);
			mNextButton.setVisibility(View.VISIBLE);
		} else {
			mNextButton.setVisibility(View.INVISIBLE);
		}
		if (mShowPrevious) {
			mPrevButton.setVisibility(View.VISIBLE);
		} else {
			mPrevButton.setVisibility(View.INVISIBLE);
		}

	}
	
	@Override
	public synchronized void onPause() {
		super.onPause();
		Log.d(AppConstants.TAG,"WHDResultFragment : + onPause()");
		ProcessData processData = new ProcessData();
		if (processData.getStatus() != AsyncTask.Status.FINISHED) {
			processData.cancel(true);
		}
	}
}
