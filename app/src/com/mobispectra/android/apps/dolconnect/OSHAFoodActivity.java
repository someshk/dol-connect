/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.google.android.maps.GeoPoint;
import com.mobispectra.android.apps.dolconnect.api.DOLDataContext;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequest;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequestCallback;
import com.mobispectra.android.apps.dolconnect.data.OSHAData;
import com.mobispectra.android.apps.dolconnect.data.OSHAData.OSHAItem;
import com.mobispectra.android.apps.dolconnect.db.OSHADBIfc;
import com.mobispectra.android.apps.dolconnect.db.RecentSearchDBIfc;

public class OSHAFoodActivity extends BaseActivity implements DOLDataRequestCallback {
	private ListView mListView;
	private ArrayList<OSHAItem> mOSHAItemList;
	private OSHAData mOshaData;
	private MyAdapter myAdapter;
	public static ArrayList<String> mAddressList;
	private OSHAItem mOSHAItem;
	private String mSearchString;
	private String mMethod;
	private TextView mNoResultTextView;
	private String mFilterValue;
	private String mSearchBy;
	private String mCategory;
	private String mAgency;
	private Button mNextButton;
	private Button mPrevButton;
	private int mSkipResults = 0;
	private int mShownIndex = 1;
	private int mItemIndex = 0;
	private int mResultSetCounter = 100;
	private int mDisplayedResults = 0;
	private boolean mShowNext;
	private boolean mShowPrevious;
	private boolean mRunning;
	private int mTotalResults = 0;
	private boolean mTaskRunning;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Set layout for activity
		setContentView(R.layout.activity_osha_food);
		
		mListView = (ListView) findViewById(R.id.food_listview);
		mNoResultTextView = (TextView) findViewById(R.id.noresult_textview);
		mNextButton = (Button) findViewById(R.id.get_next_results);
		mPrevButton = (Button) findViewById(R.id.get_previous_results);
		
		if (savedInstanceState != null) {
			mItemIndex = savedInstanceState.getInt("ItemIndex");
			mShownIndex = savedInstanceState.getInt("ShownIndex");
			mSkipResults = savedInstanceState.getInt("SkipValue");
			mResultSetCounter = savedInstanceState.getInt("ResultSetCounter");
			mDisplayedResults = savedInstanceState.getInt("displayedResults");
			mTotalResults = savedInstanceState.getInt("TotalResults");
			mShowNext = savedInstanceState.getBoolean("NextButton");
			mShowPrevious = savedInstanceState.getBoolean("PreviousButton");
			mRunning = savedInstanceState.getBoolean("Running");
			mTaskRunning = savedInstanceState.getBoolean("TaskRunning");
		}
		
		// Extract the Extra Data passed to this activity
		Intent intent = getIntent();
		Bundle indexBundle = intent.getExtras();
		if (indexBundle != null) {
			mSearchString = indexBundle.getString(AppConstants.SEARCH_STRING);
			Log.d(AppConstants.TAG, "OSHAFoodActivity: SearchString: "+mSearchString);
			mSearchBy = indexBundle.getString(AppConstants.SEARCH_BY);
			Log.d(AppConstants.TAG, "OSHAFoodActivity: SearchBy: "+mSearchBy);
			mCategory = indexBundle.getString(AppConstants.SEARCH_CATEGORY);
			Log.d(AppConstants.TAG, "OSHAFoodActivity: SearchCategory: "+mCategory);
			mAgency = indexBundle.getString(AppConstants.SEARCH_AGENCY);
			Log.d(AppConstants.TAG, "OSHAFoodActivity: SearchAgency: "+mAgency);
			
			// API method to call
			mMethod = indexBundle.getString(AppConstants.METHOD);
			Log.d(AppConstants.TAG, "OSHAFoodActivity: Method: "+mMethod);
			mFilterValue = indexBundle.getString(AppConstants.FILTER_VALUE);
			Log.d(AppConstants.TAG, "OSHAFoodActivity: FilterValue: "+ mFilterValue);
		}
		final Object data = getLastNonConfigurationInstance();

		if (data != null) {
			// The activity is starting after screen rotation
			mOshaData = (OSHAData) data;
			displayOSHAItems(mOshaData);
			handleButtonVisibilty();
		} else {
			getNextResults(mResultSetCounter, mSkipResults);
		}

		// Handles list item click
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				mOSHAItem = mOSHAItemList.get(position);
				double lattitude = 0;
			    double longitude = 0;
				if (mOSHAItem.getPoint() != null) {
					 lattitude = mOSHAItem.getPoint().getLatitudeE6() / 1E6;
					 longitude = mOSHAItem.getPoint().getLongitudeE6() / 1E6;
				}
				Intent intent = new Intent(OSHAFoodActivity.this,OSHADetailActivity.class);
				intent.putExtra("NAME",mOSHAItem.getName());
				intent.putExtra("SITE_ADDRESS",mOSHAItem.getAddress());
				intent.putExtra("STATE",mOSHAItem.getState());
				intent.putExtra("CITY",mOSHAItem.getCity());
				intent.putExtra("ZIP_CODE",mOSHAItem.getZipCode());
				intent.putExtra("NAICS_CODE",mOSHAItem.getNaics_code(getApplicationContext()));
				intent.putExtra("PENALTY",mOSHAItem.getPenalty(getApplicationContext()));
				intent.putExtra("OPEN_DATE",mOSHAItem.getOpen_date(getApplicationContext()));
				intent.putExtra("OSHA_VIOLATION_INDICATOR",mOSHAItem.getOsha_violation_indicator(getApplicationContext()));
				intent.putExtra("TOTAL_VIOLATIONS",mOSHAItem.getTotal_violations(getApplicationContext()));
				intent.putExtra("SERIOUS_VIOLATIONS",mOSHAItem.getSerious_violations(getApplicationContext()));
				intent.putExtra("INSPECTION_TYPE",mOSHAItem.getInsp_type(getApplicationContext()));
				intent.putExtra("LOAD_DATE",mOSHAItem.getLoad_dt(getApplicationContext()));
				intent.putExtra("LATTITUDE", lattitude );
				intent.putExtra("LONGITUDE", longitude );
				String address = mOSHAItem.getAddress() + "," + mOSHAItem.getCity() + ","
				+ mOSHAItem.getState() + "," + mOSHAItem.getZipCode();
				intent.putExtra("ADDRESS",address);
				startActivity(intent);
			}
		});
		
		// OnClick Listener for get more results button
		mNextButton.setOnClickListener(new OnClickListener() {		

			@Override
			public void onClick(View v) {
				
				Log.d(AppConstants.TAG,"OSHAFoodActivity : + onNextClick : Index = " + mShownIndex);
				mShowPrevious = true;
				mOshaData = OSHADBIfc.getOSHAItemsFromDB(getApplicationContext(), (mShownIndex + mDisplayedResults));
				
				// Display data
				if (mOshaData != null) {
					displayOSHAItems(mOshaData);
					handleButtonVisibilty();
					mShownIndex = mShownIndex + mDisplayedResults;
				} else {
					getNextResults(mResultSetCounter, mSkipResults);
				}
				// Disable the next button if Async Task is in progress
				if (mTaskRunning) {
					mNextButton.setEnabled(false);
				}
				Log.d(AppConstants.TAG,"OSHAFoodActivity : - onNextClick : Index = " + mShownIndex);
			}
		});
		
		
		mPrevButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(AppConstants.TAG,"OSHAFoodActivity : + onPrevClick : Index = " + mShownIndex);
				mShownIndex = mShownIndex - mDisplayedResults;
				if(mShownIndex == 1){
					mShowPrevious = false;
				}
				mOshaData = OSHADBIfc.getOSHAItemsFromDB(getApplicationContext(), mShownIndex);
				
				// Display data
				if (mOshaData != null) {
					displayOSHAItems(mOshaData);
					handleButtonVisibilty();
				}
				Log.d(AppConstants.TAG,"OSHAFoodActivity : - onPrevClick : Index = " + mShownIndex);
			}
		});
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		final OSHAData oSHAData = mOshaData;
		return oSHAData;
	}

	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("ItemIndex",mItemIndex);
		outState.putInt("ShownIndex",mShownIndex);
		outState.putInt("SkipValue",mSkipResults);
		outState.putInt("ResultSetCounter", mResultSetCounter);
		outState.putInt("displayedResults", mDisplayedResults);
		outState.putInt("TotalResults", mTotalResults);
		outState.putBoolean("NextButton", mShowNext);
		outState.putBoolean("PreviousButton", mShowPrevious);
		outState.putBoolean("Running", mRunning);
		outState.putBoolean("TaskRunning", mTaskRunning);
	}

	private void getNextResults(int results,int skip){
		Log.d(AppConstants.TAG,"OSHAFoodActivity : + getNextResults");
		Log.d(AppConstants.TAG,"OSHAFoodActivity : getNextResults : SkipValue = " + skip);
		// Instantiate context object
		DOLDataContext context = new DOLDataContext(API_KEY, SHARED_SECRET,API_HOST, API_URI);

		// Instantiate new request object. Pass the context var that contains all the API key info
		// Set this as the callback responder
		DOLDataRequest request = new DOLDataRequest(this, context);

		// API method to call
		// String method = "Compliance/OSHA/foodService";

		// Hashmap to store the arguments
		HashMap<String, String> args = new HashMap<String, String>(4);

		// Populate the args into the HashMap
		args.put("top", Integer.toString(results));
		args.put("select","activity_nr,estab_name,site_address,site_city,site_state,site_zip,naics_code," +
				"insp_type,open_date,total_current_penalty,osha_violation_indicator,serious_violations," +
				"total_violations,load_dt");
		args.put("filter",mFilterValue);
		args.put("skip", Integer.toString(skip));

		// Display progress bar dialog
		showDialog(PROGRESS_BAR_DIALOG_ID);
		
		// Call the API method
		request.callAPIMethod(mMethod, args);
		Log.d(AppConstants.TAG,"OSHAFoodActivity : - getNextResults");
	}
	
	
	// Callback method called when error occurs
	public void DOLDataErrorCallback(String error) {
		// Dismiss progress bar dialog
		removeDialog(PROGRESS_BAR_DIALOG_ID);
		// Show error on dialog
		AlertDialog alertDialog;
		alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Error");
		alertDialog.setMessage(error);
		alertDialog.show();
	}

	// Callback method called when results are returned and parsed
	@SuppressWarnings("unchecked")
	public void DOLDataResultsCallback(List<Map<String, String>> results) {
		Log.d(AppConstants.TAG,"OSHAFoodActivity : DOLDataResultsCallback : No. of Results : " + results.size());
		if (results.size() != 0) {
			mTotalResults = mTotalResults + results.size();
			// add recent search data to DB
			RecentSearchDBIfc.addRecentSerachData(getApplicationContext(),
					mSearchBy, mSearchString, mCategory, mAgency);
			
			mTaskRunning = true;
			// Start the AsyncTask to process the received data
			new ProcessData().execute(results);
		} else if (mNextButton.isShown()) {
			// If no results are available to show and next button is showing then make it invisible
			mNextButton.setVisibility(View.INVISIBLE);
			
			// Dismiss progress bar dialog
			removeDialog(PROGRESS_BAR_DIALOG_ID);
			
			// Show dialog of no results available to show
			showDialog(NO_RESULT_DIALOG);
		} else {
			// Dismiss progress bar dialog
			removeDialog(PROGRESS_BAR_DIALOG_ID);
			Toast.makeText(getApplicationContext(), getString(R.string.no_result_warning), Toast.LENGTH_LONG).show();
			mNoResultTextView.setVisibility(View.VISIBLE);
			mNoResultTextView.setText(R.string.no_result_warning);
		}

	}

	// Async task to do the geoCoding and insert data in DB
	private class ProcessData extends AsyncTask<List<Map<String, String>>, OSHAData, Void> {
		OSHAData oshaData;
		
		@Override
		protected Void doInBackground(List<Map<String, String>>... results) {
			String address = null;
			oshaData = new OSHAData();
			Geocoder geoCoder = null;

			geoCoder = new Geocoder(getApplicationContext(), Locale.getDefault());
			
			// Iterate thru List of results. Add each field to the data structure of OSHAData
			for (Map<String, String> m : results[0]) {
				Log.d(AppConstants.TAG," ProcessData : doInBackground :Running  = " + mRunning);
				if (mRunning) {
					break;
				} else {
					mItemIndex++;
					GeoPoint point = null;
					address = m.get("site_address") + "," + m.get("site_city")
							+ " ," + m.get("site_state") + " ,"+ m.get("site_zip");
					try {
						Log.d(AppConstants.TAG,"OSHAFoodActivity : ProcessData : Address : "+ address);
						// Use GeoCoding to get the GeoPoints for every Address
						List<Address> addresses = geoCoder.getFromLocationName(address, 5);
						if (addresses.size() > 0) {
							point = new GeoPoint((int) (addresses.get(0).getLatitude() * 1E6),
									(int) (addresses.get(0).getLongitude() * 1E6));

							Log.d(AppConstants.TAG,"OSHAFoodActivity : ProcessData : Point : "+ point);
						} else {
							Log.e(AppConstants.TAG,"OSHAFoodActivity : ProcessData : "
											+ "Unable to get the GeoPoints for = "+ address);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}

					// Initialise the OSHAItem Constructor
					mOSHAItem = oshaData.new OSHAItem(mItemIndex, m.get("activity_nr"), m.get("estab_name")
							, m.get("site_address"), m.get("site_city"), m.get("site_state"), m.get("site_zip")
							, m.get("naics_code"), m.get("insp_type"),getReadableDate(m.get("open_date"))
							, m.get("total_current_penalty"), m.get("osha_violation_indicator")
							, m.get("serious_violations"), m.get("total_violations"), getReadableDate(m.get("load_dt")), point);
					
					oshaData.addData(mOSHAItem);


					if (mItemIndex % 10 == 0) {
						if (mRunning) {
							break;
						} else {
								// Add data to the DB
								OSHADBIfc.addOSHAData(getApplicationContext(), oshaData);
								publishProgress(oshaData);
								oshaData = new OSHAData();
						}
					}

				}
			}
			if (mTotalResults % 10 != 0) {
				// Add data to the DB
				OSHADBIfc.addOSHAData(getApplicationContext(), oshaData);
				publishProgress(oshaData);
			}

			return null;

		}
		
		
		@Override
		protected void onCancelled() {
			super.onCancelled();
			Log.d(AppConstants.TAG, "ProcessData : OnCancelled()");
			mRunning = true;

			// Dismiss progress bar dialog
			removeDialog(PROGRESS_BAR_DIALOG_ID);
		}


		@Override
		protected synchronized void onProgressUpdate(OSHAData... values) {
			super.onProgressUpdate(values);
			Log.d(AppConstants.TAG,"OSHAFoodActivity : + onProgressUpdate");
			Log.d(AppConstants.TAG,"onProgressUpdate : mItemIndex = " + mItemIndex);
			Log.d(AppConstants.TAG,"onProgressUpdate : mSkipResults = " + mSkipResults);
			
			// Dismiss progress bar dialog
			removeDialog(PROGRESS_BAR_DIALOG_ID);	
			
			// Display first ten results of the result set in the listview
			if (mItemIndex <= (11 + mSkipResults)) {
				mNextButton.setVisibility(View.INVISIBLE);
				// Display data
				if (values[0] != null) {
					displayOSHAItems(values[0]);
				}
				mShowNext = true;
				
			// Make the next button visible after inserting more than 20 results in DB
			} else if(mItemIndex > (11 + mSkipResults)){
				mNextButton.setVisibility(View.VISIBLE);
			}
			mNextButton.setEnabled(true);
			mListView.setTextFilterEnabled(true);
			Log.d(AppConstants.TAG,"OSHAFoodActivity : - onProgressUpdate");
		}

		@Override
		protected void onPostExecute(Void result) {
			Log.d(AppConstants.TAG, "ProcessData : + onPostExecute");
			super.onPostExecute(result);
			
			// False value indicate that AsyncTask is over and not running now
			mTaskRunning = false;
			handleButtonVisibilty();
			mSkipResults = mSkipResults + mResultSetCounter;
			Log.d(AppConstants.TAG, "ProcessData : - onPostExecute");
		}
		
	}

	private class MyAdapter extends ArrayAdapter<OSHAItem> {
		private LayoutInflater mLayoutInflater;

		public MyAdapter(Context context, int textViewResourceId,ArrayList<OSHAItem> mOSHAItemList) {
			super(context, textViewResourceId);
			mLayoutInflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = mLayoutInflater.inflate(R.layout.list_item_osha,parent, false);

				holder = new ViewHolder();
				holder.index = (TextView) convertView.findViewById(R.id.index);
				holder.establishment_name = (TextView) convertView.findViewById(R.id.establishment_name);
				holder.establishment_address = (TextView) convertView.findViewById(R.id.establishment_address);
				holder.establishment_penalty = (TextView) convertView.findViewById(R.id.establishment_penalty);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			OSHAItem OSHAItem = getItem(position);
			// holder.establishment_image.setBackgroundResource(R.drawable.icon);
			holder.establishment_name.setText(OSHAItem.getName());
			holder.establishment_address.setText(OSHAItem.getAddress() + " , "
					+ OSHAItem.getCity() + " , " + OSHAItem.getState() + " , "
					+ OSHAItem.getZipCode());
			holder.establishment_penalty.setText("Penalty : "+ OSHAItem.getPenalty(getApplicationContext()));
			holder.index.setText(Integer.toString(OSHAItem.getIndex()));

			return convertView;
		}

	}

	private static class ViewHolder {
		TextView index;
		TextView establishment_name;
		TextView establishment_address;
		TextView establishment_penalty;
	}

	// Display the data in array adapter
	public void displayOSHAItems(OSHAData oshaData) {
		Log.d(AppConstants.TAG, "OSHAFoodActivity : + displayOSHAItems");
		mOshaData = oshaData;
		mAddressList = new ArrayList<String>();
		String address = null;

		if (mOshaData != null) {
			mOSHAItemList = mOshaData.getData();
		}
		if (mOSHAItemList != null && mOSHAItemList.isEmpty() == false) {
			mDisplayedResults = mOSHAItemList.size();
			if (myAdapter == null) {
				myAdapter = new MyAdapter(getApplicationContext(),
						R.layout.list_item_osha, mOSHAItemList);
			}
			myAdapter.clear();
			for (OSHAItem item : mOSHAItemList) {
				myAdapter.add(item);
				address = item.getAddress() + "," + item.getCity() + ","
						+ item.getState() + "," + item.getZipCode();
				
				if (item.getPoint() != null) {
					// Add address to arrayList
					mAddressList.add(address);
				}
				// HashMap with Address as key and OSHAItem object as value
				OSHAaddressMap.put(address, item);
			}
			if (myAdapter.isEmpty()) {
				Toast.makeText(getApplicationContext(), R.string.fatal_error, Toast.LENGTH_SHORT).show();
			} else {
				mListView.setAdapter(myAdapter);
			}
		}
		Log.d(AppConstants.TAG, "OSHAFoodActivity : - displayOSHAItems");
	}
	
	/** 
	 * Handle visibility of next and previous button on the basis of results
	 */
	private void handleButtonVisibilty() {
		if (mShowNext && mDisplayedResults == 10) {
			mNextButton.setEnabled(true);
			mNextButton.setVisibility(View.VISIBLE);
		} else {
			mNextButton.setVisibility(View.INVISIBLE);
		}
		if (mShowPrevious) {
			mPrevButton.setVisibility(View.VISIBLE);
		} else {
			mPrevButton.setVisibility(View.INVISIBLE);
		}

	} 
	
	
	@Override
	public synchronized void onDestroy() {
		super.onDestroy();
		Log.d(AppConstants.TAG,"OSHAFoodActivity : + onDestroy()");
		ProcessData processData = new ProcessData();
		if (processData.getStatus() != AsyncTask.Status.FINISHED) {
			processData.cancel(true);
		}
	}
	
}