/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.mobispectra.android.apps.dolconnect.api.DOLDataContext;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequest;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequestCallback;
import com.mobispectra.android.apps.dolconnect.data.FAQSubTopicData;
import com.mobispectra.android.apps.dolconnect.data.FAQSubTopicData.FAQSubTopicItem;

public class FAQSubTopicsActivity extends BaseActivity implements DOLDataRequestCallback {
	private ListView mListView;
	private ArrayList<FAQSubTopicItem> mFAQSubTopicItemList;
	private FAQSubTopicData mFAQSubTopicData = new FAQSubTopicData();
	private MyAdapter myAdapter;
	private FAQSubTopicItem mFAQSubTopicItem;
	private String mTopicId = null;
	private String mSubTopicId = null;
	private TextView mTitleBarTextView;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Set layout for activity
		setContentView(R.layout.activity_faq_sub_topics);
		
		// Setup the title bar text
		mTitleBarTextView = (TextView) findViewById(R.id.titlebar_text);
		if (mTitleBarTextView != null) {
			mTitleBarTextView.setText(R.string.sub_topic);
		}
		
		mListView = (ListView) findViewById(R.id.faq_sub_topics_listview);
		// Extract the Extra Data passed to this activity
		Intent intent = getIntent();
		Bundle indexBundle = intent.getExtras();
		if (indexBundle != null) {
			mTopicId = indexBundle.getString(AppConstants.FAQ_TOPIC_ID);
			Log.d(AppConstants.TAG, "FAQSubTopicsActivity : TopicID : " + mTopicId);
		}
		final Object data = getLastNonConfigurationInstance();

		if (data != null) {
			// The activity is starting after screen rotation
			mFAQSubTopicData = (FAQSubTopicData) data;
			displayFAQSubTopicsItems(mFAQSubTopicData);
		} else {

			// Instantiate context object
			DOLDataContext context = new DOLDataContext(API_KEY, SHARED_SECRET,API_HOST, API_URI);

			// Instantiate new request object. Pass the context var that contains
			// all the API key info Set this as the callback responder
			DOLDataRequest request = new DOLDataRequest(this, context);

			// API method to call
			String method = "FAQ/SubTopics";
			// Hashmap to store the arguments
			HashMap<String, String> args = new HashMap<String, String>(3);

			// Populate the args into the HashMap
			args.put("top", "20");
			args.put("select", "SubTopicID,SubTopicValue,TopicID");
			args.put("filter", "TopicID eq " + mTopicId);

			// Show dialog
			showDialog(PROGRESS_BAR_DIALOG_ID);

			// Call the API method
			request.callAPIMethod(method, args);
		}

		// On item click listener for list view
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long id) {
				mFAQSubTopicItem = mFAQSubTopicItemList.get(position);
				mTopicId = mFAQSubTopicItem.getTopicID();
				mSubTopicId = mFAQSubTopicItem.getSubTopicID();
				
				Intent intent = new Intent(FAQSubTopicsActivity.this,FAQTopicQuestionsActivity.class);
				intent.putExtra(AppConstants.FAQ_TOPIC_ID, mTopicId);
				intent.putExtra(AppConstants.FAQ_SUB_TOPIC_ID, mSubTopicId);
				startActivity(intent);
			}

		});
	}
	@Override
	public Object onRetainNonConfigurationInstance() {
		final FAQSubTopicData fAQSubTopicData  = mFAQSubTopicData;
		return fAQSubTopicData;
	}
	// Callback method called when error occurs
	public void DOLDataErrorCallback(String error) {
		// dismiss dialog
		dismissDialog(PROGRESS_BAR_DIALOG_ID);
		// Show error on dialog
		AlertDialog alertDialog;
		alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Error");
		alertDialog.setMessage(error);
		alertDialog.show();
	}

	// Callback method called when results are returned and parsed
	public void DOLDataResultsCallback(List<Map<String, String>> results) {
		// dismiss dialog
		dismissDialog(PROGRESS_BAR_DIALOG_ID);
		if (results.size() != 0) {
			// Iterate thru List of results. Add each field to the data structure of FAQSubTopics
			for (Map<String, String> m : results) {
				mFAQSubTopicItem = mFAQSubTopicData.new FAQSubTopicItem(m
						.get("SubTopicID"), m.get("SubTopicValue"), m.get("TopicID"));
				mFAQSubTopicData.addData(mFAQSubTopicItem);
			}
			// Display data
			if (mFAQSubTopicData != null) {
				displayFAQSubTopicsItems(mFAQSubTopicData);
			}
			mListView.setTextFilterEnabled(true);
		} else {
			Toast.makeText(getApplicationContext(),getString(R.string.no_results_found),
					Toast.LENGTH_LONG).show();
		}

	}

	private class MyAdapter extends ArrayAdapter<FAQSubTopicItem> {
		private LayoutInflater mLayoutInflater;

		public MyAdapter(Context context, int textViewResourceId,ArrayList<FAQSubTopicItem> mFAQSubTopicItemList) {
			super(context, textViewResourceId);
			mLayoutInflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = mLayoutInflater.inflate(R.layout.list_item_faq_topics, parent, false);

				holder = new ViewHolder();

				holder.sub_topic_name = (TextView) convertView.findViewById(R.id.faq_topic_name);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			FAQSubTopicItem fAQSubTopicItem = getItem(position);

			holder.sub_topic_name.setText(fAQSubTopicItem.getSubTopicValue());

			return convertView;
		}

	}

	private static class ViewHolder {

		TextView sub_topic_name;
	}

	// Display the data in array adapter
	public void displayFAQSubTopicsItems(FAQSubTopicData fAQSubTopicData) {
		Log.d(AppConstants.TAG,"FAQSubTopicsActivity : +displayFAQSubTopicsItems");
		mFAQSubTopicData = fAQSubTopicData;

		if (mFAQSubTopicData != null) {
			mFAQSubTopicItemList = mFAQSubTopicData.getData();
		}
		if (mFAQSubTopicItemList != null && mFAQSubTopicItemList.isEmpty() == false) {
			if (myAdapter == null) {
				myAdapter = new MyAdapter(getApplicationContext(),R.layout.list_item_osha, mFAQSubTopicItemList);
			}
			myAdapter.clear();
			for (FAQSubTopicItem item : mFAQSubTopicItemList) {
				myAdapter.add(item);
			}
			if (myAdapter.isEmpty()) {
				Toast.makeText(this, R.string.fatal_error, Toast.LENGTH_SHORT).show();
			} else {
				mListView.setAdapter(myAdapter);
			}
		}
		Log.d(AppConstants.TAG,"FAQSubTopicsActivity : -displayFAQSubTopicsItems");
	}
}
