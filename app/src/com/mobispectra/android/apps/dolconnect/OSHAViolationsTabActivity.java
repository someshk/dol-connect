package com.mobispectra.android.apps.dolconnect;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.mobispectra.android.apps.dolconnect.maps.OSHAMapActivityJSInterface;

public class OSHAViolationsTabActivity extends TabActivity {
	private static final String LIST_VIEW_TAB = "list_view";
	private static final String MAP_VIEW_TAB = "map";
	private String mSearchString;
	private String mMethod;
	private String mFilterValue;
	private String mSearchBy;
	private String mCategory;
	private String mAgency;
	private TextView mTitleBarTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_oshactivity_tab);
		mTitleBarTextView = (TextView) findViewById(R.id.titlebar_text);
		
		// Extract the Extra Data passed to this activity
		Intent intent = getIntent();
		Bundle indexBundle = intent.getExtras();
		if (indexBundle != null) {
			mSearchString = indexBundle.getString(AppConstants.SEARCH_STRING);
			Log.d(AppConstants.TAG, "OSHAViolationsTabActivity: SearchString: "+mSearchString);
			mSearchBy = indexBundle.getString(AppConstants.SEARCH_BY);
			Log.d(AppConstants.TAG, "OSHAViolationsTabActivity: SearchBy: "+mSearchBy);
			mCategory = indexBundle.getString(AppConstants.SEARCH_CATEGORY);
			Log.d(AppConstants.TAG, "OSHAViolationsTabActivity: SearchCategory: "+mCategory);
			mAgency = indexBundle.getString(AppConstants.SEARCH_AGENCY);
			Log.d(AppConstants.TAG, "OSHAViolationsTabActivity: SearchAgency: "+mAgency);
			
			// API method to call
			mMethod = indexBundle.getString(AppConstants.METHOD);
			Log.d(AppConstants.TAG, "OSHAViolationsTabActivity: Method: "+mMethod);
			mFilterValue = indexBundle.getString(AppConstants.FILTER_VALUE);
			Log.d(AppConstants.TAG, "OSHAViolationsTabActivity: FilterValue: "+ mFilterValue);
		}

		// Set titlebar text
		if (mTitleBarTextView != null) {
			mTitleBarTextView.setText(getString(R.string.agency_osha) + " "
					+ getString(R.string.violations));
		}
		
		setupListViewTab();
		setupMapViewTab();
	}

	// Set up tab for ListView
	private void setupListViewTab() {
		final TabHost host = getTabHost();
		Intent intent = new Intent(this, OSHAFoodActivity.class);
		intent.putExtra(AppConstants.SEARCH_STRING, mSearchString);
		intent.putExtra(AppConstants.SEARCH_BY, mSearchBy);
		intent.putExtra(AppConstants.SEARCH_CATEGORY, mCategory);
		intent.putExtra(AppConstants.SEARCH_AGENCY, mAgency);
		intent.putExtra(AppConstants.METHOD, mMethod);
		intent.putExtra(AppConstants.FILTER_VALUE, mFilterValue);
		host.addTab(host.newTabSpec(LIST_VIEW_TAB ).setIndicator(
				getResources().getText(R.string.list),
				getResources().getDrawable(R.layout.tab_selector_recent))
				.setContent(intent));
	}

	// Set up tab for Map
	private void setupMapViewTab() {
		final TabHost host = getTabHost();
		Intent intent = new Intent(this, OSHAMapActivityJSInterface.class);
		host.addTab(host.newTabSpec(MAP_VIEW_TAB).setIndicator(
				getResources().getText(R.string.map),
				getResources().getDrawable(R.drawable.ic_tab_map))
				.setContent(intent));
	}
	
	 /** Handle "Home" action. */
    public void onHomeClick(View v) {
        this.onDestroy();
        Log.w(AppConstants.TAG, "OSHAviolationTabactivity : + onHomeClick(View v)");
    	Intent intent = new Intent(this, PhoneMainActivity.class);
        startActivity(intent);
    }
    
    
}
