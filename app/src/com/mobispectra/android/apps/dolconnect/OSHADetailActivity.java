/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

import com.mobispectra.android.apps.dolconnect.maps.OSHAMapListItemActivityJSInterface;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class OSHADetailActivity extends BaseActivity {
	private String mName;
	private String mState;
	private String mCity;
	private String mPenalty;
	private String mZipCode;
	private String mOpenDate;
	private String mOshaViolationIndicator;
	private String mTotalViolation;
	private String mSeriousViolation;
	private String mInspectionType;
	private String mStreetAddress;
	private String mNaicsCode;
	private String mLoadDate;
	private String mAddress;
	private double mLattitude;
	private double mLongitude;
	private TextView nameTextView;
	private TextView addressTextView;
	private TextView naicsCodeTextView;
	private TextView penaltyTextView;
	private TextView inspTypeTextView;
	private TextView openDateTextView;
	private TextView OshaViolationIndicatorTextView;
	private TextView totalViolationTextView;
	private TextView seriousViolationTextView;
	private TextView loadDateTextView;
	private TextView mTitleBarTextView;
	private StringBuilder mShareMessage;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_osha_detail);
		
		// Extract the Extra Data passed to this activity
		Intent intent = getIntent();
		Bundle indexBundle = intent.getExtras();
		if (indexBundle != null) {
			mName = indexBundle.getString("NAME");
			mStreetAddress = indexBundle.getString("SITE_ADDRESS");
			mState = indexBundle.getString("STATE");
			mCity = indexBundle.getString("CITY");
			mNaicsCode = indexBundle.getString("NAICS_CODE");
			mPenalty = indexBundle.getString("PENALTY");
			mZipCode = indexBundle.getString("ZIP_CODE");
			mAddress = indexBundle.getString("ADDRESS");
			mOpenDate = indexBundle.getString("OPEN_DATE");
			mOshaViolationIndicator = indexBundle.getString("OSHA_VIOLATION_INDICATOR");
			mTotalViolation = indexBundle.getString("TOTAL_VIOLATIONS");
			mSeriousViolation = indexBundle.getString("SERIOUS_VIOLATIONS");
			mInspectionType = indexBundle.getString("INSPECTION_TYPE");
			mLoadDate = indexBundle.getString("LOAD_DATE");
			mLattitude = indexBundle.getDouble("LATTITUDE");
			mLongitude = indexBundle.getDouble("LONGITUDE");
		}
		
		nameTextView = (TextView) findViewById(R.id.name);
		addressTextView = (TextView) findViewById(R.id.address);
		naicsCodeTextView = (TextView) findViewById(R.id.naics_code);
		penaltyTextView = (TextView) findViewById(R.id.penalty);
		inspTypeTextView = (TextView) findViewById(R.id.insp_type);
		openDateTextView = (TextView) findViewById(R.id.open_date);
		OshaViolationIndicatorTextView = (TextView) findViewById(R.id.Osha_violation_indicator);
		totalViolationTextView = (TextView) findViewById(R.id.total_violation);
		seriousViolationTextView = (TextView) findViewById(R.id.serious_violation);
		loadDateTextView = (TextView) findViewById(R.id.load_date);
		
		// Set titleBar text
		mTitleBarTextView = (TextView) findViewById(R.id.titlebar_text);
		mTitleBarTextView.setText(R.string.osha_violations_detail);
	
		nameTextView.setText(mName);
		addressTextView.setText(mStreetAddress+ " "+mState +" "+  mCity+" " +mZipCode  );
		naicsCodeTextView.setText(getString(R.string.naics_code)+" "+mNaicsCode);
		penaltyTextView.setText(getString(R.string.total_current_penalty)+" "+mPenalty);
		inspTypeTextView.setText(getString(R.string.insp_type)+" "+ mInspectionType);
		openDateTextView.setText(getString(R.string.open_date)+" "+mOpenDate);
		OshaViolationIndicatorTextView.setText(getString(R.string.osha_violation_indicator)+" "+mOshaViolationIndicator);
		totalViolationTextView.setText(getString(R.string.total_violations)+" "+mTotalViolation);
		seriousViolationTextView.setText(getString(R.string.serious_violations)+" "+mSeriousViolation);
		loadDateTextView.setText(getString(R.string.load_date)+" "+mLoadDate);
		
		mShareMessage = new StringBuilder();
		mShareMessage.append("\n" +mName).append("\n" + mAddress).append("\n" + getString(R.string.naics_code)+" "+mNaicsCode)
		.append("\n"+getString(R.string.total_current_penalty)+" "+mPenalty).append("\n"+getString(R.string.insp_type)+" "+ mInspectionType)
		.append("\n"+getString(R.string.open_date)+" "+mOpenDate).append("\n"+getString(R.string.osha_violation_indicator)+" "+mOshaViolationIndicator)
		.append("\n"+getString(R.string.total_violations)+" "+mTotalViolation).append("\n"+getString(R.string.serious_violations)+" "+mSeriousViolation)
		.append("\n"+getString(R.string.load_date)+" "+mLoadDate);
	}
	
	/**
	 * Handle map click
	 * @param v
	 */
	public void onMapClick(View v){
		Intent intent = new Intent(OSHADetailActivity.this,OSHAMapListItemActivityJSInterface.class);
		intent.putExtra("NAME",mName);
		intent.putExtra("ADDRESS",mAddress);
		intent.putExtra("NAICS_CODE",mNaicsCode);
		intent.putExtra("PENALTY",mPenalty);
		intent.putExtra("OPEN_DATE",mOpenDate);
		intent.putExtra("OSHA_VIOLATION_INDICATOR",mOshaViolationIndicator);
		intent.putExtra("TOTAL_VIOLATIONS",mTotalViolation);
		intent.putExtra("SERIOUS_VIOLATIONS",mSeriousViolation);
		intent.putExtra("INSPECTION_TYPE",mInspectionType);
		intent.putExtra("LOAD_DATE",mLoadDate);
		intent.putExtra("LATTITUDE", mLattitude );
		intent.putExtra("LONGITUDE", mLongitude );
		startActivity(intent);
	}
	
	/**
	 * Handle share click
	 * @param v
	 */
	public void onShareClick(View v){
		Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
		shareIntent.putExtra(Intent.EXTRA_SUBJECT,getResources().getString(R.string.share_osha_subject));
		shareIntent.putExtra(Intent.EXTRA_TEXT,String.format(getString(R.string.share_osha_message) +  mShareMessage.toString()));
		shareIntent.setType("text/plain");
		startActivity(Intent.createChooser(shareIntent,
				getString(R.string.share_osha_violation)));
	}

}
