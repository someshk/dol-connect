/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.maps;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.mobispectra.android.apps.dolconnect.AppConstants;
import com.mobispectra.android.apps.dolconnect.BaseActivity;
import com.mobispectra.android.apps.dolconnect.R;

public class WHDMapListItemActivityJSInterface extends BaseActivity {
	private static final String MAP_URL = "file:///android_asset/WHDListItemMarker.html";
	private WebView webView;
	
	private String mTradeName;
	private String mFindingsStartDate;
	private String mFindingsEndDate;
	private String mFlsaViolationCount;
	private String mChildLaborViolation;
	private String mChildLaborViolationcount;
	private String mNaicCode;
	private String mNaicCodeDescription;
	private String mRepeatViolator;
	private String mFlsa15a3BwAtpAmt;
	private String mFlsaOtBwAtpAmt;
	private String mFlsaEeBwAtpCnt;
	private String mFlsaBwAtpAmt;
	private String mFlsaMwBwAtpCnt;
	private String mFlsaCmpAssdAmt;
	private String mFlsaClCmpAssdAmt;
	private double mLattitude = 0;
	private double mLongitude = 0;
	private String mAddress;
	private TextView titleBarTextView;

	@Override
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_item_jsinterface);
		 titleBarTextView = (TextView) findViewById(R.id.titlebar_text);
			if (titleBarTextView != null) {
				titleBarTextView.setText(R.string.whd_map);
			}

		// Extract the Extra Data passed to this activity
		Intent intent = getIntent();
		Bundle indexBundle = intent.getExtras();
		if (indexBundle != null) {
			mAddress = indexBundle.getString("ADDRESS");
			mLattitude = indexBundle.getDouble("LATTITUDE");
			mLongitude = indexBundle.getDouble("LONGITUDE");
			mTradeName = indexBundle.getString("TRADE_NAME");
			mFindingsStartDate = indexBundle.getString("FINDINGS_START_DATE");
			mFindingsEndDate = indexBundle.getString("FINDINGS_END_DATE");
			mFlsaViolationCount = indexBundle.getString("FLSA_VIOLATION_COUNT");
			mChildLaborViolation = indexBundle.getString("CHILD_LABOR_VIOLATION");
			mChildLaborViolationcount  = indexBundle.getString("CHILD_LABOR_VIOLATION_COUNT");
			mNaicCode = indexBundle.getString("NAIC_CODE");
			mNaicCodeDescription = indexBundle.getString("NAIC_CODE_DESCRIPTION");
			mRepeatViolator = indexBundle.getString("REPEAT_VIOLATOR");
			mFlsa15a3BwAtpAmt = indexBundle.getString("FLSA_15a3_BW_ATP_AMT");
			mFlsaOtBwAtpAmt = indexBundle.getString("FLSA_OT_BW_ATP_AMT");
			mFlsaEeBwAtpCnt= indexBundle.getString("FLSA_EE_BW_ATP_CNT");
			mFlsaBwAtpAmt = indexBundle.getString("FLSA_BW_ATP_AMT");
			mFlsaMwBwAtpCnt  = indexBundle.getString("FLSA_MW_BW_ATP_CNT");
			mFlsaCmpAssdAmt = indexBundle.getString("FLSA_CMP_ASSD_AMT");
			mFlsaClCmpAssdAmt = indexBundle.getString("FLSA_CL_CMP_ASSD_AMT");
			
		}
		setupWebView();

	}
	
	/** Sets up the WebView object and loads the URL of the page **/
	private void setupWebView() {

		webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new WebViewClient());
		webView.loadUrl(MAP_URL);

		/** Allows JavaScript calls to access application resources **/
		webView.addJavascriptInterface(new WHDListItemJavaScriptInterface(), "WHD");

	}

	/**
	 * Sets up the interface for getting access to Latitude and Longitude data
	 * from device
	 **/
	private class WHDListItemJavaScriptInterface {
		
		public double getLatitude() {
			Log.d(AppConstants.TAG, " WHDListItemJavaScriptInterface: point="+ mLattitude);
			return mLattitude;
		}

		public double getLongitude() {
			
			Log.d(AppConstants.TAG, " WHDListItemJavaScriptInterface: point="+ mLongitude);
			return mLongitude;
		}
		
		public String getContent(){
			Log.d(AppConstants.TAG," WHDListItemJavaScriptInterface: + getContent");
			String result = null;
	
			result = " Name : " + mTradeName + "</br> Address : " + mAddress
					+ "</br>NAIC Code : "  +mNaicCode
					+ "</br>NAICDesription : " + mNaicCodeDescription
					+ "</br>Finding Date Start : " + 	mFindingsStartDate 
					+ "</br>Finding Date End : " + 	mFindingsEndDate 
					+ "</br>Minors found employed: " + mChildLaborViolation
					+ "</br>FLSA Violation Count : " + 	mFlsaViolationCount
					+ "</br>Child Labour Finding Indicator : "+ mFlsaClCmpAssdAmt
					+ "</br>FLSA Repeat violator: " + mRepeatViolator
					+ "</br>FLSA 15(a)(3) BW ATP: " + mFlsa15a3BwAtpAmt
					+ "</br>BW Agreed to under FLSA Overtime: " +mFlsaOtBwAtpAmt
					+ "</br>EE\'s Agreed to under FLSA: " +mFlsaEeBwAtpCnt
					+ "</br>BW Agreed to under FLSA: "    +mFlsaBwAtpAmt
					+ "</br>BW Agreed to under FLSA  Minimum Wages: " + mFlsaMwBwAtpCnt
					+ "</br>CMP\'s assessed under FLSA: " +	mFlsaCmpAssdAmt ;
					
			Log.d(AppConstants.TAG," WHDListItemJavaScriptInterface: - getContent");	
			return result;
		}
	
	}

}
