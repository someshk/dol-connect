/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect.maps;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.mobispectra.android.apps.dolconnect.AppConstants;
import com.mobispectra.android.apps.dolconnect.BaseActivity;
import com.mobispectra.android.apps.dolconnect.R;

public class OSHAMapListItemActivityJSInterface extends BaseActivity  {
	private static final String MAP_URL = "file:///android_asset/OshaListItemMarker.html";
	private WebView webView;
	private String mAddress;
	private String mName;
	private String mPenalty;
	private String mOpenDate;
	private String mOshaViolationIndicator;
	private String mTotalViolation;
	private String mSeriousViolation;
	private String mInspectionType;
	private String mNaicsCode;
	private String mLoadDate;
	private double mLattitude = 0;
	private double mLongitude = 0;
	private TextView titleBarTextView;
	
	@Override
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_item_jsinterface);
		 titleBarTextView = (TextView) findViewById(R.id.titlebar_text);
			if (titleBarTextView != null) {
				titleBarTextView.setText(R.string.osha_map);
			}


		// Extract the Extra Data passed to this activity
		Intent intent = getIntent();
		Bundle indexBundle = intent.getExtras();
		if (indexBundle != null) {
			mAddress = indexBundle.getString("ADDRESS");
			mLattitude = indexBundle.getDouble("LATTITUDE");
			mLongitude = indexBundle.getDouble("LONGITUDE");
			mName = indexBundle.getString("NAME");
			mNaicsCode = indexBundle.getString("NAICS_CODE");
			mPenalty = indexBundle.getString("PENALTY");
			mOpenDate = indexBundle.getString("OPEN_DATE");
			mOshaViolationIndicator = indexBundle.getString("OSHA_VIOLATION_INDICATOR");
			mTotalViolation = indexBundle.getString("TOTAL_VIOLATIONS");
			mSeriousViolation = indexBundle.getString("SERIOUS_VIOLATIONS");
			mInspectionType = indexBundle.getString("INSPECTION_TYPE");
			mLoadDate = indexBundle.getString("LOAD_DATE");
		}
		setupWebView();

	}

	/** Sets up the WebView object and loads the URL of the page **/
	private void setupWebView() {

		webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new WebViewClient());
		webView.loadUrl(MAP_URL);

		/** Allows JavaScript calls to access application resources **/
		webView.addJavascriptInterface(new OSHAListItemJavaScriptInterface(), "OSHA");

	}

	/**
	 * Sets up the interface for getting access to Latitude and Longitude data
	 * from device
	 **/
	private class OSHAListItemJavaScriptInterface {
		
		public double getLatitude() {
			Log.d(AppConstants.TAG, "OSHAJavaScriptInterface: latitude="+ mLattitude);
			return mLattitude;
		}

		public double getLongitude() {
		 Log.d(AppConstants.TAG, "OSHAJavaScriptInterface: longitude="+ mLongitude);
			return mLongitude;
		}
		
		public String getContent(){
			Log.d(AppConstants.TAG,"OSHAJavaScriptInterface: + getContent");
			String result = null;
		
			result = " Name : " + mName + "</br> Address : " + mAddress
			+ "</br> Penalty :  " + mPenalty  + "</br>NAICS Code : "
			+ mNaicsCode + "</br>Inspection Starting Date : " + 	mOpenDate 
			+ "</br>OSHA Violation Indicator :" + mOshaViolationIndicator
			+ "</br>Serious Violations : " + mSeriousViolation
			+ "</br>Total Violations : " + 	mTotalViolation 
			+ "</br>The load was completed on: " + 	mLoadDate ;
			Log.d(AppConstants.TAG,"OSHAJavaScriptInterface: - getContent");	
			return result;
		}
		
		public String getPentaltyFromIfc() {
			Log.d(AppConstants.TAG,"OSHAJavaScriptInterface: + getPentaltyFromIfc");
			Log.d(AppConstants.TAG,"OSHAJavaScriptInterface: -getPentaltyFromIfc");
			return mPenalty;
		}
	}

}