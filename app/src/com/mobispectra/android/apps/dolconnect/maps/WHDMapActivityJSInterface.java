/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.maps;

import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.mobispectra.android.apps.dolconnect.AppConstants;
import com.mobispectra.android.apps.dolconnect.BaseActivity;
import com.mobispectra.android.apps.dolconnect.R;
import com.mobispectra.android.apps.dolconnect.data.WHDData.WHDItem;
import static com.mobispectra.android.apps.dolconnect.WHDFoodActivity.mAddressList;

public class WHDMapActivityJSInterface extends BaseActivity {
	private static final String MAP_URL = "file:///android_asset/whd-map.html";
	private WebView webView;

	@Override
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jsinterface);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	//	Toast.makeText(this, "onResume", Toast.LENGTH_LONG).show();
		setupWebView();
	}
	
	/** Sets up the WebView object and loads the URL of the page **/
	private void setupWebView() {

		webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new WebViewClient());
		webView.loadUrl(MAP_URL);

		/** Allows JavaScript calls to access application resources **/
		webView.addJavascriptInterface(new WHDJavaScriptInterface(), "WHD");

	}

	/**
	 * Sets up the interface for getting access to Latitude and Longitude data
	 * from device
	 **/
	private class WHDJavaScriptInterface {
		private int mIndex = 0;
		private WHDItem mWHDItem;
		private String mAddress = null;
		
		public void getWHDItem() {
			Log.d(AppConstants.TAG,"WHDJavaScriptInterface : + getWHDItem");
			if (mIndex == 10) {
				mIndex = 0;
			}
			if (mAddressList != null) {
				mAddress = mAddressList.get(mIndex);

				Log.d(AppConstants.TAG,"WHDJavaScriptInterface : + getWHDItem : Address = "+ mAddress);
				// Get the Object from HashMap using address as key
				mWHDItem = WHDAddressMap.get(mAddress);
			}
			Log.d(AppConstants.TAG,"WHDJavaScriptInterface : - getWHDItem");
		}
		
		public double getLatitude() {
			double lattitude = 40.016521;
			if (mWHDItem != null) {
				if (mWHDItem.getPoint() != null) {
					lattitude = mWHDItem.getPoint().getLatitudeE6() / 1E6;
				}
			}
			Log.d(AppConstants.TAG, "WHDJavaScriptInterface: point="+ lattitude);
			return lattitude;
		}

		public double getLongitude() {
			double longitude = -105.282866;
			if (mWHDItem != null) {
				if (mWHDItem.getPoint() != null) {
					longitude = mWHDItem.getPoint().getLongitudeE6() / 1E6;
				}
			}
			Log.d(AppConstants.TAG, "WHDJavaScriptInterface: point="+ longitude);
			return longitude;
		}
		
		public void moveToNextGeoPoint() {
			mIndex++;
		}
		
		public boolean isGeoListEmpty() {
			Log.d(AppConstants.TAG,"WHDJavaScriptInterface(): index=" + mIndex);
			Log.d(AppConstants.TAG,"WHDJavaScriptInterface(): size=" + mAddressList.size());
			return mAddressList.size() == mIndex ? false : true;
		}
		
		public String getContent(){
			Log.d(AppConstants.TAG,"WHDJavaScriptInterface(): + getContent");
			String name = null;
			String NAICDescription = null;
			String startDate = null;
		    String result = null;
			String endDate = null;
			String FLSAviolationCount = null;
			String childLabourFindingsIndicator = null;
			if (mWHDItem != null) {
				name = mWHDItem.getTrade_nm();
				NAICDescription = mWHDItem.getNaics_code_description();
				startDate = mWHDItem.getFindings_start_date();
				endDate = mWHDItem.getFindings_end_date();
				FLSAviolationCount = mWHDItem.getFlsa_violtn_cnt();
				childLabourFindingsIndicator = mWHDItem.getFlsa_cl_cmp_assd_amt();
			}	
			result = " Name : " + name + "</br> Address : " + mAddress
					+ "</br>NAICDesription : " + NAICDescription
					+ "</br>Finding Date Start : " + startDate
					+ "</br>Finding Date End : " + endDate
					+ "</br>FLSA Violation Count : " + FLSAviolationCount
					+ "</br>Child Labour Finding Indicator : "+ childLabourFindingsIndicator;
	
			Log.d(AppConstants.TAG,"WHDJavaScriptInterface(): - getContent");	
			return result;
		}
	
	}

}
