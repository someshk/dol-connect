/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.db;

import static com.mobispectra.android.apps.dolconnect.db.DbConstants.*;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.mobispectra.android.apps.dolconnect.AppConstants;
import com.mobispectra.android.apps.dolconnect.data.WHDData;
import com.mobispectra.android.apps.dolconnect.data.WHDData.WHDItem;

public class WHDDBIfc extends BaseDBIfc {
	
	public static synchronized void addWHDData(Context context,WHDData whdData){
		Log.d(AppConstants.TAG,"WHDDBIfc : + addWHDData");
		SQLiteDatabase db = null;
		WHDItem whdItem;
		ArrayList<WHDItem> whdItemList;
		whdItemList = whdData.getWHDData();
		int i = 0;
		try {
			if (context != null && whdData != null) {
				// Get red/write handle to the database
				db = openWritableDb(context);
			} else {
				return;
			}
			for (i = 0; i < whdItemList.size(); i++) {
				whdItem = whdItemList.get(i);

				// Fill the ContentValues object with the data values.
				ContentValues values = new ContentValues();
				values.put(INDEX,whdItem.getIndex());
				values.put(TRADE_NAME,whdItem.getTrade_nm());
				values.put(WHD_ADDRESS,whdItem.getStreet_addr_1_txt());
				values.put(WHD_CITY,whdItem.getCity_nm());
				values.put(WHD_STATE,whdItem.getSt_cd());
				values.put(WHD_ZIPCODE,whdItem.getZip_cd());
				values.put(WHD_NAICS_CODE,whdItem.getNaic_cd());
				values.put(NAICS_CODE_DESCRIPTION,whdItem.getNaics_code_description());
				values.put(START_DATE,whdItem.getFindings_start_date());
				values.put(END_DATE,whdItem.getFindings_end_date());
				values.put(FLSA_VIOLATION_COUNT,whdItem.getFlsa_violtn_cnt());
				values.put(FLSA_REPEAT_VIOLATOR ,whdItem.getFlsa_repeat_violator());
				values.put(FLSA_BW_ATP_AMT,whdItem.getFlsa_bw_atp_amt());
				values.put(FLSA_EE_ATP_CNT,whdItem.getFlsa_ee_atp_cnt());
				values.put(FLSA_MW_BW_AMT,whdItem.getFlsa_mw_bw_atp_amt());
				values.put(FLSA_OT_BW_ATP_AMT,whdItem.getFlsa_ot_bw_atp_am());
				values.put(FLSA_15A3_BW_ATP_AMT,whdItem.getFlsa_15a3_bw_atp_amt());
				values.put(FLSA_CMP_ASSD_AMT,whdItem.getFlsa_cmp_assd_amt());
				values.put(FLSA_CL_VIOLATION_CNT,whdItem.getFlsa_cl_violtn_cnt());
				values.put(FLSA_CL_MINOR_CNT ,whdItem.getFlsa_cl_minor_cnt());
				values.put(FLSA_CL_CMP_ASSD_AMT,whdItem.getFlsa_cmp_assd_amt());
				values.put(LATITUDE,(whdItem.getPoint() != null ? whdItem.getPoint().getLatitudeE6() : 0 ));
				values.put(LONGITUDE,(whdItem.getPoint() != null ? whdItem.getPoint().getLongitudeE6() : 0));
				
				if (db != null) {
					// insert data into the database - execute INSERT statement
					db.insertOrThrow(TABLE_WHD, null, values);
				}
			}
			closeWritableDb(db);
			db = null;
		} finally {
			// Close the DB
			if (db != null) {
				closeWritableDb(db);
			}
		}
		Log.d(AppConstants.TAG,"WHDDBIfc :- addWHDData");
	}
	
	public static synchronized WHDData getWHDItemsFromDB(Context context,int index){
		Log.d(AppConstants.TAG, "WHDDBIfc : + getWHDItemsFromDB");
		Log.d(AppConstants.TAG, "WHDDBIfc : + getWHDItemsFromDB : Index" + index);
		SQLiteDatabase db = null;
		Cursor cursor = null;
		WHDData whdData = new WHDData();
		WHDItem whdItem = null;
		String trade_nm = null;
		String street_addr = null;
		String city_nm = null;
		String st_cd = null;
		String zip_cd = null;
		String naic_cd = null;
		String naics_code_description = null;
		String findings_start_date = null;
		String findings_end_date = null;
		String flsa_violtn_cnt = null;
		String flsa_repeat_violator = null;
		String flsa_bw_atp_amt = null;
		String flsa_ee_atp_cnt = null;
		String flsa_mw_bw_atp_amt = null;
		String flsa_ot_bw_atp_am = null;
		String flsa_15a3_bw_atp_amt = null;
		String flsa_cmp_assd_amt = null;
		String flsa_cl_violtn_cnt = null;
		String flsa_cl_minor_cnt = null;
		String flsa_cl_cmp_assd_amt = null;
		GeoPoint geoPoint = null;
		long latitude = 0;
		long longitude = 0;
		int itemIndex = 0;
		
		try {
			db = openReadableDb(context);
			String selectionClause = INDEX + " >= ?";
			String[] selectionArgs = new String[] { Integer.toString(index) }; 
			cursor = db.query(TABLE_WHD, null,selectionClause , selectionArgs, null, null, null);
			int j = cursor.getCount();
			Log.d(AppConstants.TAG,"WHDDBIfc : getWHDItemsFromDB : Cursor count : " + cursor.getCount());
			if (cursor.moveToFirst()) {
				for(int i = 1;i <= (j > 10 ? 10 : j) ; i++){
					itemIndex = cursor.getInt(cursor.getColumnIndex(INDEX));
					trade_nm  = cursor.getString(cursor.getColumnIndex(TRADE_NAME));
					street_addr = cursor.getString(cursor.getColumnIndex(WHD_ADDRESS));
					city_nm = cursor.getString(cursor.getColumnIndex(WHD_CITY));
					st_cd = cursor.getString(cursor.getColumnIndex(WHD_STATE));
					zip_cd = cursor.getString(cursor.getColumnIndex(WHD_ZIPCODE));
					naic_cd = cursor.getString(cursor.getColumnIndex(WHD_NAICS_CODE));
					naics_code_description = cursor.getString(cursor.getColumnIndex(NAICS_CODE_DESCRIPTION));
					findings_start_date = cursor.getString(cursor.getColumnIndex(START_DATE));
					findings_end_date = cursor.getString(cursor.getColumnIndex(END_DATE));
					flsa_violtn_cnt = cursor.getString(cursor.getColumnIndex(FLSA_VIOLATION_COUNT));
					flsa_repeat_violator = cursor.getString(cursor.getColumnIndex(FLSA_REPEAT_VIOLATOR));
					flsa_bw_atp_amt= cursor.getString(cursor.getColumnIndex(FLSA_BW_ATP_AMT));
					flsa_ee_atp_cnt = cursor.getString(cursor.getColumnIndex(FLSA_EE_ATP_CNT));
					flsa_mw_bw_atp_amt = cursor.getString(cursor.getColumnIndex(FLSA_MW_BW_AMT));
					flsa_ot_bw_atp_am = cursor.getString(cursor.getColumnIndex(FLSA_OT_BW_ATP_AMT));
					flsa_15a3_bw_atp_amt = cursor.getString(cursor.getColumnIndex(FLSA_15A3_BW_ATP_AMT));
					flsa_cmp_assd_amt = cursor.getString(cursor.getColumnIndex(FLSA_CMP_ASSD_AMT));
					flsa_cl_violtn_cnt = cursor.getString(cursor.getColumnIndex(FLSA_CL_VIOLATION_CNT));
					flsa_cl_minor_cnt = cursor.getString(cursor.getColumnIndex(FLSA_CL_MINOR_CNT));
					flsa_cl_cmp_assd_amt = cursor.getString(cursor.getColumnIndex(FLSA_CL_CMP_ASSD_AMT));
					latitude = cursor.getLong(cursor.getColumnIndex(LATITUDE));
					longitude = cursor.getLong(cursor.getColumnIndex(LONGITUDE));
					
					// Check if geoPoints are 0 or not .If points are 0 then put geoPoint to null
					if (latitude == 0 && longitude == 0) {
						geoPoint = null;
					} else {
						geoPoint = new GeoPoint((int) (latitude),(int) (longitude));
					}
					// Add data to the object
					whdItem = whdData.new WHDItem(itemIndex, trade_nm,
							street_addr, city_nm, st_cd, zip_cd, naic_cd,
							naics_code_description, findings_start_date,
							findings_end_date, flsa_violtn_cnt,
							flsa_repeat_violator, flsa_bw_atp_amt,
							flsa_ee_atp_cnt, flsa_mw_bw_atp_amt,
							flsa_ot_bw_atp_am, flsa_15a3_bw_atp_amt,
							flsa_cmp_assd_amt, flsa_cl_violtn_cnt,
							flsa_cl_minor_cnt, flsa_cl_cmp_assd_amt, geoPoint);
					whdData.addWHDData(whdItem);
					cursor.moveToNext();
				}
			}else{
				return null;
			}
			closeReadableDb(db);
			db = null;
			
			cursor.close();
			cursor = null;

		} finally {
			// Close the DB
			if (db != null) {
				closeWritableDb(db);
			}
			// Close the cursor
			if (cursor != null) {
				cursor.close();
			}
		}
		Log.d(AppConstants.TAG, "WHDDBIfc : - getWHDItemsFromDB");
		return whdData;
		
	}
	
	public static void deleteWHDData(Context context) {
		Log.d(AppConstants.TAG, "WHDDBIfc : + deleteWHDData");
		SQLiteDatabase db = null;
		try {
			db = openReadableDb(context);
			db.delete(TABLE_WHD, null, null);
			closeReadableDb(db);
			db = null;
		} finally {
			// Close the DB
			if (db != null) {
				closeWritableDb(db);
			}
		}
		Log.d(AppConstants.TAG, "WHDDBIfc : - deleteWHDData");

	}

}
