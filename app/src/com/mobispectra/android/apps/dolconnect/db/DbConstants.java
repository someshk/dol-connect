/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.db;

import android.provider.BaseColumns;

public interface DbConstants extends BaseColumns {

	public static final String DATABASE_NAME = "DOL";
	public static final int DATABASE_VERSION = 1;

	public static final String TABLE_STATE = "states";

	// Columns in the State table
	public static final String STATE_CODE = "stateCode";
	public static final String STATE_NAME= "stateName";
	
	//Recent Search Table
	public static final String TABLE_RECENT_SEARCH = "recent_search";

	// Columns in the Recent Search Table
	public static final String SEARCH_BY = "searchBy";
	public static final String SEARCH_STRING= "searchString";
	public static final String SEARCH_CATEGORY = "searchCategory";
	public static final String SEARCH_AGENCY = "searchAgency";
	
	
	//OSHA Table
	public static final String TABLE_OSHA = "osha";

	// Columns in the OSHA Table
	public static final String INDEX = "item_index";
	public static final String ACTIVITY_NR = "activity_nr";
	public static final String ESTABLISHMENT_NAME= "estab_name";
	public static final String ADDRESS = "site_address";
	public static final String CITY = "site_city";
	public static final String STATE = "site_state";
	public static final String ZIP_CODE= "site_zip";
	public static final String NAICS_CODE = "naics_code";
	public static final String INSP_TYPE = "insp_type";
	public static final String OPEN_DATE = "open_date";
	public static final String TOTAL_CURRENT_PENALTY= "total_current_penalty";
	public static final String VIOLATION_INDICATOR = "osha_violation_indicator";
	public static final String SERIOUS_VIOLATIONS = "serious_violations";
	public static final String TOTAL_VIOLATIONS = "total_violations";
	public static final String LOAD_DATE = "load_dt";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	
	//WHD Table
	public static final String TABLE_WHD = "whd";

	// Columns in the WHD  Table
	public static final String TRADE_NAME = "trade_nm";
	public static final String WHD_ADDRESS= "street_addr_1_txt";
	public static final String WHD_CITY = "city_nm";
	public static final String WHD_STATE = "st_cd";
	public static final String WHD_ZIPCODE = "zip_cd";
	public static final String WHD_NAICS_CODE= "naic_cd";
	public static final String NAICS_CODE_DESCRIPTION = "naics_code_description";
	public static final String START_DATE = "findings_start_date";
	public static final String END_DATE = "findings_end_date";
	public static final String FLSA_VIOLATION_COUNT= "flsa_violtn_cnt";
	public static final String FLSA_REPEAT_VIOLATOR = "flsa_repeat_violator";
	public static final String FLSA_BW_ATP_AMT = "flsa_bw_atp_amt";
	public static final String FLSA_EE_ATP_CNT = "flsa_ee_atp_cnt";
	public static final String FLSA_MW_BW_AMT = "flsa_mw_bw_atp_amt";
	public static final String FLSA_OT_BW_ATP_AMT = "flsa_ot_bw_atp_amt";
	public static final String FLSA_15A3_BW_ATP_AMT = "flsa_15a3_bw_atp_amt";
	public static final String FLSA_CMP_ASSD_AMT = "flsa_cmp_assd_amt";
	public static final String FLSA_CL_VIOLATION_CNT= "flsa_cl_violtn_cnt";
	public static final String FLSA_CL_MINOR_CNT = "flsa_cl_minor_cnt";
	public static final String FLSA_CL_CMP_ASSD_AMT = "flsa_cl_cmp_assd_amt";
}