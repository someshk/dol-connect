/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.db;


import static android.provider.BaseColumns._ID;
import static com.mobispectra.android.apps.dolconnect.db.DbConstants.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import com.mobispectra.android.apps.dolconnect.AppConstants;
import com.mobispectra.android.apps.dolconnect.R;


import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

public class DOLDB extends SQLiteOpenHelper{
	private Context mContext;
	private SQLiteDatabase mDatabase;

	public DOLDB(Context ctx) {
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
		mContext = ctx;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d(AppConstants.TAG, "DOLDB:+onCreate");
		mDatabase = db;
		// Create table for QuickPlanner
		db.execSQL("CREATE TABLE " + TABLE_STATE + " (" + _ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + STATE_CODE
				+ " TEXT," + STATE_NAME + " TEXT);");

		// Create table for Routes
		db.execSQL("CREATE TABLE " + TABLE_RECENT_SEARCH + " (" + _ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + SEARCH_BY + " TEXT,"
				+ SEARCH_STRING + " TEXT," + SEARCH_CATEGORY + " TEXT,"
				+ SEARCH_AGENCY + " TEXT);");

		// Create table for FareCalculator
		db.execSQL("CREATE TABLE " + TABLE_OSHA + " (" + _ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + INDEX + " INTEGER," + ACTIVITY_NR
				+ " TEXT," + ESTABLISHMENT_NAME + " TEXT," + ADDRESS + " TEXT,"
				+ CITY + " TEXT," + STATE + " TEXT," + ZIP_CODE + " TEXT,"
				+ NAICS_CODE + " TEXT," + INSP_TYPE + " TEXT," + OPEN_DATE
				+ " TEXT" + "," + TOTAL_CURRENT_PENALTY + " TEXT,"
				+ VIOLATION_INDICATOR + " TEXT," + SERIOUS_VIOLATIONS
				+ " TEXT," + TOTAL_VIOLATIONS + " TEXT," + LOAD_DATE
				+ " TEXT," + LATITUDE + " TEXT," + LONGITUDE + " TEXT);");

		// Create table for FareCalculator
		db.execSQL("CREATE TABLE " + TABLE_WHD + " (" + _ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, "+ INDEX + " INTEGER," + TRADE_NAME
				+ " TEXT," + WHD_ADDRESS + " TEXT," + WHD_CITY + " TEXT,"
				+ WHD_STATE + " TEXT," + WHD_ZIPCODE + " TEXT,"
				+ WHD_NAICS_CODE + " TEXT," + NAICS_CODE_DESCRIPTION + " TEXT,"
				+ START_DATE + " TEXT," + END_DATE + " TEXT" + ","
				+ FLSA_VIOLATION_COUNT + " TEXT," + FLSA_REPEAT_VIOLATOR
				+ " TEXT," + FLSA_BW_ATP_AMT + " TEXT," + FLSA_EE_ATP_CNT
				+ " TEXT," + FLSA_MW_BW_AMT + " TEXT," + FLSA_OT_BW_ATP_AMT
				+ " TEXT," + FLSA_15A3_BW_ATP_AMT + " TEXT,"
				+ FLSA_CMP_ASSD_AMT + " TEXT," + FLSA_CL_VIOLATION_CNT
				+ " TEXT," + FLSA_CL_MINOR_CNT + " TEXT" + ","
				+ FLSA_CL_CMP_ASSD_AMT + " TEXT," + LATITUDE + " TEXT," + LONGITUDE + " TEXT);");
		
		loadStates();

		Log.d(AppConstants.TAG, "DOLDB:Table created");
		Log.d(AppConstants.TAG, "DOLDB:-onCreate");
	}
	
	 /**
     * Starts a thread to load the database table with words
     */
    private synchronized void loadStates() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    loadData();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }).start();
    }

    private synchronized void loadData() throws IOException {
    	Log.d(AppConstants.TAG,"DOLDB : loadData : loading States");
        final Resources resources = mContext.getResources();
        InputStream inputStream = resources.openRawResource(R.raw.states);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] strings =  TextUtils.split(line,",");
                if (strings.length < 1) continue;
                long id = addState(strings[0].trim(), strings[1].trim());
                if (id < 0) {
                    Log.e(AppConstants.TAG, "DOLDB : loadData : unable to add word: " + strings[0].trim());
                }
            }
        } finally {
            reader.close();
        }
        Log.d(AppConstants.TAG,"DOLDB : loadData : Done loading States");
    }
    
    /**
     * Add a state to the dictionary.
     * @return rowId or -1 if failed
     */
    public long addState(String stateCode, String stateName) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(STATE_CODE, stateCode);
        initialValues.put(STATE_NAME, stateName);

        return mDatabase.insert(TABLE_STATE, null, initialValues);
    }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(AppConstants.TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
		
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATE);	
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENT_SEARCH);	
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_OSHA);		
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_WHD);	
		
		onCreate(db);
	}
}
