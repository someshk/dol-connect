/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;


public class BaseDBIfc {
	protected static DOLDB mDOLDb = null;
	
	public static SQLiteDatabase openWritableDb(Context context) {
		// Insert route data into the database
		SQLiteDatabase writableDb=null;
		if (context != null) {
			if (mDOLDb == null)
				mDOLDb = new DOLDB(context);
			if (mDOLDb != null) {
				writableDb = mDOLDb.getWritableDatabase();
				// Log.d(AppConstants.TAG,"BaseDBIfc: openWritableDB - SUCCESS, db = " + writableDb);
			}
		}
		return writableDb;
	}
	
	public static void closeWritableDb(SQLiteDatabase writableDb) {
		if(writableDb!=null) {
			writableDb.close();
			// Log.d(AppConstants.TAG,"BaseDBIfc: closeWritableDb - SUCCESS");
			writableDb = null;
		}
		if(mDOLDb!=null) {
			mDOLDb.close();
			mDOLDb = null;
		}
	}
	
	public static SQLiteDatabase openReadableDb(Context context) {
		// Insert route data into the database
		SQLiteDatabase readableDb = null;
		if (context != null) {
			if (mDOLDb == null)
				mDOLDb = new DOLDB(context);
			if (mDOLDb != null) {
				readableDb = mDOLDb.getReadableDatabase();
				// Log.d(AppConstants.TAG,"BaseDBIfc: openReadableDb - SUCCESS, db = " + readableDb );
			}
		}
		return readableDb;
	}
	
	public static void closeReadableDb(SQLiteDatabase readableDb) {
		if(readableDb!=null) {
			readableDb.close();
			// Log.d(AppConstants.TAG,"BaseDBIfc: closeReadableDb - SUCCESS");
			readableDb = null;
		}
		if(mDOLDb!=null) {
			mDOLDb.close();
			mDOLDb = null;
		}
	}
}
