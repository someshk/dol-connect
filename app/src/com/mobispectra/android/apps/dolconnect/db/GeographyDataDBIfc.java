/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.db;

import static android.provider.BaseColumns._ID;
import static com.mobispectra.android.apps.dolconnect.db.DbConstants.*;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.mobispectra.android.apps.dolconnect.AppConstants;

public class GeographyDataDBIfc extends BaseDBIfc {
	private static Context mContext;
	
	public GeographyDataDBIfc(Context context) {
		mContext = context;
	}
	
	// Fetch states from DB
	public static Cursor fetchAllStates(Context context) {
		Cursor cursor = null;
		SQLiteDatabase db;
		Log.d(AppConstants.TAG, "GeographyDataDBIfc: +fetchAllStates");

		try {
			db = openReadableDb(context);

			cursor = db.query(TABLE_STATE, new String[] { _ID,STATE_NAME}, null, null, null,
					null, null);
		} catch (SQLiteException e) {
			Log.e(AppConstants.TAG, "GeographyDataDBIfc:fetchAllStates: FATAL ERROR ="+e.toString());
		} catch (IllegalStateException e) {
			Log.e(AppConstants.TAG, "GeographyDataDBIfc:fetchAllStates: FATAL ERROR ="+e.toString());
		}
		Log.d(AppConstants.TAG, "GeographyDataDBIfc: -fetchAllStates");

		return cursor;
	}
	
	public Cursor getStatesFromDB(String[] columns, String selection, String[] selectionArgs) {
		return query(selection, selectionArgs, columns);
	}
	
	 /**
     * Returns a Cursor over all stops that match the given query
     *
     * @param query The string to search for
     * @param columns The columns to include, if null then all are included
     * @return Cursor over all stops that match, or null if none found.
     */
    public Cursor getWordMatches(String query, String[] columns) {
        String selection = STATE_NAME + " like '%" + query + "%'";
        return query(selection, null, columns);
    }
    
    /**
     * Performs a database query.
     * @param selection The selection clause
     * @param selectionArgs Selection arguments for "?" components in the selection
     * @param columns The columns to return
     * @return A Cursor over all rows matching the query
     */
	private Cursor query(String selection, String[] selectionArgs,String[] columns) {
		/*
		 * The SQLiteBuilder provides a map for all possible columns requested
		 * to actual columns in the database, creating a simple column alias
		 * mechanism by which the ContentProvider does not need to know the real
		 * column names
		 */
		Cursor cursor = null;
		SQLiteDatabase db = null;
		try {
			db = openWritableDb(mContext);
			cursor = db.query(TABLE_STATE, columns, selection,
					selectionArgs, null, null, null);
			if (cursor == null) {
				return null;
			} else if (!cursor.moveToFirst()) {
				cursor.close();
				return null;
			}
		} catch (SQLiteException e) {
			Log.e(AppConstants.TAG,"GeographyDataDBIfc:query: FATAL ERROR ="+ e.toString());
		} catch (IllegalStateException e) {
			Log.e(AppConstants.TAG,"GeographyDataDBIfc:query: FATAL ERROR ="+ e.toString());
		}
		return cursor;
	}
	
	// Get state code for given state 
	public static String getStateCode(String state) {
		Log.d(AppConstants.TAG, "GeographyDataDBIfc : + getStateCode");
		Cursor cursor = null;
		SQLiteDatabase db = null;
		String code = null;
		String FROM[] = { STATE_CODE };
		try {
			db = openWritableDb(mContext);
			cursor = db.query(TABLE_STATE, FROM, STATE_NAME + "=  '" + state+ "'", null, null, null, null);
			if (cursor == null) {
				return null;
			} else if (cursor.moveToFirst()) {
				code = cursor.getString(cursor.getColumnIndex(STATE_CODE));
			}
			cursor.close();
			cursor = null;
			closeWritableDb(db);
			db = null;
		} catch (SQLiteException e) {
			Log.e(AppConstants.TAG, "GeographyDataDBIfc : getStateCode : FATAL ERROR ="+ e.toString());
		} catch (IllegalStateException e) {
			Log.e(AppConstants.TAG, "GeographyDataDBIfc : getStateCode : FATAL ERROR ="
					+ e.toString());
		}finally{
			if(cursor != null){
				cursor.close();
			}
			if(db != null){
				db.close();
			}
		}
		Log.d(AppConstants.TAG, "GeographyDataDBIfc : - getStateCode");
		return code;
	}
	
	// Get state code for given state 
	public static String getStateName(String code) {
		Log.d(AppConstants.TAG, "GeographyDataDBIfc : + getStateName");
		Cursor cursor = null;
		SQLiteDatabase db = null;
		String name = null;
		String FROM[] = { STATE_NAME };
		try {
			db = openWritableDb(mContext);
			cursor = db.query(TABLE_STATE, FROM, STATE_CODE + "=  '" + code+ "'", null, null, null, null);
			if (cursor == null) {
				return null;
			} else if (cursor.moveToFirst()) {
				name = cursor.getString(cursor.getColumnIndex(STATE_NAME));
			}
			cursor.close();
			cursor = null;
			closeWritableDb(db);
			db = null;
		} catch (SQLiteException e) {
			Log.e(AppConstants.TAG, "GeographyDataDBIfc : getStateName : FATAL ERROR ="+ e.toString());
		} catch (IllegalStateException e) {
			Log.e(AppConstants.TAG, "GeographyDataDBIfc : getStateName : FATAL ERROR ="
					+ e.toString());
		}finally{
			if(cursor != null){
				cursor.close();
			}
			if(db != null){
				db.close();
			}
		}
		Log.d(AppConstants.TAG, "GeographyDataDBIfc : - getStateName");
		return name;
	}
}
