/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.db;

import static android.provider.BaseColumns._ID;
import static com.mobispectra.android.apps.dolconnect.db.DbConstants.*;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mobispectra.android.apps.dolconnect.AppConstants;

public class RecentSearchDBIfc extends BaseDBIfc {

	public static void addRecentSerachData(Context context, String searchBy,
			String searchString, String searchCategory, String searchAgency) {
		SQLiteDatabase db = null;
		Cursor cursor = null;
		Cursor numCursor = null;
		int id = 0;
		try {
			if (context != null) {
				// Get red/write handle to the database
				db = openWritableDb(context);
			} else {
				return;
			}
			// Check if Recent trip already exists in the DB
			String whereClause = "searchBy=? AND searchString=? AND searchCategory=? AND searchAgency=?";
			String[] whereArgs = new String[] { searchBy, searchString,searchCategory, searchAgency };
			cursor = db.query(TABLE_RECENT_SEARCH, null, whereClause,whereArgs, null, null, null);
			if(cursor.moveToFirst()){
				// delete the older one
				db.delete(TABLE_RECENT_SEARCH, whereClause, whereArgs);
			}
			
			// Get the total number of records in the DB
			numCursor = db.query(TABLE_RECENT_SEARCH, null, null, null, null, null, null);
			// If records are greater than 10 then delete the last one
			if (numCursor.getCount() == 10) {
				numCursor.moveToLast();
				id = numCursor.getInt(numCursor.getColumnIndex(_ID));
				db.delete(TABLE_RECENT_SEARCH, _ID + "=?",
						new String[] { Integer.toString(id) });
			}
			
			// Fill the ContentValues object with the data values.
			ContentValues values = new ContentValues();
			values.put(SEARCH_BY, searchBy);
			values.put(SEARCH_STRING,searchString);
			values.put(SEARCH_CATEGORY,searchCategory);
			values.put(SEARCH_AGENCY,searchAgency);
			
			// insert data into the database - execute INSERT statement
			db.insertOrThrow(TABLE_RECENT_SEARCH, null, values);
			
			cursor.close();
			cursor = null;
			
			closeWritableDb(db);
			db = null;
		} finally {
			// Close the DB
			if(db!=null) {
				closeWritableDb(db);
			}
			// close the cursor
			if(cursor!=null) {
				cursor.close();
			}
		}
		Log.d(AppConstants.TAG,"RecentSearchDBIfc : - addRecentSerachData");
	}
	
	public static Cursor getRecentSearches(Context context) {
		Log.d(AppConstants.TAG,"RecentSearchDBIfc : + getRecentSearches");
		SQLiteDatabase db = null;
		Cursor cursor = null;
		String ORDER_BY = _ID + " DESC";
		try {
			db = openWritableDb(context);
			cursor = db.query(TABLE_RECENT_SEARCH, null, null, null, null,null, ORDER_BY);
			if (cursor.moveToFirst()) {
				return cursor;
			}
			
			closeWritableDb(db);
			db = null;
			
		} finally {
			// Close the DB
			if (db != null) {
				closeWritableDb(db);
			}
		}
		Log.d(AppConstants.TAG,"RecentSearchDBIfc : - getRecentSearches");
		return null;
	}
}
