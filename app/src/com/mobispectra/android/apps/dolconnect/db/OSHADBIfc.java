/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.db;

import static com.mobispectra.android.apps.dolconnect.db.DbConstants.*;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.mobispectra.android.apps.dolconnect.AppConstants;
import com.mobispectra.android.apps.dolconnect.data.OSHAData;
import com.mobispectra.android.apps.dolconnect.data.OSHAData.OSHAItem;

public class OSHADBIfc extends BaseDBIfc {
	
	public static synchronized void addOSHAData(Context context,OSHAData oshaData){
		Log.d(AppConstants.TAG,"OSHADBIfc : + addOSHAData");
		SQLiteDatabase db = null;
		OSHAItem oshaItem;
		ArrayList<OSHAItem> oshaItemList;
		
		int i = 0;
		try {
			if (context != null && oshaData != null) {
				// Get red/write handle to the database
				db = openWritableDb(context);
				oshaItemList = oshaData.getData();
			} else {
				return;
			}
			for (i = 0; i < oshaItemList.size(); i++) {
				oshaItem = oshaItemList.get(i);

				// Fill the ContentValues object with the data values.
				ContentValues values = new ContentValues();
				values.put(INDEX,oshaItem.getIndex());
				values.put(ACTIVITY_NR,oshaItem.getActivity_nr(context));
				values.put(ESTABLISHMENT_NAME,oshaItem.getName());
				values.put(ADDRESS,oshaItem.getAddress());
				values.put(CITY,oshaItem.getCity());
				values.put(STATE,oshaItem.getState());
				values.put(ZIP_CODE,oshaItem.getZipCode());
				values.put(NAICS_CODE,oshaItem.getNaics_code(context));
				values.put(INSP_TYPE,oshaItem.getInsp_type(context));
				values.put(OPEN_DATE,oshaItem.getOpen_date(context));
				values.put(TOTAL_CURRENT_PENALTY,oshaItem.getPenalty(context));
				values.put(VIOLATION_INDICATOR,oshaItem.getOsha_violation_indicator(context));
				values.put(SERIOUS_VIOLATIONS,oshaItem.getSerious_violations(context));
				values.put(TOTAL_VIOLATIONS,oshaItem.getTotal_violations(context));
				values.put(LOAD_DATE,oshaItem.getLoad_dt(context));
				values.put(LATITUDE,(oshaItem.getPoint() != null ? oshaItem.getPoint().getLatitudeE6() : 0 ));
				values.put(LONGITUDE,(oshaItem.getPoint() != null ? oshaItem.getPoint().getLongitudeE6() : 0));
				
				if (db != null) {
					// insert data into the database - execute INSERT statement
					db.insertOrThrow(TABLE_OSHA, null, values);
				}else{
					return;
				}
			}
			closeWritableDb(db);
			db = null;
		} finally {
			// Close the DB
			if (db != null) {
				closeWritableDb(db);
			}
		}
		Log.d(AppConstants.TAG,"OSHADBIfc :- addOSHAData");
	}
	
	public static synchronized OSHAData getOSHAItemsFromDB(Context context,int index){
		Log.d(AppConstants.TAG, "OSHADBIfc : + getOSHAItemsFromDB");
		Log.d(AppConstants.TAG, "OSHADBIfc : + getOSHAItemsFromDB : Index" + index);
		SQLiteDatabase db = null;
		Cursor cursor = null;
		OSHAData oshaData = new OSHAData();
		OSHAItem oshaItem = null;
		String activity_nr = null;
		String name = null;
		String address = null;
		String city = null;
		String state = null;
		String zipCode = null;
		String naicsCode = null;
		String insp_type = null;
		String open_date = null;
		String penalty = null;
		String osha_violation_indicator = null;
		String serious_violations = null;
		String total_violations = null;
		String load_dt = null;
		GeoPoint geoPoint = null;
		long latitude = 0;
		long longitude = 0;
		int itemIndex = 0;
		
		try {
			db = openReadableDb(context);
			String selectionClause = INDEX + " >= ?";
			String[] selectionArgs = new String[] { Integer.toString(index) }; 
			cursor = db.query(TABLE_OSHA, null,selectionClause , selectionArgs, null, null, null);
			int j = cursor.getCount();
			Log.d(AppConstants.TAG,"OSHADBIfc : getOSHAItemsFromDB : Cursor count : " + cursor.getCount());
			if (cursor.moveToFirst()) {
				for(int i = 1;i <= (j > 10 ? 10 : j) ; i++){
					itemIndex = cursor.getInt(cursor.getColumnIndex(INDEX));
					activity_nr = cursor.getString(cursor.getColumnIndex(ACTIVITY_NR));
					name = cursor.getString(cursor.getColumnIndex(ESTABLISHMENT_NAME));
					address = cursor.getString(cursor.getColumnIndex(ADDRESS));
					city = cursor.getString(cursor.getColumnIndex(CITY));
					state = cursor.getString(cursor.getColumnIndex(STATE));
					zipCode = cursor.getString(cursor.getColumnIndex(ZIP_CODE));
					naicsCode = cursor.getString(cursor.getColumnIndex(NAICS_CODE));
					insp_type = cursor.getString(cursor.getColumnIndex(INSP_TYPE));
					open_date = cursor.getString(cursor.getColumnIndex(OPEN_DATE));
					penalty = cursor.getString(cursor.getColumnIndex(TOTAL_CURRENT_PENALTY));
					osha_violation_indicator = cursor.getString(cursor.getColumnIndex(VIOLATION_INDICATOR));
					serious_violations = cursor.getString(cursor.getColumnIndex(SERIOUS_VIOLATIONS));
					total_violations = cursor.getString(cursor.getColumnIndex(TOTAL_VIOLATIONS));
					load_dt = cursor.getString(cursor.getColumnIndex(LOAD_DATE));
					latitude = cursor.getLong(cursor.getColumnIndex(LATITUDE));
					longitude = cursor.getLong(cursor.getColumnIndex(LONGITUDE));
					
					// Check if geoPoints are 0 or not .If points are 0 then put geoPoint to null
					if (latitude == 0 && longitude == 0) {
						geoPoint = null;
					} else {
						geoPoint = new GeoPoint((int) (latitude),(int) (longitude));
					}

					// Add data to the object
					oshaItem = oshaData.new OSHAItem(itemIndex, activity_nr, name,
							address, city, state, zipCode, naicsCode,
							insp_type, open_date, penalty,
							osha_violation_indicator, serious_violations,
							total_violations, load_dt, geoPoint);
					oshaData.addData(oshaItem);
					cursor.moveToNext();
				}
			}else{
				return null;
			}
			closeReadableDb(db);
			db = null;
			
			cursor.close();
			cursor = null;

		} finally {
			// Close the DB
			if (db != null) {
				closeWritableDb(db);
			}
			// Close the cursor
			if (cursor != null) {
				cursor.close();
			}
		}
		Log.d(AppConstants.TAG, "OSHADBIfc : - getOSHAItemsFromDB");
		return oshaData;
		
	}
	
	public static void deleteOSHAData(Context context) {
		Log.d(AppConstants.TAG, "OSHADBIfc : + deleteOSHAData");
		SQLiteDatabase db = null;
		try {
			db = openReadableDb(context);
			db.delete(TABLE_OSHA, null, null);
			closeReadableDb(db);
			db = null;
		} finally {
			// Close the DB
			if (db != null) {
				closeWritableDb(db);
			}
		}
		Log.d(AppConstants.TAG, "OSHADBIfc : - deleteOSHAData");

	}

}
