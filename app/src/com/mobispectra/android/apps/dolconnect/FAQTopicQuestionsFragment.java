/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mobispectra.android.apps.dolconnect.api.DOLDataContext;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequest;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequestCallback;
import com.mobispectra.android.apps.dolconnect.data.FAQTopicQuestionsData;
import com.mobispectra.android.apps.dolconnect.data.FAQTopicQuestionsData.FAQTopicQuestionsItem;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import static com.mobispectra.android.apps.dolconnect.BaseActivity.*;

public class FAQTopicQuestionsFragment extends Fragment implements DOLDataRequestCallback{
	private ListView mListView;
	private ArrayList<FAQTopicQuestionsItem> mFAQTopicQuestionsItemList;
	private FAQTopicQuestionsData mFAQTopicQuestionsData = new FAQTopicQuestionsData();
	private MyAdapter myAdapter;
	private FAQTopicQuestionsItem mFAQTopicQuestionsItem;
	private String mTopicId = null;
	private String mSubTopicId = null;
	private ViewGroup mRootView;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Extract the Extra Data passed to this activity
		Intent intent = BaseFragmentActivity.fragmentArgumentsToIntent(getArguments());

		mTopicId = intent.getStringExtra(AppConstants.FAQ_TOPIC_ID);
		mSubTopicId = intent.getStringExtra(AppConstants.FAQ_SUB_TOPIC_ID);
		Log.d(AppConstants.TAG, "FAQTopicQuestionsFragment :TopicID : "+ mTopicId);
		Log.d(AppConstants.TAG, "FAQTopicQuestionsFragment :SubTopicID : "+ mSubTopicId);
	
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = (ViewGroup) inflater.inflate(R.layout.activity_faq_topic_questions, null);
		mListView = (ListView) mRootView.findViewById(R.id.faq_topic_questions_listview);
		
		// Instantiate context object
		DOLDataContext context = new DOLDataContext(API_KEY, SHARED_SECRET,API_HOST, API_URI);

		// Instantiate new request object. Pass the context var that contains
		// all the API key info Set this as the callback responder
		DOLDataRequest request = new DOLDataRequest(this, context);

		// API method to call
		String method = "FAQ/TopicQuestions";
		// Hashmap to store the arguments
		HashMap<String, String> args = new HashMap<String, String>(3);

		// Populate the args into the HashMap
		args.put("top", "20");
		args.put("select","FAQID,TopicID,SubTopicID,Question,Answer,SourceURL,Hits,DateMod,FAQSource,Keywords,LastReviewDate");
		args.put("filter", "(TopicID eq " + mTopicId + ") and (SubTopicID eq "+ mSubTopicId + ")");

		// Call the API method
		request.callAPIMethod(method, args);
			
		return mRootView;
	}	
	
	// Callback method called when error occurs
	public void DOLDataErrorCallback(String error) {
		// Show error on dialog
		AlertDialog alertDialog;
		alertDialog = new AlertDialog.Builder(getActivity()).create();
		alertDialog.setTitle("Error");
		alertDialog.setMessage(error);
		alertDialog.show();
	}

	// Callback method called when results are returned and parsed
	public void DOLDataResultsCallback(List<Map<String, String>> results) {
		// Iterate thru List of results. Add each field to the data structure of
		if (results.size() != 0) {
			for (Map<String, String> m : results) {
				mFAQTopicQuestionsItem = mFAQTopicQuestionsData.new FAQTopicQuestionsItem(
						m.get("FAQID"), m.get("TopicID"), m.get("SubTopicID"),
						m.get("Question"), m.get("Answer"), m.get("SourceURL"),
						m.get("Hits"), getReadableDate(m.get("DateMod")), 
						m.get("FAQSource"), m.get("Keywords"), m.get("LastReviewDate"));
				mFAQTopicQuestionsData.addData(mFAQTopicQuestionsItem);

			}
			// Display data
			if (mFAQTopicQuestionsData != null) {
				displayFAQTopicQuestionsItems(mFAQTopicQuestionsData);
			}
			mListView.setTextFilterEnabled(true);
		} else {
			Toast.makeText(getActivity(), getString(R.string.no_results_found),
					Toast.LENGTH_LONG).show();
		}

	}

	private class MyAdapter extends ArrayAdapter<FAQTopicQuestionsItem> {
		private LayoutInflater mLayoutInflater;

		public MyAdapter(Context context, int textViewResourceId,ArrayList<FAQTopicQuestionsItem> mFAQTopicQuestionsItemList) {
			super(context, textViewResourceId);
			mLayoutInflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = mLayoutInflater.inflate(R.layout.list_item_faq_topic_question, parent, false);

				holder = new ViewHolder();

				holder.question = (TextView) convertView.findViewById(R.id.faq_topic_question);
				holder.question_modified_date = (TextView) convertView.findViewById(R.id.faq_topic_date_modified);
				holder.answer = (WebView) convertView.findViewById(R.id.faq_topic_answer);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			FAQTopicQuestionsItem fAQTopicQuestionsItem = getItem(position);

			holder.question.setText(fAQTopicQuestionsItem.getQuestion());
			holder.question_modified_date.setText(fAQTopicQuestionsItem.getDateMod());
			holder.answer.loadData(fAQTopicQuestionsItem.getAnswer(),"text/html", "utf-8");

			return convertView;
		}

	}

	private static class ViewHolder {
		TextView question;
		WebView answer;
		TextView question_modified_date;
	}

	// Display the data in array adapter
	public void displayFAQTopicQuestionsItems(FAQTopicQuestionsData fAQTopicQuestionsData) {
		Log.d(AppConstants.TAG,"FAQTopicQuestionsFragment : + displayFAQTopicQuestionsItems");
		mFAQTopicQuestionsData = fAQTopicQuestionsData;
		if (getActivity() != null) {
			if (mFAQTopicQuestionsData != null) {
				mFAQTopicQuestionsItemList = mFAQTopicQuestionsData.getData();
			}
			if (mFAQTopicQuestionsItemList != null&& mFAQTopicQuestionsItemList.isEmpty() == false) {
				if (myAdapter == null) {
					myAdapter = new MyAdapter(getActivity(),R.layout.list_item_osha, mFAQTopicQuestionsItemList);
				}
				myAdapter.clear();
				for (FAQTopicQuestionsItem item : mFAQTopicQuestionsItemList) {
					myAdapter.add(item);
				}
				if (myAdapter.isEmpty()) {
					Toast.makeText(getActivity(), R.string.fatal_error,Toast.LENGTH_SHORT).show();
				} else {
					mListView.setAdapter(myAdapter);
				}
			}
			Log.d(AppConstants.TAG,"FAQTopicQuestionsFragment : - displayFAQTopicQuestionsItems");
		}
	}
}
