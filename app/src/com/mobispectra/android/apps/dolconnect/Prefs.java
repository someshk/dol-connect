/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public final class Prefs {

	private static SharedPreferences mPrefs=null;
	
	private static final String  CURRENT_SCHEDULE_NUM="current_schedule_num";
	private static final String SPECIAL_SCHEDULE_UPDATE_TIMESTAMP="special_schedule_update_timestamp";;
	
	public static SharedPreferences init(Context context) {
		mPrefs = get(context);
		return mPrefs;
	}
	
	public static SharedPreferences get(Context context) {
		mPrefs = context.getSharedPreferences("SMILINGRIDE_PREFS", Context.MODE_PRIVATE);
		return mPrefs;
	}
	
	public static String getSpecialSchedUpdateTimeStamp() {
		return mPrefs.getString(SPECIAL_SCHEDULE_UPDATE_TIMESTAMP, "");
	}
	
	public static void setSpecialSchedUpdateTimeStamp(String timestamp) {
		Editor editor = mPrefs.edit();
		editor.putString(SPECIAL_SCHEDULE_UPDATE_TIMESTAMP, timestamp);
		editor.commit();
		return;
	}
	
	public static String getCurrentScheduleNumber() {
		String scheduleNum = mPrefs.getString(CURRENT_SCHEDULE_NUM, "");
		Log.d(AppConstants.TAG,"Prefs: Get Schedule Number = " + scheduleNum);
		return scheduleNum;
	}
	
	public static void setCurrentScheduleNumber(String scheduleNum) {
		if(scheduleNum!=null) {
			Log.d(AppConstants.TAG,"Prefs: Set Schedule Number = " + scheduleNum);
			Editor editor = mPrefs.edit();
			editor.putString(CURRENT_SCHEDULE_NUM, scheduleNum);
			editor.commit();
		}
	}
}