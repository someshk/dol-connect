/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

import com.mobispectra.android.apps.utils.NwAccessIfc;
import com.mobispectra.android.apps.utils.UIUtils;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

public class FacebookMobispectraActivity extends BaseActivity {

	private WebView mWebView;
	private TextView mTitleBarTextView;
	private ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_systemmap);

		if (UIUtils.isHoneycombTablet(getApplicationContext())) {
			// Setup Action Bar
			setupActionBar();
		} else {
			// Setup the title bar text
			mTitleBarTextView = (TextView) findViewById(R.id.titlebar_text);
			if (mTitleBarTextView != null) {
				mTitleBarTextView.setText(R.string.like_mobispectra_on_facebook);
			}
		}
			
		
		
		// Setup Action Bar Title
		
		mWebView = (WebView) findViewById(R.id.systemmap_webview);
		WebSettings settings = mWebView.getSettings();
		
		// Enable Javascript
		settings.setJavaScriptEnabled(true);
		
		// Set Zoom Control
		settings.setBuiltInZoomControls(true);

		// Google Analytics Integration
		// mAnalyticsTracker = GoogleAnalyticsTracker.getInstance();

		if (savedInstanceState != null) {
			mWebView.restoreState(savedInstanceState);
		} else {
			mWebView.setWebViewClient(new Client());

			if (NwAccessIfc.isNetworkConnected(getApplicationContext())) {
				// Load URL
				mWebView.loadUrl(AppConstants.WebUrl.MOBISPECTRA_FACEBOOK_URL);
			} else {
				Toast.makeText(getApplicationContext(),
						R.string.error_no_network_coverage, Toast.LENGTH_SHORT)
						.show();
			}
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		// if (mAnalyticsTracker != null) {
		// Track Feedback Form
		// mAnalyticsTracker.trackPageView("/Feedback");
		// }

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		mWebView.saveState(outState);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
	}

	private class Client extends WebViewClient {

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
		}

		@Override
		public void onLoadResource(WebView view, String url) {

			if (mProgressDialog == null) {
				mProgressDialog = new ProgressDialog(FacebookMobispectraActivity.this);
				mProgressDialog.setMessage("Please wait while loading...");
				mProgressDialog.show();
			}
		}

		@Override
		public void onPageFinished(WebView view, String url) {

			if (mProgressDialog != null && mProgressDialog.isShowing()) {
				mProgressDialog.dismiss();
				mProgressDialog = null;
			}

		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			Toast.makeText(FacebookMobispectraActivity.this, "Oh no! " + description,
					Toast.LENGTH_SHORT).show();
		}
	}
}
