/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

import java.util.ArrayList;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobispectra.android.apps.dolconnect.data.DOLTwitterFeedData;
import com.mobispectra.android.apps.dolconnect.data.DOLTwitterFeedIfc;
import com.mobispectra.android.apps.dolconnect.data.DOLTwitterFeedData.DOLTwitterFeed;
import com.mobispectra.android.apps.utils.ImageDownloader;
import com.mobispectra.android.apps.utils.NwAccessIfc;

public class DOLTwitterFeedActivity extends BaseActivity {
	static final int PROGRESS_BAR_DIALOG_ID = 0;
	TwitterFeedAdapter adapter;
	DOLTwitterFeedData mTwitterFeedData;
	ListView mListView; 
	ArrayList<DOLTwitterFeed> mTwitterFeedList;
	private TextView titleBarTextView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dol_twitter_feed);
		
		// Setup the title bar text
		titleBarTextView = (TextView) findViewById(R.id.titlebar_text);
		if (titleBarTextView != null) {
			titleBarTextView.setText(R.string.twitter_stream);
		}
		
		// Setup Action Bar
		setupActionBar();
		
		final Object data = getLastNonConfigurationInstance();
		mListView = (ListView) findViewById(R.id.bart_twitter_listview);

		if (data != null) {
			displayFeeds((DOLTwitterFeedData) data);
		} else {
			if (NwAccessIfc.isNetworkConnected(getApplicationContext())) {
				showDialog(PROGRESS_BAR_DIALOG_ID);
				new TwitterFeed().execute();
			} else {
				Toast.makeText(getApplicationContext(),R.string.error_no_network_coverage, Toast.LENGTH_SHORT).show();
			}
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {

		case PROGRESS_BAR_DIALOG_ID: {
			ProgressDialog dialog = new ProgressDialog(this);
			dialog.setMessage("Please wait while loading...");
			dialog.setIndeterminate(true);
			dialog.setCancelable(true);
			return dialog;
		}
		}
		return null;
	}
	
	@Override
	public Object onRetainNonConfigurationInstance() {
		final DOLTwitterFeedData twitterFeedData = mTwitterFeedData;
		return twitterFeedData;
	}

	class TwitterFeedAdapter extends ArrayAdapter<DOLTwitterFeed> {
		private final LayoutInflater mLayoutInflater;
		private final ImageDownloader imageDownloader = new ImageDownloader();
		
		TwitterFeedAdapter(DOLTwitterFeedActivity cDCTwitterFeedActivity,ArrayList<DOLTwitterFeed> twitterFeedList) {
			super(cDCTwitterFeedActivity, 0);
			mLayoutInflater = LayoutInflater.from(cDCTwitterFeedActivity);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = mLayoutInflater.inflate(R.layout.list_item_dol_twitter_feed, parent, false);

				holder = new ViewHolder();

				holder.profile_img = (ImageView) convertView.findViewById(R.id.profile_img);
				holder.screen_name = (TextView) convertView.findViewById(R.id.screen_name);
				holder.text = (TextView) convertView.findViewById(R.id.text);
				holder.created_at = (TextView) convertView.findViewById(R.id.created_at);
				holder.retweet = (TextView) convertView.findViewById(R.id.retweet);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			final DOLTwitterFeed twitterFeed = getItem(position);
			
			imageDownloader.download(twitterFeed.getImageUrl(), (ImageView) holder.profile_img);
			
			holder.screen_name.setText(twitterFeed.getScreen_name());
			holder.text.setText(twitterFeed.getText());
			holder.created_at.setText(twitterFeed.getCreated_at());
			holder.retweet.setText(" retweeted by " + twitterFeed.getRetweet_count() + " people");
		
			return convertView;
		}
	}

	class ViewHolder {
		ImageView profile_img;
		TextView screen_name;
		TextView text;
		TextView created_at;
		TextView retweet;
	}
	
	private class TwitterFeed extends AsyncTask<String, String, DOLTwitterFeedData> {

		protected DOLTwitterFeedData doInBackground(String... params) {
			return DOLTwitterFeedIfc.getTwitterFeeds(getApplicationContext());
		}

		protected void onProgressUpdate(String... progress) {

		}

		protected void onPostExecute(DOLTwitterFeedData twitterFeedData) {
			try {
				 dismissDialog(PROGRESS_BAR_DIALOG_ID);
			} catch (IllegalArgumentException ex) {
				// Just ignore this exception for now.
			}
			if (twitterFeedData != null) {
				displayFeeds(twitterFeedData);
			}else{
				Toast.makeText(DOLTwitterFeedActivity.this, R.string.no_server_response, Toast.LENGTH_LONG).show();
			}
		}

		protected void onPreExecute() {

		}
	}

	private void displayFeeds(DOLTwitterFeedData twitterFeedData) {
		mTwitterFeedData = twitterFeedData;
		if (mTwitterFeedData != null) {
			mTwitterFeedList = mTwitterFeedData.getTwitterFeeds();

			if (mTwitterFeedList != null && mTwitterFeedList.isEmpty() == false) {
				if (adapter == null) {
					adapter = new TwitterFeedAdapter(DOLTwitterFeedActivity.this,mTwitterFeedList);
				}
				adapter.clear();
				for (DOLTwitterFeed feeds : mTwitterFeedList) {
					adapter.add(feeds);
				}
				if (adapter.isEmpty()) {
					Toast.makeText(this, R.string.fatal_error, Toast.LENGTH_SHORT).show();
				} else {
					mListView.setAdapter(adapter);
				}
			}
			else{
				Toast.makeText(getApplicationContext(), "No Twitter Feeds available.", Toast.LENGTH_SHORT).show();
			}
		} else {
			Log.e(AppConstants.TAG,"BARTTwitterFeedActivity : displayFeeds: mTwitterFeedData = null");
		}
	}
}
