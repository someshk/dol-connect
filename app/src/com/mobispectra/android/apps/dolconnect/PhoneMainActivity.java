/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

import static android.provider.BaseColumns._ID;
import static com.mobispectra.android.apps.dolconnect.db.DbConstants.STATE_CODE;
import static com.mobispectra.android.apps.dolconnect.db.DbConstants.STATE_NAME;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.mobispectra.android.apps.dolconnect.db.DOLDB;
import com.mobispectra.android.apps.dolconnect.db.GeographyDataDBIfc;
import com.mobispectra.android.apps.utils.AppRater;
import com.mobispectra.android.apps.utils.Eula;
import com.mobispectra.android.apps.utils.NwAccessIfc;

public class PhoneMainActivity extends BaseActivity {
	private Button mButton;
	private AutoCompleteTextView mAutoCompleteTextView;
	private String mSearchString;
	private Cursor mCursor;
	private StopsListAdapter mAdapter;
	private SimpleAdapter mSimpleAdapter = null;
	private DOLDB myDbHelper;
	private RadioButton mOSHARadio;
	private RadioButton mWHDRadio;
	private Spinner mSearchBySpinner;
	private String mAgency;
	private Spinner mCategorySpinner;
	private String mCategory;
	private String mSearchBy;
	private RadioGroup mRadioGroup;
	private Button mTwitterButton;
	private Button mDOLFaqButton;
	private TextView mtitleBarTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Sets the layout for Activity
		setContentView(R.layout.activity_phone_main);
		
		mtitleBarTextView = (TextView) findViewById(R.id.titlebar_text);
		mtitleBarTextView.setText(getString(R.string.app_name));
		
		// Show App Rater
		if (savedInstanceState == null) {
			AppRater.app_launched(this);
		}
		
		// Create DB
		myDbHelper = new DOLDB(this);
		if (myDbHelper != null) {
			myDbHelper.getWritableDatabase();
		}
		
		// Show End User License Agreement
    	Eula.show(this) ;
		
		mSearchBySpinner = (Spinner) findViewById(R.id.search_by_spinner);

		mButton = (Button) findViewById(R.id.submit_button);
		mAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.search_box);
		mCategorySpinner = (Spinner) findViewById(R.id.category_spinner);

		mOSHARadio = (RadioButton) findViewById(R.id.osha_radio);
		mWHDRadio = (RadioButton) findViewById(R.id.whd_radio);
		mOSHARadio.setOnClickListener(agency_radio_listener);
		mWHDRadio.setOnClickListener(agency_radio_listener);
		mRadioGroup = (RadioGroup) findViewById(R.id.radio_group);
		mTwitterButton = (Button) findViewById(R.id.button_twitter);
		mDOLFaqButton = (Button) findViewById(R.id.button_faq);
		
		// Handle twitter action
		mTwitterButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(PhoneMainActivity.this,DOLTwitterFeedActivity.class));	
			}
		});

		// Handle FAQ action
		mDOLFaqButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(PhoneMainActivity.this,FAQTopicsActivity.class));		
			}
		});
		
		
		ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter
				.createFromResource(this, R.array.category,android.R.layout.simple_spinner_item);
		categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mCategorySpinner.setAdapter(categoryAdapter);

		// Handle Spinner Item Selection events
		mCategorySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						switch (position) {
						case 0:
							mCategory = FOOD_SERVICE;
							break;
						case 1:
							mCategory = HOSPITALITY;
							break;
						case 2:
							mCategory = RETAIL;
							break;
						}
						Log.d(AppConstants.TAG, "PhoneMainActivity: Category : "+ mCategory);
					}

					public void onNothingSelected(AdapterView<?> parent) {
						// showToast("From: unselected");
					}
				});

		ArrayAdapter<CharSequence> searchByAdapter = ArrayAdapter.createFromResource
		            (this, R.array.searchBy,android.R.layout.simple_spinner_item);
		searchByAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSearchBySpinner.setAdapter(searchByAdapter);

		// Handle Spinner Item Selection events
		mSearchBySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						mSearchBy = (String) parent.getItemAtPosition(position);
						
						// Change the hint of AutoCompleteTextView 
						if (mSearchBy.equals(getString(R.string.state))) {
							mAutoCompleteTextView.setHint(R.string.search_by_state);
							mAutoCompleteTextView.setAdapter(mAdapter);
						} else if (mSearchBy.equals(getString(R.string.establishment_name))) {
							mAutoCompleteTextView.setHint(R.string.search_by_establishment);
							mAutoCompleteTextView.setAdapter(mSimpleAdapter);
						} else if (mSearchBy.equals(getString(R.string.city))) {
							mAutoCompleteTextView.setHint(R.string.search_by_city);
							mAutoCompleteTextView.setAdapter(mSimpleAdapter);
						}
						Log.d(AppConstants.TAG, "PhoneMainActivity: SearchBy : "+ mSearchBy);
					}

					public void onNothingSelected(AdapterView<?> parent) {
						// showToast("From: unselected");
					}
				});

		mCursor = managedQuery(StatesProvider.CONTENT_URI, new String[] { _ID,
				STATE_NAME, STATE_CODE }, null, null, null);
		

		if (mCursor != null) {
			startManagingCursor(mCursor);
			// Adapter for showing suggestions
			mAdapter = new StopsListAdapter(this, mCursor);
		}

		mAutoCompleteTextView.setOnEditorActionListener(onSearch);

		// OnClickListener for Submit Button
		mButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Get the String from AutoCompleteTextView
				if (mSearchBy.equals(getString(R.string.state))) {
					mSearchString = GeographyDataDBIfc.getStateCode(mAutoCompleteTextView.getText().toString());
				} else {
					mSearchString = mAutoCompleteTextView.getText().toString();
				}
				Log.d(AppConstants.TAG, "PhoneMainActivity: SearchString : "+ mSearchString);
				if (NwAccessIfc.isNetworkConnected(getApplicationContext())) {
					startActivity(mSearchBy, mSearchString, mCategory, mAgency);
				} else {
					// Inform User that the Device is not connected to the Network
					Toast.makeText(getApplicationContext(),getString(R.string.error_no_network_coverage),
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}


	@Override
	protected void onResume() {
		super.onResume();
		if(mOSHARadio.isChecked()){
			mAgency = (String) mOSHARadio.getText();
			Log.d(AppConstants.TAG, "PhoneMainActivity : onResume Agency = " + mAgency);
			}
		if(mWHDRadio.isChecked()){
			mAgency = (String) mWHDRadio.getText();
			Log.d(AppConstants.TAG, "PhoneMainActivity : onResume Agency = " + mAgency);
			}
	}

	
	@Override
	protected void onRestart() {
		super.onRestart();
		mRadioGroup.clearCheck();
		mAgency = null;
		mAutoCompleteTextView.setText(null);
		mSearchBySpinner.setSelection(0);
		mCategorySpinner.setSelection(0);
	}


	// Radio button click listener
	private OnClickListener agency_radio_listener = new OnClickListener() {
		public void onClick(View v) {
			// Perform action on clicks
			RadioButton rb = (RadioButton) v;
			mAgency = (String) rb.getText();
			Log.d(AppConstants.TAG, "PhoneMainActivity : Agency = " + mAgency);
		}
	};
	
	
	// OnEditorAction Listener for AutoCompleteTextView
	private TextView.OnEditorActionListener onSearch =

	new TextView.OnEditorActionListener() {
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (event == null || event.getAction() == KeyEvent.ACTION_UP) {
				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
			}
			return (true);
		}
	};

	@Override
	protected void onStop() {
		super.onStop();
		if(mCursor != null){
			stopManagingCursor(mCursor);
		}
	}
	
	

}
