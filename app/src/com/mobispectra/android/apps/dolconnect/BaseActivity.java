/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

import static android.provider.BaseColumns._ID;
import static com.mobispectra.android.apps.dolconnect.db.DbConstants.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import com.mobispectra.android.apps.dolconnect.data.OSHAData.OSHAItem;
import com.mobispectra.android.apps.dolconnect.data.WHDData.WHDItem;
import com.mobispectra.android.apps.dolconnect.db.OSHADBIfc;
import com.mobispectra.android.apps.dolconnect.db.RecentSearchDBIfc;
import com.mobispectra.android.apps.dolconnect.db.WHDDBIfc;
import com.mobispectra.android.apps.dolconnect.tablet.TabletMainActivity;
import com.mobispectra.android.apps.utils.NwAccessIfc;
import com.mobispectra.android.apps.utils.UIUtils;

public class BaseActivity extends Activity {

	// API Key and URL constants
	public static final String API_KEY = "ADD YOUR API KEY";
	public static final String SHARED_SECRET = "ADD YOUR SHARED SECRET";
	public static final String API_HOST = "http://api.dol.gov";
	public static final String API_URI = "/V1";
	public static final String FOOD_SERVICE = "foodService";
	public static final String HOSPITALITY = "hospitality";
	public static final String RETAIL = "retail";
	public static HashMap<String, OSHAItem> OSHAaddressMap = new HashMap<String, OSHAItem>();
	public static HashMap<String, WHDItem> WHDAddressMap = new HashMap<String, WHDItem>();
	public static final int DIALOG_WELCOME_MESSAGE = 1;
	private static final int DIALOG_RECENT_SEARCH = 2;
	public static final int PROGRESS_BAR_DIALOG_ID = 3;
	public static final int NO_RESULT_DIALOG = 4;
	private Cursor mRecentSearchCursor;
	private RecentSearchAdapter mRecentSearchAdapter;
	private final String[] FROM = { _ID, SEARCH_BY, SEARCH_STRING,
			SEARCH_CATEGORY, SEARCH_AGENCY };
	private final int[] TO = { R.id.id, R.id.search_by, R.id.search_string,
			R.id.search_category, R.id.search_agency };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Put your code here
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case PROGRESS_BAR_DIALOG_ID: {
			ProgressDialog dialog = new ProgressDialog(this);
			dialog.setMessage(getResources().getString(R.string.please_wait));
			dialog.setIndeterminate(true);
			dialog.setCancelable(true);
			return dialog;
		}
		case DIALOG_WELCOME_MESSAGE:
			LayoutInflater dialoginflater = LayoutInflater.from(this);
			View dialogLayout = dialoginflater.inflate(R.layout.dialog_web_view, null);
			final WebView webView = (WebView) dialogLayout.findViewById(R.id.dialog_webview);
			webView.loadUrl("file:///android_asset/welcome.html");
			return new AlertDialog.Builder(BaseActivity.this).setTitle(
					R.string.welcome_title).setView(dialogLayout).setIcon(
					R.drawable.dol_app_icon_app_launcher).setPositiveButton(R.string.welcome_enjoy,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int whichButton) {
							
						}
					}).create();
		case DIALOG_RECENT_SEARCH:
			LayoutInflater recentSerachInflater = LayoutInflater.from(this);
			View recentSearchLayout = recentSerachInflater.inflate(R.layout.activity_recent_search, null);
			showRecentSearchData(recentSearchLayout);
			return new AlertDialog.Builder(new ContextThemeWrapper(this,android.R.style.Theme_Holo_Light_Dialog))
					.setTitle(R.string.recent_search).setView(recentSearchLayout)
					.setIcon(R.drawable.dol_app_icon_app_launcher).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int whichButton) {
	                       removeDialog(DIALOG_RECENT_SEARCH);
	                    }
	                }).create();
		case NO_RESULT_DIALOG:
			return new AlertDialog.Builder(BaseActivity.this)
					.setTitle(R.string.no_more_results).setIcon(android.R.drawable.ic_dialog_alert)
					.setMessage(getString(R.string.no_more_results_message))
					.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int whichButton) {
	                    	
	                    }
	                }).create();
	}

		return null;
	}
	
	/**
	 * Convert JSON date object into string date
	 * @param dateStringFromJSON  JSON response of date
	 * @return readableDate as string
	 */
	public static String getReadableDate(String dateStringFromJSON) {
		Log.i(AppConstants.TAG, "BaseActivity :Date From JSON ="+ dateStringFromJSON);
		// Remove prefix and suffix extra string information
		String dateString = dateStringFromJSON.replace("/Date(", "").replace(")/", "");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Long.parseLong(dateString));

		// Convert it to a Date() object now:
		Date date = calendar.getTime();

		// Convert it to a formatted string as you wish:
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String readableDate = null;
		readableDate = formatter.format(date);
		// Show it in the Log window:
		Log.i(AppConstants.TAG,"BaseActivity :Date After Converting to date Object ="+ readableDate);
		return readableDate;
	}

	public void startActivity(String searchBy, String searchText,String category, String agency) {
		Intent intent = null;
		String filterValue = null;
		String searchOption = null;
		String method = null;
		String searchString = null;
		if(searchText == null && agency != null){
			Toast.makeText(getApplicationContext(), R.string.enter_text_for_search,
					Toast.LENGTH_LONG).show();
		}
		if(agency == null && !(searchText == null)){
			Toast.makeText(getApplicationContext(), R.string.select_agency_for_results,
					Toast.LENGTH_LONG).show();
		}
		
		if (agency != null && !(searchText == null)) {

			if (agency.equals(getString(R.string.agency_osha))) {
				OSHADBIfc.deleteOSHAData(getApplicationContext());
				searchString = searchText.toUpperCase(Locale.US).trim();
				// Get searchOption
				searchOption = getSearchOptionForOSHA(searchBy);
				
				intent = new Intent(BaseActivity.this,OSHAViolationsTabActivity.class);	
			} else if (agency.equals(getString(R.string.agency_whd))) {
				WHDDBIfc.deleteWHDData(getApplicationContext());
				searchString = searchText.trim();
				if (searchBy.equals("State")) {
					searchString = searchString.toUpperCase(Locale.US);
				}
				// Get searchOption
				searchOption = getSearchOptionForWHD(searchBy);
				
				intent = new Intent(BaseActivity.this,WHDViolationsTabActivity.class);
			}
			filterValue = searchOption + "'"+ searchString.replaceAll("'", "''") + "'";
			method = "Compliance/" + agency + "/" + category;
			Log.d(AppConstants.TAG, "AppMainActivity: Method : " + method);
			intent.putExtra(AppConstants.SEARCH_STRING, searchString);
			intent.putExtra(AppConstants.SEARCH_BY, searchBy);
			intent.putExtra(AppConstants.SEARCH_CATEGORY, category);
			intent.putExtra(AppConstants.SEARCH_AGENCY, agency);
			intent.putExtra(AppConstants.METHOD, method);
			intent.putExtra(AppConstants.FILTER_VALUE, filterValue);

			// Start new Activity
			startActivity(intent);
		} else if (agency == null && (searchText == null)) {
			Toast.makeText(getApplicationContext(), R.string.enter_text_for_search,
					Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Get the SearchOption for particular SearchBy value for OSHA
	 * 
	 * @param searchBy
	 * 
	 * @return searchOption used to form the filter value to get content from
	 *         webAPI
	 */
	public String getSearchOptionForOSHA(String searchBy) {
		String searchOption = null;
		if (searchBy.equals(getString(R.string.state))) {
			searchOption = "site_state eq ";
		} else if (searchBy.equals(getString(R.string.establishment_name))) {
			searchOption = "estab_name eq ";
		} else if (searchBy.equals(getString(R.string.city))) {
			searchOption = "site_city eq ";
		}
		return searchOption;
	}

	/**
	 * Get the SearchOption for particular SearchBy value for WHD
	 * 
	 * @param searchBy
	 * 
	 * @return searchOption used to form the filter value to get content from
	 *         webAPI
	 */
	public String getSearchOptionForWHD(String searchBy) {
		String searchOption = null;
		if (searchBy.equals(getString(R.string.state))) {
			searchOption = "st_cd eq ";
		} else if (searchBy.equals(getString(R.string.establishment_name))) {
			searchOption = "trade_nm eq ";
		} else if (searchBy.equals(getString(R.string.city))) {
			searchOption = "city_nm eq ";
		}
		return searchOption;
	}
	
	 /** Handle "Home" action. */
    public void onHomeClick(View v) {
    	Log.d(AppConstants.TAG, "BaseActivity : + onHomeClick(View v)");
    	startActivity(new Intent(this, PhoneMainActivity.class));
    }
   
    /** Handle "Search" action. */
    public void onSearchClick(View v){
    	showDialog(DIALOG_RECENT_SEARCH);
    }
	
    // On click listener for recent search in dialog
	private OnItemClickListener recent_list_click_listener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
			// Check for network connectivity
			if (NwAccessIfc.isNetworkConnected(getApplicationContext())) {
				
				// Dismiss dialog of Recent search
				removeDialog(DIALOG_RECENT_SEARCH);
				
				TextView searchByTextView = (TextView) v.findViewById(R.id.search_by);
				String searchBy = (String) searchByTextView.getText();
				TextView searchStringTextView = (TextView) v.findViewById(R.id.search_string);
				String searchString = (String) searchStringTextView.getText();
				TextView searchCategoryTextView = (TextView) v.findViewById(R.id.search_category);
				String category = (String) searchCategoryTextView.getText();
				TextView searchAgencyTextView = (TextView) v.findViewById(R.id.search_agency);
				String agency = (String) searchAgencyTextView.getText();

				// Delete the earlier data from DB
				if (agency.equals(getString(R.string.agency_osha))) {
					OSHADBIfc.deleteOSHAData(getApplicationContext());
				} else if (agency.equals(getString(R.string.agency_whd))) {
					WHDDBIfc.deleteWHDData(getApplicationContext());
				}
				
				startActivity(searchBy, searchString, category, agency);
			} else {
				// Inform User that the Device is not connected to the Network
				Toast.makeText(getApplicationContext(),
						getString(R.string.error_no_network_coverage),
						Toast.LENGTH_LONG).show();
			}
		}

	};
	
    /**
	 * Show Recent Search Data in ListView in dialog
	 * @param View view
	 */
	private void showRecentSearchData(View view) {
		Log.d(AppConstants.TAG, "BaseActivity : + showRecentSearchData ");
		ListView listView = (ListView) view.findViewById(R.id.recent_search_listview);
		TextView noRecentSearchTextView = (TextView) view.findViewById(R.id.no_recent_search_textview);
		
		// Get the cursor of recent searches
		mRecentSearchCursor = RecentSearchDBIfc.getRecentSearches(getApplicationContext());
		if (mRecentSearchCursor != null) {
			startManagingCursor(mRecentSearchCursor);
			
			// Form the adapter
			mRecentSearchAdapter = new RecentSearchAdapter(getApplicationContext(),
					R.layout.list_item_recent_search, mRecentSearchCursor,FROM, TO);
			
			// Set adapter with list view
			listView.setAdapter(mRecentSearchAdapter);
			listView.setOnItemClickListener(recent_list_click_listener);
		}else{
			noRecentSearchTextView.setVisibility(View.VISIBLE);
			noRecentSearchTextView.setText(getString(R.string.no_recent_search));
		}
		Log.d(AppConstants.TAG, "BaseActivity : - showRecentSearchData ");
	}

	
	// Adapter for providing suggestions
	public static class StopsListAdapter extends CursorAdapter implements
			Filterable {
		public StopsListAdapter(Context context, Cursor c) {
			super(context, c);
			mContent = context.getContentResolver();
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			final LayoutInflater inflater = LayoutInflater.from(context);
			final TextView view = (TextView) inflater.inflate(
					R.layout.dropdown_layout, parent, false);
			view.setText(cursor.getString(cursor.getColumnIndex(STATE_NAME)));
			return view;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			((TextView) view).setText(cursor.getString(cursor
					.getColumnIndex(STATE_NAME)));
		}

		@Override
		public String convertToString(Cursor cursor) {
			return cursor.getString(cursor.getColumnIndex(STATE_NAME));
		}

		@Override
		public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
			FilterQueryProvider filter = getFilterQueryProvider();
			if (filter != null) {
				return filter.runQuery(constraint);
			}

			Uri uri = Uri.withAppendedPath(StatesProvider.CONTENT_URI, Uri
					.encode(constraint.toString()));
			return mContent.query(uri, new String[] { _ID, STATE_NAME,
					STATE_CODE }, null, null, null);
		}

		private ContentResolver mContent;
	}
	
	// Adapter for Recent Search
	public static class RecentSearchAdapter extends SimpleCursorAdapter {
		private Context context;
		private final LayoutInflater mLayoutInflater;

		public RecentSearchAdapter(Context context, int layout, Cursor c,
				String[] from, int[] to) {
			super(context, layout, c, from, to);
			this.context = context;
			mLayoutInflater = LayoutInflater.from(context);

		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = mLayoutInflater.inflate(
						R.layout.list_item_recent_search, parent, false);
				holder = new ViewHolder();
				holder.index = (TextView) convertView.findViewById(R.id.index);
				holder.searchString = (TextView) convertView.findViewById(R.id.search_string);
				holder.searchCategory = (TextView) convertView.findViewById(R.id.search_category);
				holder.searchAgency = (TextView) convertView.findViewById(R.id.search_agency);
				holder.searchBy = (TextView) convertView.findViewById(R.id.search_by);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			Cursor cursor = getCursor();
			cursor.moveToPosition(position);
			holder.searchBy.setText(cursor.getString(cursor.getColumnIndex(SEARCH_BY)));
			holder.searchAgency.setText(cursor.getString(cursor.getColumnIndex(SEARCH_AGENCY)));
			holder.searchCategory.setText(cursor.getString(cursor.getColumnIndex(SEARCH_CATEGORY)));	
			holder.searchString.setText(cursor.getString(cursor.getColumnIndex(SEARCH_STRING)));
			holder.index.setText(Integer.toString(position + 1));

			return convertView;
		}
	}

	private static class ViewHolder {
		TextView searchBy;
		TextView searchAgency;
		TextView searchCategory;
		TextView searchString;
		TextView index;	
	}
	
	public void setupActionBar() {
		// Setup the ActionBar title
		if (UIUtils.isHoneycombTablet(getApplicationContext())) {
			ActionBar bar = getActionBar();
			if (bar.isShowing()) {
				// bar.setTitle(R.string.quick_planner);
				bar.setDisplayHomeAsUpEnabled(true);
			}
		}
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mainmenu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case android.R.id.home:
			intent = new Intent(this, TabletMainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		case R.id.settings:
			intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			break;
			
		default:
			return super.onOptionsItemSelected(item);
		}
		return false;
	}
	
}
