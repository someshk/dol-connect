/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mobispectra.android.apps.dolconnect.BaseFragmentActivity.MyDialogFragment;
import com.mobispectra.android.apps.dolconnect.api.DOLDataContext;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequest;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequestCallback;
import com.mobispectra.android.apps.dolconnect.data.FAQSubTopicData;
import com.mobispectra.android.apps.dolconnect.data.FAQSubTopicData.FAQSubTopicItem;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import static com.mobispectra.android.apps.dolconnect.BaseActivity.*;

public class FAQSubTopicFragment extends Fragment implements DOLDataRequestCallback{
	private ListView mListView;
	private ArrayList<FAQSubTopicItem> mFAQSubTopicItemList;
	private FAQSubTopicData mFAQSubTopicData ; 
	private MyAdapter myAdapter;
	private FAQSubTopicItem mFAQSubTopicItem;
	private String mTopicId = null;
	private String mSubTopicId = null;
	private ViewGroup mRootView;
	private MyDialogFragment myDialogFragment;
	
	/** Called when the fragment is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Extract the Extra Data passed to this activity
		Intent intent = BaseFragmentActivity.fragmentArgumentsToIntent(getArguments());
		mTopicId = intent.getStringExtra(AppConstants.FAQ_TOPIC_ID);
		Log.d(AppConstants.TAG, "FAQSubTopicsFragment : TopicID : " + mTopicId);
			
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = (ViewGroup) inflater.inflate(R.layout.activity_faq_sub_topics, null);
		mListView = (ListView) mRootView.findViewById(R.id.faq_sub_topics_listview);
		// On item click listener for list view
		mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mListView.setCacheColorHint(Color.TRANSPARENT);
		
		// Instantiate context object
		DOLDataContext context = new DOLDataContext(API_KEY, SHARED_SECRET,API_HOST, API_URI);

		// Instantiate new request object. Pass the context var that contains
		// all the API key info Set this as the callback responder
		DOLDataRequest request = new DOLDataRequest(this, context);

		// API method to call
		String method = "FAQ/SubTopics";
		
		// Hashmap to store the arguments
		HashMap<String, String> args = new HashMap<String, String>(3);

		// Populate the args into the HashMap
		args.put("top", "20");
		args.put("select", "SubTopicID,SubTopicValue,TopicID");
		args.put("filter", "TopicID eq " + mTopicId);

		// Call the API method
		request.callAPIMethod(method, args);
		
		// Show progress dialog
		myDialogFragment = MyDialogFragment.newInstance(PROGRESS_BAR_DIALOG_ID);
		myDialogFragment.show(getFragmentManager(), "AlertDialog");
		
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long id) {
				mListView.setItemChecked(position, true);
				mFAQSubTopicItem = mFAQSubTopicItemList.get(position);
				mTopicId = mFAQSubTopicItem.getTopicID();
				mSubTopicId = mFAQSubTopicItem.getSubTopicID();
				
				Intent intent = new Intent(getActivity(),FAQTopicQuestionsActivity.class);
				intent.putExtra(AppConstants.FAQ_TOPIC_ID, mTopicId);
				intent.putExtra(AppConstants.FAQ_SUB_TOPIC_ID, mSubTopicId);
				((BaseFragmentActivity) getActivity()).openActivityOrFragment(intent);
			}

		});
		
		return mRootView;
	}
	
	// Callback method called when error occurs
	public void DOLDataErrorCallback(String error) {
		// Dismiss progress dialog
		if (myDialogFragment.isResumed()) {
			myDialogFragment.dismiss();
		}
		// Show error on dialog
		AlertDialog alertDialog;
		alertDialog = new AlertDialog.Builder(getActivity()).create();
		alertDialog.setTitle("Error");
		alertDialog.setMessage(error);
		alertDialog.show();
	}

	// Callback method called when results are returned and parsed
	public void DOLDataResultsCallback(List<Map<String, String>> results) {
		mFAQSubTopicData = new FAQSubTopicData();
		// Dismiss progress dialog
		if (myDialogFragment.isResumed() && myDialogFragment.isAdded()) {
			try {
				myDialogFragment.dismiss();
			} catch (Exception e) {
				Log.e(AppConstants.TAG, "FAQSubTopicFragment : DOLDataRequestCallback Exception in dialog : "+ e);
			}
		}
		// Iterate thru List of results. Add each field to the data structure of FAQ SubTopics
		if (results.size() != 0) {
			for (Map<String, String> m : results) {
			
				mFAQSubTopicItem = mFAQSubTopicData.new FAQSubTopicItem(m
						.get("SubTopicID"), m.get("SubTopicValue"), m.get("TopicID"));
				mFAQSubTopicData.addData(mFAQSubTopicItem);
			}
			// Display data
			if (mFAQSubTopicData != null) {
				displayFAQSubTopicsItems(mFAQSubTopicData);
			}
		mListView.setTextFilterEnabled(true);
		}else {
			Toast.makeText(getActivity(),getString(R.string.no_results_found),
					Toast.LENGTH_LONG).show();
		}

	}

	private class MyAdapter extends ArrayAdapter<FAQSubTopicItem> {
		private LayoutInflater mLayoutInflater;

		public MyAdapter(Context context, int textViewResourceId,ArrayList<FAQSubTopicItem> mFAQSubTopicItemList) {
			super(context, textViewResourceId);
			mLayoutInflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = mLayoutInflater.inflate(R.layout.list_item_faq_topics, parent, false);

				holder = new ViewHolder();

				holder.sub_topic_name = (TextView) convertView.findViewById(R.id.faq_topic_name);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			FAQSubTopicItem fAQSubTopicItem = getItem(position);

			holder.sub_topic_name.setText(fAQSubTopicItem.getSubTopicValue());

			return convertView;
		}

	}

	private static class ViewHolder {

		TextView sub_topic_name;
	}

	// Display the data in array adapter
	public void displayFAQSubTopicsItems(FAQSubTopicData fAQSubTopicData) {
		Log.d(AppConstants.TAG,"FAQSubTopicsFragment : +displayFAQSubTopicsItems");
		mFAQSubTopicData = fAQSubTopicData;
		if(getActivity() != null){
			if (mFAQSubTopicData != null) {
				mFAQSubTopicItemList = mFAQSubTopicData.getData();
			}
			if (mFAQSubTopicItemList != null&& mFAQSubTopicItemList.isEmpty() == false) {
				if (myAdapter == null) {
					myAdapter = new MyAdapter(getActivity(),R.layout.list_item_osha, mFAQSubTopicItemList);
				}
				myAdapter.clear();
				for (FAQSubTopicItem item : mFAQSubTopicItemList) {
					myAdapter.add(item);
				}
				if (myAdapter.isEmpty()) {
					Toast.makeText(getActivity(), R.string.fatal_error,Toast.LENGTH_SHORT).show();
				} else {
					mListView.setAdapter(myAdapter);
				}
			}
		Log.d(AppConstants.TAG,"FAQSubTopicsFragment : -displayFAQSubTopicsItems");
	}
	}
	
	

}
