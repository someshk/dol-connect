/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SendEmailActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
	
		// Set up Email Data
		emailIntent.setType("plain/text");
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"apps@mobispectra.com"});
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.feedback_email_title));
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString(R.string.feedback_email_text));
	
		// Fire the Intent to start Email Activity
		this.startActivity(Intent.createChooser(emailIntent, "Choose Email Account.."));
		
		// Close this Activity
		finish();
	}
}
