/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mobispectra.android.apps.dolconnect.BaseFragmentActivity.MyDialogFragment;
import com.mobispectra.android.apps.dolconnect.api.DOLDataContext;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequest;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequestCallback;
import com.mobispectra.android.apps.dolconnect.data.FAQTopicsData;
import com.mobispectra.android.apps.dolconnect.data.FAQTopicsData.FAQTopicsItem;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import static com.mobispectra.android.apps.dolconnect.BaseActivity.*;

public class FAQTopicsFragment extends ListFragment implements DOLDataRequestCallback {
	private ListView mListView;
	private ArrayList<FAQTopicsItem> mFAQTopicsItemList;
	private FAQTopicsData mFAQTopicsData = new FAQTopicsData();
	private MyAdapter myAdapter;
	private FAQTopicsItem mFAQTopicsItem;
	private String mTopicId;
	private MyDialogFragment myDialogFragment;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mListView = getListView();
		mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mListView.setCacheColorHint(Color.TRANSPARENT);
		
		// Instantiate context object
		DOLDataContext context = new DOLDataContext(API_KEY, SHARED_SECRET,API_HOST, API_URI);

		// Instantiate new request object. Pass the context var that contains all the API key info
		// Set this as the callback responder
		DOLDataRequest request = new DOLDataRequest(this, context);
		
		// API method to call
		String method = "FAQ/Topics";

		// Hashmap to store the arguments
		HashMap<String, String> args = new HashMap<String, String>(3);

		// Populate the args into the HashMap
		args.put("top", "20");
		args.put("select", "TopicID,TopicValue");

		if (getRetainInstance()) {
			displayFAQTopicsItems(mFAQTopicsData);
		} else {
			// Call the API method
			request.callAPIMethod(method, args);
			// Show progress dialog
			myDialogFragment = MyDialogFragment.newInstance(PROGRESS_BAR_DIALOG_ID);
			myDialogFragment.show(getFragmentManager(), "AlertDialog");
			setRetainInstance(true);
		}		
		
	}


	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		mListView.setItemChecked(position, true);
		mFAQTopicsItem = mFAQTopicsItemList.get(position);
		mTopicId = mFAQTopicsItem.getTopicID();
		Intent intent = new Intent(getActivity(),FAQSubTopicsActivity.class);
		intent.putExtra(AppConstants.FAQ_TOPIC_ID, mTopicId);
		((BaseFragmentActivity) getActivity()).openActivityOrFragment(intent);
	}



	// Callback method called when error occurs
	public void DOLDataErrorCallback(String error) {
		// Dismiss progress dialog
		if (myDialogFragment.isResumed()) {
			myDialogFragment.dismiss();
		}
		// Show error on dialog
		AlertDialog alertDialog;
		alertDialog = new AlertDialog.Builder(getActivity()).create();
		alertDialog.setTitle("Error");
		alertDialog.setMessage(error);
		alertDialog.show();
	}

	// Callback method called when results are returned and parsed
	public void DOLDataResultsCallback(List<Map<String, String>> results) {
		// Dismiss progress dialog
		if (myDialogFragment.isResumed() && myDialogFragment.isAdded()) {
			try {
				myDialogFragment.dismiss();
			} catch (Exception e) {
				Log.e(AppConstants.TAG, "FAQTopicsFragment : DOLDataRequestCallback Exception in dialog : "+ e);
			}
		}
		if (results.size() != 0) {
			// Iterate thru List of results. Add each field to the data structure of FAQ Topics
			for (Map<String, String> m : results) {
				mFAQTopicsItem = mFAQTopicsData.new FAQTopicsItem(m.get("TopicID"), m.get("TopicValue"));
				mFAQTopicsData.addData(mFAQTopicsItem);
			}
			// Display data
			if (mFAQTopicsData != null) {
				displayFAQTopicsItems(mFAQTopicsData);
			}
			mListView.setTextFilterEnabled(true);
		}else {
			Toast.makeText(getActivity(),getString(R.string.no_results_found),
					Toast.LENGTH_LONG).show();
		}

	}

	private class MyAdapter extends ArrayAdapter<FAQTopicsItem> {
		private LayoutInflater mLayoutInflater;

		public MyAdapter(Context context, int textViewResourceId,ArrayList<FAQTopicsItem> mFAQTopicsItemList) {
			super(context, textViewResourceId);
			mLayoutInflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = mLayoutInflater.inflate(R.layout.list_item_faq_topics, parent, false);

				holder = new ViewHolder();
				holder.topic_name = (TextView) convertView.findViewById(R.id.faq_topic_name);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			FAQTopicsItem fAQTopicsItem = getItem(position);
			holder.topic_name.setText(fAQTopicsItem.getTopicValue());

			return convertView;
		}

	}

	private static class ViewHolder {
		TextView topic_name;
	}

	// Display the data in array adapter
	public void displayFAQTopicsItems(FAQTopicsData fAQTopicsData) {
		Log.d(AppConstants.TAG,"FAQTopicsFragment : + displayFAQTopicsItems");
		mFAQTopicsData = fAQTopicsData;
		if (getActivity() != null) {
			if (mFAQTopicsData != null) {
				mFAQTopicsItemList = mFAQTopicsData.getData();
			}
			if (mFAQTopicsItemList != null
					&& mFAQTopicsItemList.isEmpty() == false) {
				if (myAdapter == null) {
					myAdapter = new MyAdapter(getActivity(),R.layout.list_item_osha, mFAQTopicsItemList);
				}
				myAdapter.clear();
				for (FAQTopicsItem item : mFAQTopicsItemList) {
					myAdapter.add(item);
				}
				if (myAdapter.isEmpty()) {
					Toast.makeText(getActivity(), R.string.fatal_error,Toast.LENGTH_SHORT).show();
				} else {
					mListView.setAdapter(myAdapter);
				}
			}
			Log.d(AppConstants.TAG,"FAQTopicsFragment : - displayFAQTopicsItems");
		}
	}

}
