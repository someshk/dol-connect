/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.google.android.maps.GeoPoint;
import com.mobispectra.android.apps.dolconnect.BaseFragmentActivity.MyDialogFragment;
import com.mobispectra.android.apps.dolconnect.api.DOLDataContext;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequest;
import com.mobispectra.android.apps.dolconnect.api.DOLDataRequestCallback;
import com.mobispectra.android.apps.dolconnect.data.OSHAData;
import com.mobispectra.android.apps.dolconnect.data.OSHAData.OSHAItem;
import com.mobispectra.android.apps.dolconnect.db.GeographyDataDBIfc;
import com.mobispectra.android.apps.dolconnect.db.OSHADBIfc;
import com.mobispectra.android.apps.dolconnect.db.RecentSearchDBIfc;
import static com.mobispectra.android.apps.dolconnect.BaseActivity.*;

public class OSHAResultFragment extends Fragment implements DOLDataRequestCallback {
	private ListView mListView;
	private ArrayList<OSHAItem> mOSHAItemList;
	private OSHAData mOshaData;
	private MyAdapter myAdapter;
	ArrayList<String> mAddressList;
	private OSHAItem mOSHAItem;
	private String mSearchString;
	private String mMethod;
	private TextView mNoResultTextView;
	private TextView mSearchResultTextView;
	private String mFilterValue;
	private String mSearchBy;
	private String mCategory;
	private String mAgency;
	private ViewGroup mRootView;
	private MyDialogFragment fragment;
	private Button mNextButton;
	private Button mPrevButton;
	private int mSkipResults = 0;
	private int mShownIndex = 1;
	private int mItemIndex = 0;
	private int mResultSetCounter = 100;
	private int mDisplayedResults = 0;
	private boolean mShowNext;
	private boolean mShowPrevious;
	private OnResultSelectedListener onResultSelectedListener;
	private boolean mRunning;
	private int mTotalResults = 0;
	private boolean mTaskRunning;
	
	public OSHAResultFragment(String searchString, String searchBy,String category,
			String agency, String method,String filterValue) {
		mSearchString = searchString;
		mSearchBy = searchBy;
		mCategory = category;
		mAgency = agency;
		mMethod = method;
		mFilterValue = filterValue;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = (ViewGroup) inflater.inflate(R.layout.activity_osha_food, null);
		mListView = (ListView) mRootView.findViewById(R.id.food_listview);
		mNoResultTextView = (TextView) mRootView.findViewById(R.id.noresult_textview);
		mSearchResultTextView = (TextView) mRootView.findViewById(R.id.search_result_textview);
		mNextButton = (Button) mRootView.findViewById(R.id.get_next_results);
		mPrevButton = (Button) mRootView.findViewById(R.id.get_previous_results);
		mListView .setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mListView .setCacheColorHint(Color.TRANSPARENT);
		if (getRetainInstance()) {
			displayOSHAItems(mOshaData);
			handleButtonVisibilty();
		}else{
			setRetainInstance(true);
			getNextResults(mResultSetCounter, mSkipResults);
		}
		
		// Handles list item click
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				mListView.setItemChecked(position, true);
				mOSHAItem = mOSHAItemList.get(position);
				Intent intent = new Intent(getActivity(),OSHADetailActivity.class);
				intent.putExtra("NAME",mOSHAItem.getName());
				intent.putExtra("SITE_ADDRESS",mOSHAItem.getAddress());
				intent.putExtra("STATE",mOSHAItem.getState());
				intent.putExtra("CITY",mOSHAItem.getCity());
				intent.putExtra("ZIP_CODE",mOSHAItem.getZipCode());
				intent.putExtra("NAICS_CODE",mOSHAItem.getNaics_code(getActivity()));
				intent.putExtra("PENALTY",mOSHAItem.getPenalty(getActivity()));
				intent.putExtra("OPEN_DATE",mOSHAItem.getOpen_date(getActivity()));
				intent.putExtra("OSHA_VIOLATION_INDICATOR",mOSHAItem.getOsha_violation_indicator(getActivity()));
				intent.putExtra("TOTAL_VIOLATIONS",mOSHAItem.getTotal_violations(getActivity()));
				intent.putExtra("SERIOUS_VIOLATIONS",mOSHAItem.getSerious_violations(getActivity()));
				intent.putExtra("INSPECTION_TYPE",mOSHAItem.getInsp_type(getActivity()));
				intent.putExtra("LOAD_DATE",mOSHAItem.getLoad_dt(getActivity()));
				// startActivity(intent);
				String address = mOSHAItem.getAddress() + "," + mOSHAItem.getCity() + ","
				+ mOSHAItem.getState() + "," + mOSHAItem.getZipCode();
				Log.d(AppConstants.TAG,"OshaResultFragment: Address"+address);
				onResultSelectedListener.onResultListItemClicked(address);
			}
		});
		
		// OnClick Listener for next button
		mNextButton.setOnClickListener(new OnClickListener() {		

			@Override
			public void onClick(View v) {
				Log.d(AppConstants.TAG,"OSHAResultFragment : + onNextClick : Index = " + mShownIndex);
				mShowPrevious = true;
				mOshaData = OSHADBIfc.getOSHAItemsFromDB(getActivity(), (mShownIndex + mDisplayedResults));
				
				// Display data
				if (mOshaData != null) {
					displayOSHAItems(mOshaData);
					handleButtonVisibilty();
					mShownIndex = mShownIndex + mDisplayedResults;
				} else {
					getNextResults(mResultSetCounter, mSkipResults);
				}
				// Disable the next button if Async Task is in progress
				if (mTaskRunning) {
					mNextButton.setEnabled(false);
				}
				Log.d(AppConstants.TAG,"OSHAResultFragment : - onNextClick : Index = " + mShownIndex);
			}
		});
		
		// OnClick Listener for previous button
		mPrevButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(AppConstants.TAG,"OSHAResultFragment : + onPrevClick : Index = " + mShownIndex);
				mShownIndex = mShownIndex - mDisplayedResults;
				if(mShownIndex == 1){
					mShowPrevious = false;
				}
				mOshaData = OSHADBIfc.getOSHAItemsFromDB(getActivity(), mShownIndex);
				
				// Display data
				if (mOshaData != null) {
					displayOSHAItems(mOshaData);
					handleButtonVisibilty();
				}
				Log.d(AppConstants.TAG,"OSHAResultFragment : - onPrevClick : Index = " + mShownIndex);
			}
		});
		
		return mRootView;
	}
	
	private void getNextResults(int results,int skip){
		Log.d(AppConstants.TAG,"OSHAResultFragment : + getNextResults");
		Log.d(AppConstants.TAG,"OSHAResultFragment : getNextResults : SkipValue = " + skip);
		
		// Instantiate context object
		DOLDataContext context = new DOLDataContext(API_KEY, SHARED_SECRET,API_HOST, API_URI);

		// Instantiate new request object. Pass the context var that contains all the API key info
		// Set this as the callback responder
		DOLDataRequest request = new DOLDataRequest(this, context);

		// API method to call
		// String method = "Compliance/OSHA/foodService";

		// Hashmap to store the arguments
		HashMap<String, String> args = new HashMap<String, String>(4);

		// Populate the args into the HashMap
		args.put("top", Integer.toString(results));
		args.put("select","activity_nr,estab_name,site_address,site_city,site_state,site_zip,naics_code," +
				"insp_type,open_date,total_current_penalty,osha_violation_indicator,serious_violations," +
				"total_violations,load_dt");
		args.put("filter",mFilterValue);
		args.put("skip", Integer.toString(skip));

		// Display progress bar dialog
		fragment = MyDialogFragment.newInstance(PROGRESS_BAR_DIALOG_ID);
		fragment.show(getFragmentManager(), "AlertDialog");
		
		// Call the API method
		request.callAPIMethod(mMethod, args);
		Log.d(AppConstants.TAG,"OSHAResultFragment : - getNextResults");
	}
	
	// Callback method called when error occurs
	public void DOLDataErrorCallback(String error) {
		// Dismiss progress bar dialog
		if (fragment.isResumed() && fragment.isAdded()) {
			fragment.dismiss();
		}
		// Show error on dialog
		AlertDialog alertDialog;
		alertDialog = new AlertDialog.Builder(getActivity()).create();
		alertDialog.setTitle("Error");
		alertDialog.setMessage(error);
		alertDialog.show();
	}

	// Callback method called when results are returned and parsed
	@SuppressWarnings("unchecked")
	public void DOLDataResultsCallback(List<Map<String, String>> results) {
		Log.d(AppConstants.TAG,"OSHAResultFragment : DOLDataResultsCallback : No. of Results : " + results.size());
		if (results.size() != 0) {
			String state = null;
			String category = null;
			mTotalResults = mTotalResults + results.size();
			
			if (mCategory.equals(BaseActivity.FOOD_SERVICE)) {
				category = getString(R.string.food_service);
			} else if (mCategory.equals(BaseActivity.HOSPITALITY)) {
				category = getString(R.string.hospitality);
			} else if (mCategory.equals(BaseActivity.RETAIL)) {
				category = getString(R.string.retail);
			}
			
			// If searchBy is state then display State Name
			if(mSearchBy.equals(getString(R.string.state))){
				state = GeographyDataDBIfc.getStateName(mSearchString);
				// Set text for Result text view
				mSearchResultTextView.setText(mAgency + " " + category + " "+ getString(R.string.violations) 
						+ " "+ getString(R.string.violations_for) + " " + state);
			} else {
				// Set text for Result text view
				mSearchResultTextView.setText(mAgency + " " + category + " "+ getString(R.string.violations) + " "
						+ getString(R.string.violations_for) + " "+ mSearchString);
			}
			
			// add recent search data to DB
			RecentSearchDBIfc.addRecentSerachData(getActivity(),
					mSearchBy, mSearchString, mCategory, mAgency);
			
			mTaskRunning = true;
			// Start the AsyncTask to process the received data
			new ProcessData().execute(results);
		} else if (mNextButton.isShown()) {
			// If no results are available to show and next button is showing then make it invisible
			mNextButton.setVisibility(View.INVISIBLE);
			
			// Dismiss progress bar dialog
			if (fragment.isResumed() && fragment.isAdded()) {
				try {
					fragment.dismiss();
				} catch (Exception e) {
					Log.e(AppConstants.TAG, "OSHAResultFragment : DOLDataRequestCallback : Exception in dialog : "+ e);
				}
			}
			// Display no result dialog
			fragment = MyDialogFragment.newInstance(NO_RESULT_DIALOG);
			fragment.show(getFragmentManager(), "No Result Dialog");
		}else {
			// Dismiss progress bar dialog
			if (fragment.isResumed() && fragment.isAdded()) {
				try {
					fragment.dismiss();
				} catch (Exception e) {
					Log.e(AppConstants.TAG, "OSHAResultFragment : DOLDataRequestCallback : Exception in dialog : "+ e);
				}
			}
			Toast.makeText(getActivity(), getString(R.string.no_result_warning), Toast.LENGTH_LONG).show();
			mNoResultTextView.setVisibility(View.VISIBLE);
			mNoResultTextView.setText(R.string.no_result_warning);
		}

	}

	// Async task to do the geoCoding and insert data in DB
	private class ProcessData extends AsyncTask<List<Map<String, String>>, OSHAData, Void> {
		OSHAData oshaData ;
		
		@Override
		protected Void doInBackground(List<Map<String, String>>... results) {
			String address = null;
			oshaData = new OSHAData();
			Geocoder geoCoder = null;
			if (getActivity() != null) {
				geoCoder = new Geocoder(getActivity(), Locale.getDefault());
			}
			
			// Iterate thru List of results. Add each field to the data structure of OSHAData
			for (Map<String, String> m : results[0]) {
				Log.d(AppConstants.TAG," ProcessData : doInBackground :Running  = " + mRunning);
				if (mRunning) {
					break;
				} else {
					mItemIndex++;
					GeoPoint point = null;
					address = m.get("site_address") + "," + m.get("site_city")
							+ " ," + m.get("site_state") + " ,"+ m.get("site_zip");
					try {
						Log.d(AppConstants.TAG,"OSHAResultFragment : ProcessData : Address : "+ address);
						// Use GeoCoding to get the GeoPoints for every Address
						List<Address> addresses = geoCoder.getFromLocationName(address, 5);
						if (addresses.size() > 0) {
							point = new GeoPoint((int) (addresses.get(0).getLatitude() * 1E6),
									(int) (addresses.get(0).getLongitude() * 1E6));

							Log.d(AppConstants.TAG,"OSHAResultFragment : ProcessData : Point : "+ point);
						} else {
							Log.e(AppConstants.TAG,"OSHAResultFragment : ProcessData : "
											+ "Unable to get the GeoPoints for = "+ address);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}

					// Initialise the OSHAItem Constructor
					mOSHAItem = oshaData.new OSHAItem(mItemIndex, m.get("activity_nr"), m.get("estab_name")
							, m.get("site_address"), m.get("site_city"), m.get("site_state"), m.get("site_zip")
							, m.get("naics_code"), m.get("insp_type"),getReadableDate(m.get("open_date"))
							, m.get("total_current_penalty"), m.get("osha_violation_indicator")
							, m.get("serious_violations"), m.get("total_violations"), getReadableDate(m.get("load_dt")), point);
					
					oshaData.addData(mOSHAItem);


					if (mItemIndex % 10 == 0) {
						if (mRunning) {
							break;
						} else {
							if (getActivity() != null) {
								// Add data to the DB
								OSHADBIfc.addOSHAData(getActivity(), oshaData);
								publishProgress(oshaData);
								oshaData = new OSHAData();
							}
						}
					}
				}
			}
			if (getActivity() != null) {
				if (mTotalResults % 10 != 0) {
					// Add data to the DB
					OSHADBIfc.addOSHAData(getActivity(), oshaData);
					publishProgress(oshaData);
				}
			}

			return null;

		}
		
		
		@Override
		protected void onCancelled() {
			super.onCancelled();
			Log.d(AppConstants.TAG,"ProcessData : OnCancelled()");
			mRunning = true;

			// Dismiss progress bar dialog
			if (fragment.isResumed() && fragment.isAdded()) {
				try {
					fragment.dismiss();
				} catch (Exception e) {
					Log.e(AppConstants.TAG, "ProcessData : onCancelled : Exception in dialog : "+ e);
				}
			}	
		}


		@Override
		protected synchronized void onProgressUpdate(OSHAData... values) {
			super.onProgressUpdate(values);
			Log.d(AppConstants.TAG,"OSHAResultFragment : + onProgressUpdate");
			Log.d(AppConstants.TAG,"onProgressUpdate : mItemIndex = " + mItemIndex);
			Log.d(AppConstants.TAG,"onProgressUpdate : mSkipResults = " + mSkipResults);
			
			// Dismiss progress bar dialog
			if (fragment.isResumed() && fragment.isAdded()) {
				try {
					fragment.dismiss();
				} catch (Exception e) {
					Log.e(AppConstants.TAG, "ProcessData : onProgressUpdate : Exception in dialog : "+ e);
				}
			}
			// Display first ten results of the result set in the listview
			if (mItemIndex <= (11 + mSkipResults)) {
				mNextButton.setVisibility(View.INVISIBLE);
				// Display data
				if (values[0] != null) {
					displayOSHAItems(values[0]);
				}
				mShowNext = true;
				
			// Make the next button visible after inserting more than 20 results in DB
			} else if(mItemIndex > (11 + mSkipResults)){
				mNextButton.setVisibility(View.VISIBLE);
			}
			mNextButton.setEnabled(true);
			mListView.setTextFilterEnabled(true);
			Log.d(AppConstants.TAG,"OSHAResultFragment : - onProgressUpdate");
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			// False value indicate that AsyncTask is over and not running now
			mTaskRunning = false;
			handleButtonVisibilty();
			mSkipResults = mSkipResults + mResultSetCounter;
		}
		
	}
	

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			onResultSelectedListener = (OnResultSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement  onOSHAResultSelectedListener");
		}
	}
    
	private class MyAdapter extends ArrayAdapter<OSHAItem> {
		private LayoutInflater mLayoutInflater;

		public MyAdapter(Context context, int textViewResourceId,ArrayList<OSHAItem> mOSHAItemList) {
			super(context, textViewResourceId);
			mLayoutInflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = mLayoutInflater.inflate(R.layout.list_item_osha,parent, false);

				holder = new ViewHolder();
				holder.index = (TextView) convertView.findViewById(R.id.index);
				holder.establishment_name = (TextView) convertView.findViewById(R.id.establishment_name);
				holder.establishment_address = (TextView) convertView.findViewById(R.id.establishment_address);
				holder.establishment_penalty = (TextView) convertView.findViewById(R.id.establishment_penalty);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			OSHAItem OSHAItem = getItem(position);
			// holder.establishment_image.setBackgroundResource(R.drawable.icon);
			holder.establishment_name.setText(OSHAItem.getName());
			holder.establishment_address.setText(OSHAItem.getAddress() + " , "
					+ OSHAItem.getCity() + " , " + OSHAItem.getState() + " , "
					+ OSHAItem.getZipCode());
			holder.establishment_penalty.setText("Penalty : "+ OSHAItem.getPenalty(getActivity()));
			holder.index.setText(Integer.toString(OSHAItem.getIndex()));

			return convertView;
		}

	}

	private static class ViewHolder {
		TextView index;
		TextView establishment_name;
		TextView establishment_address;
		TextView establishment_penalty;
	}

	// Display the data in array adapter
	public synchronized void displayOSHAItems(OSHAData oshaData) {
		Log.d(AppConstants.TAG, "OSHAResultFragment : + displayOSHAItems");
		mOshaData = oshaData;
		mAddressList = new ArrayList<String>();
		String address = null;

		if (mOshaData != null) {
			mOSHAItemList = mOshaData.getData();
		}
		if (mOSHAItemList != null && mOSHAItemList.isEmpty() == false) {
			mDisplayedResults = mOSHAItemList.size();
			if (myAdapter == null && getActivity() != null) {
				myAdapter = new MyAdapter(getActivity(),
						R.layout.list_item_osha, mOSHAItemList);
			}
			myAdapter.clear();
			for (OSHAItem item : mOSHAItemList) {
				myAdapter.add(item);
				address = item.getAddress() + "," + item.getCity() + ","
						+ item.getState() + "," + item.getZipCode();
				
				if (item.getPoint() != null) {
					// Add address to arrayList
					mAddressList.add(address);
				}
				// HashMap with Address as key and OSHAItem object as value
				OSHAaddressMap.put(address, item);
			}
			if (myAdapter.isEmpty()) {
				Toast.makeText(getActivity(), R.string.fatal_error, Toast.LENGTH_SHORT).show();
			} else {
				mListView.setAdapter(myAdapter);
			}
		}

		// Inform the map fragment of changed data
		onResultSelectedListener.onNextSelected(mAddressList);
		Log.d(AppConstants.TAG, "OSHAResultFragment : - displayOSHAItems");
	}

	/** 
	 * Handle visibility of next and previous button on the basis of results
	 */
	private void handleButtonVisibilty() {
		if (mShowNext && mDisplayedResults == 10) {
			mNextButton.setEnabled(true);
			mNextButton.setVisibility(View.VISIBLE);
		} else {
			mNextButton.setVisibility(View.INVISIBLE);
		}
		if (mShowPrevious) {
			mPrevButton.setVisibility(View.VISIBLE);
		} else {
			mPrevButton.setVisibility(View.INVISIBLE);
		}

	}
	
	
	@Override
	public synchronized void onPause() {
		super.onPause();
		Log.d(AppConstants.TAG,"OSHAResultFragment : + onPause()");
		ProcessData processData = new ProcessData();
		if (processData.getStatus() != AsyncTask.Status.FINISHED) {
			processData.cancel(true);
		}
	}	
	
}
