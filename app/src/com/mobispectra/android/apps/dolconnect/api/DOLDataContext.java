/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.api;

public class DOLDataContext {
	private String apiKey;
	private String apiSecret;
	private String apiHost;
	private String apiURI;
	
	/**
	 * @param apiKey
	 * @param apiSecret
	 * @param apiHost
	 * @param apiURI
	 */
	public DOLDataContext(String apiKey, String apiSecret, String apiHost,
			String apiURI) {
		super();
		this.apiKey = apiKey;
		this.apiSecret = apiSecret;
		this.apiHost = apiHost;
		this.apiURI = apiURI;
	}
	
	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}
	/**
	 * @param apiKey the apiKey to set
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	/**
	 * @return the apiSecret
	 */
	public String getApiSecret() {
		return apiSecret;
	}
	/**
	 * @param apiSecret the apiSecret to set
	 */
	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}
	/**
	 * @return the apiHost
	 */
	public String getApiHost() {
		return apiHost;
	}
	/**
	 * @param apiHost the apiHost to set
	 */
	public void setApiHost(String apiHost) {
		this.apiHost = apiHost;
	}
	/**
	 * @return the apiURI
	 */
	public String getApiURI() {
		return apiURI;
	}
	/**
	 * @param apiURI the apiURI to set
	 */
	public void setApiURI(String apiURI) {
		this.apiURI = apiURI;
	}
}
