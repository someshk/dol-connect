/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import static com.mobispectra.android.apps.dolconnect.BaseActivity.*;

public class BaseFragmentActivity extends FragmentActivity {

	/**
	 * Takes a given intent and either starts a new activity to handle it (the
	 * default behavior), or creates/updates a fragment (in the case of a
	 * multi-pane activity) that can handle the intent.
	 * 
	 * Must be called from the main (UI) thread.
	 */
	public void openActivityOrFragment(Intent intent) {
		// Default implementation simply calls startActivity
		startActivity(intent);
	}

	/**
	 * Converts an intent into a {@link Bundle} suitable for use as fragment
	 * arguments.
	 */
	public static Bundle intentToFragmentArguments(Intent intent) {
		Bundle arguments = new Bundle();
		if (intent == null) {
			return arguments;
		}

		final Uri data = intent.getData();
		if (data != null) {
			arguments.putParcelable("_uri", data);
		}

		final Bundle extras = intent.getExtras();
		if (extras != null) {
			arguments.putAll(intent.getExtras());
		}

		return arguments;
	}

	/**
	 * Converts a fragment arguments bundle into an intent.
	 */
	public static Intent fragmentArgumentsToIntent(Bundle arguments) {
		Intent intent = new Intent();
		if (arguments == null) {
			return intent;
		}

		final Uri data = arguments.getParcelable("_uri");
		if (data != null) {
			intent.setData(data);
		}

		intent.putExtras(arguments);
		intent.removeExtra("_uri");
		return intent;
	}


	public static class MyDialogFragment extends DialogFragment {
		int mId;

		public static MyDialogFragment newInstance(int id) {
			MyDialogFragment myDialogFragment = new MyDialogFragment();
			Bundle args = new Bundle();
			args.putInt("id", id);
			myDialogFragment.setArguments(args);
			return myDialogFragment;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			mId = getArguments().getInt("id");
			switch (mId) {
			case PROGRESS_BAR_DIALOG_ID: {
				ProgressDialog dialog = new ProgressDialog(getActivity());
				dialog.setMessage(getResources().getString(R.string.please_wait));
				dialog.setIndeterminate(true);
				dialog.setCancelable(true);
				return dialog;
			}
			case NO_RESULT_DIALOG:
				return new AlertDialog.Builder(getActivity()).setTitle(
						R.string.no_more_results).setIcon(
						android.R.drawable.ic_dialog_alert).setMessage(
						getString(R.string.no_more_results_message))
						.setPositiveButton(R.string.ok,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {

									}
								}).create();
			}
			return null;
		}
	}

}
