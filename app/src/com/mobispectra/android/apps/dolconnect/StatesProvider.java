/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect;

import com.mobispectra.android.apps.dolconnect.db.GeographyDataDBIfc;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public class StatesProvider extends ContentProvider {
	
	public static String AUTHORITY = "com.mobispectra.android.apps.dolconnect.StatesProvider";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY+ "/DOL/states");
	
	private GeographyDataDBIfc mGeographyDataDBIfc;

	// UriMatcher stuff
	private static final int GET_STATES = 1;
	private static final int GET_SUGGESTION = 2;
	private static final UriMatcher sURIMatcher = buildUriMatcher();

	@Override
	public boolean onCreate() {
		mGeographyDataDBIfc = new GeographyDataDBIfc(getContext());
		return true;
	}

	/**
	 * Builds up a UriMatcher for search suggestion and shortcut refresh
	 * queries.
	 */
	private static UriMatcher buildUriMatcher() {
		UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
		matcher.addURI(AUTHORITY, "DOL/states", GET_STATES);
		matcher.addURI(AUTHORITY, "DOL/states/*", GET_SUGGESTION);

		return matcher;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Log.d(AppConstants.TAG, "StatesProvider : Inside Provider");
		Log.d(AppConstants.TAG, "Value : " + sURIMatcher.match(uri));
		// Use the UriMatcher to see what kind of query we have and format the
		// db query accordingly
		switch (sURIMatcher.match(uri)) {
		
		case GET_STATES:
			return getStops(projection,selection,selectionArgs);
		case GET_SUGGESTION:
			return getSuggestions(uri,projection);
		default:
			throw new IllegalArgumentException("Unknown Uri: " + uri);
		}
	}

	/**
     * Returns a Cursor 
     *
     * @param columns The columns to return
     * @param selection The selection clause
     * @param selectionArgs Selection arguments for "?" components in the selection
     * @return Cursor 
     */
	private Cursor getStops(String[] columns,String selection,String[] selectionArgs) {
		return mGeographyDataDBIfc.getStatesFromDB(columns,selection,selectionArgs);
	}
	
	/**
     * Returns a Cursor over all stops that match the given query
     *
     * @param uri URI
	 * @param projection 
     * @return Cursor over all stops that match, or null if none found.
     */
	private Cursor getSuggestions(Uri uri, String[] projection) {
		String query = uri.getLastPathSegment();
		query = query.toLowerCase().replaceAll("'","''");
		return mGeographyDataDBIfc.getWordMatches(query, projection);
	}

	// Other required implementations...

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

}
