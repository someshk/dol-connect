/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect;

public class AppConstants {

	public static final String TAG = "DOLConnect";
	public static final String FEATURE_NAME = "feature_name";
	public static final String FEATURE_ICON = "feature_icon";
	public static final String ADDRESS = "address";
	public static final String SEARCH_STRING = "search_string";
	public static final String METHOD = "Method";
	public static final String FILTER_VALUE = "filter_value";
	public static final String SEARCH_BY = "search_by";
	public static final String SEARCH_CATEGORY = "search_category";
	public static final String SEARCH_AGENCY = "search_agency";
	public static final String FAQ_TOPIC_ID = "faq_topic_id";
	public static final String FAQ_SUB_TOPIC_ID = "faq_sub_topic_id";
	public static final String DATA_AVAILABLE = "data_avaliable";
	public static final String APP_WEB_URL="facebook_url";
	
	
	
	public class WebUrl {
		public static final String MOBISPECTRA_FACEBOOK_URL="http://www.facebook.com/pages/Mobispectra/179486289548?sk=wall";
		public static final String FEEDBACK_FORM_URL="https://docs.google.com/spreadsheet/embeddedform?formkey=dDZoLVVEdkdKQ1dGSXdqQk81RVktNFE6MQ";
		public static final String REGISTER_FOR_UPDATES_URL="https://docs.google.com/a/mobispectra.com/spreadsheet/embeddedform?formkey=dFVIWVpxVFM4bWZMSmQ1Q0dEeFpONUE6MQ";
	}
}
