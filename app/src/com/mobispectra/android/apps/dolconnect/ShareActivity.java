/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect;


import android.content.Intent;
import android.os.Bundle;

public class ShareActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_share);
		Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
		shareIntent.putExtra(Intent.EXTRA_SUBJECT,getResources().getString(R.string.share));
		shareIntent.putExtra(Intent.EXTRA_TEXT,String.format(getString(R.string.share_string)+ "https://market.android.com/details?id=com.mobispectra.android.apps.dolconnect"));
		shareIntent.setType("text/plain");
		startActivity(Intent.createChooser(shareIntent,
				getString(R.string.share_dolconnect)));
		
		finish();
	}
}

