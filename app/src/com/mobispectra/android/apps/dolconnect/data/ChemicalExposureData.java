/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobispectra.android.apps.dolconnect.data;

import java.util.ArrayList;

public class ChemicalExposureData {
	private ArrayList<ChemicalExposureItem> mChemicalExposureItemsList;

	public ChemicalExposureData() {
		super();
		mChemicalExposureItemsList = new ArrayList<ChemicalExposureItem>();
	}

	public ArrayList<ChemicalExposureItem> getData() {
		return mChemicalExposureItemsList;
	}

	public void addData(ChemicalExposureItem chemicalExposureItem){
		mChemicalExposureItemsList.add(chemicalExposureItem);
		
	}
	
	public class ChemicalExposureItem {
		String inspection_number;
		String establishment_name;
		String city;
		String state;
		String zip_code;
		String sic_code;
		String naics_code;
		String sampling_number;
		String office_id;
		String date_sampled;
		String date_reported;
		String instrument_type;
		String lab_number;
		String field_number;
		String sample_type;
		String blank_used;
		String time_sampled;
		String air_volume_sampled;
		String sample_weight;
		String imis_substance_code;
		String substance;
		String sample_result;
		String qualifier;
		String unit_of_measurement;
		String RowId;
		
		public ChemicalExposureItem(String inspectionNumber,
				String establishmentName, String city, String state,
				String zipCode, String sicCode, String naicsCode,
				String samplingNumber, String officeId, String dateSampled,
				String dateReported, String instrumentType, String labNumber,
				String fieldNumber, String sampleType, String blankUsed,
				String timeSampled, String airVolumeSampled,
				String sampleWeight, String imisSubstanceCode,
				String substance, String sampleResult, String qualifier,
				String unitOfMeasurement, String rowId) {
			super();
			inspection_number = inspectionNumber;
			establishment_name = establishmentName;
			this.city = city;
			this.state = state;
			zip_code = zipCode;
			sic_code = sicCode;
			naics_code = naicsCode;
			sampling_number = samplingNumber;
			office_id = officeId;
			date_sampled = dateSampled;
			date_reported = dateReported;
			instrument_type = instrumentType;
			lab_number = labNumber;
			field_number = fieldNumber;
			sample_type = sampleType;
			blank_used = blankUsed;
			time_sampled = timeSampled;
			air_volume_sampled = airVolumeSampled;
			sample_weight = sampleWeight;
			imis_substance_code = imisSubstanceCode;
			this.substance = substance;
			sample_result = sampleResult;
			this.qualifier = qualifier;
			unit_of_measurement = unitOfMeasurement;
			RowId = rowId;
		}

		/**
		 * @return the inspection_number
		 */
		public String getInspection_number() {
			return inspection_number;
		}

		/**
		 * @param inspectionNumber the inspection_number to set
		 */
		public void setInspection_number(String inspectionNumber) {
			inspection_number = inspectionNumber;
		}

		/**
		 * @return the establishment_name
		 */
		public String getEstablishment_name() {
			return establishment_name;
		}

		/**
		 * @param establishmentName the establishment_name to set
		 */
		public void setEstablishment_name(String establishmentName) {
			establishment_name = establishmentName;
		}

		/**
		 * @return the city
		 */
		public String getCity() {
			return city;
		}

		/**
		 * @param city the city to set
		 */
		public void setCity(String city) {
			this.city = city;
		}

		/**
		 * @return the state
		 */
		public String getState() {
			return state;
		}

		/**
		 * @param state the state to set
		 */
		public void setState(String state) {
			this.state = state;
		}

		/**
		 * @return the zip_code
		 */
		public String getZip_code() {
			return zip_code;
		}

		/**
		 * @param zipCode the zip_code to set
		 */
		public void setZip_code(String zipCode) {
			zip_code = zipCode;
		}

		/**
		 * @return the sic_code
		 */
		public String getSic_code() {
			return sic_code;
		}

		/**
		 * @param sicCode the sic_code to set
		 */
		public void setSic_code(String sicCode) {
			sic_code = sicCode;
		}

		/**
		 * @return the naics_code
		 */
		public String getNaics_code() {
			return naics_code;
		}

		/**
		 * @param naicsCode the naics_code to set
		 */
		public void setNaics_code(String naicsCode) {
			naics_code = naicsCode;
		}

		/**
		 * @return the sampling_number
		 */
		public String getSampling_number() {
			return sampling_number;
		}

		/**
		 * @param samplingNumber the sampling_number to set
		 */
		public void setSampling_number(String samplingNumber) {
			sampling_number = samplingNumber;
		}

		/**
		 * @return the office_id
		 */
		public String getOffice_id() {
			return office_id;
		}

		/**
		 * @param officeId the office_id to set
		 */
		public void setOffice_id(String officeId) {
			office_id = officeId;
		}

		/**
		 * @return the date_sampled
		 */
		public String getDate_sampled() {
			return date_sampled;
		}

		/**
		 * @param dateSampled the date_sampled to set
		 */
		public void setDate_sampled(String dateSampled) {
			date_sampled = dateSampled;
		}

		/**
		 * @return the date_reported
		 */
		public String getDate_reported() {
			return date_reported;
		}

		/**
		 * @param dateReported the date_reported to set
		 */
		public void setDate_reported(String dateReported) {
			date_reported = dateReported;
		}

		/**
		 * @return the instrument_type
		 */
		public String getInstrument_type() {
			return instrument_type;
		}

		/**
		 * @param instrumentType the instrument_type to set
		 */
		public void setInstrument_type(String instrumentType) {
			instrument_type = instrumentType;
		}

		/**
		 * @return the lab_number
		 */
		public String getLab_number() {
			return lab_number;
		}

		/**
		 * @param labNumber the lab_number to set
		 */
		public void setLab_number(String labNumber) {
			lab_number = labNumber;
		}

		/**
		 * @return the field_number
		 */
		public String getField_number() {
			return field_number;
		}

		/**
		 * @param fieldNumber the field_number to set
		 */
		public void setField_number(String fieldNumber) {
			field_number = fieldNumber;
		}

		/**
		 * @return the sample_type
		 */
		public String getSample_type() {
			return sample_type;
		}

		/**
		 * @param sampleType the sample_type to set
		 */
		public void setSample_type(String sampleType) {
			sample_type = sampleType;
		}

		/**
		 * @return the blank_used
		 */
		public String getBlank_used() {
			return blank_used;
		}

		/**
		 * @param blankUsed the blank_used to set
		 */
		public void setBlank_used(String blankUsed) {
			blank_used = blankUsed;
		}

		/**
		 * @return the time_sampled
		 */
		public String getTime_sampled() {
			return time_sampled;
		}

		/**
		 * @param timeSampled the time_sampled to set
		 */
		public void setTime_sampled(String timeSampled) {
			time_sampled = timeSampled;
		}

		/**
		 * @return the air_volume_sampled
		 */
		public String getAir_volume_sampled() {
			return air_volume_sampled;
		}

		/**
		 * @param airVolumeSampled the air_volume_sampled to set
		 */
		public void setAir_volume_sampled(String airVolumeSampled) {
			air_volume_sampled = airVolumeSampled;
		}

		/**
		 * @return the sample_weight
		 */
		public String getSample_weight() {
			return sample_weight;
		}

		/**
		 * @param sampleWeight the sample_weight to set
		 */
		public void setSample_weight(String sampleWeight) {
			sample_weight = sampleWeight;
		}

		/**
		 * @return the imis_substance_code
		 */
		public String getImis_substance_code() {
			return imis_substance_code;
		}

		/**
		 * @param imisSubstanceCode the imis_substance_code to set
		 */
		public void setImis_substance_code(String imisSubstanceCode) {
			imis_substance_code = imisSubstanceCode;
		}

		/**
		 * @return the substance
		 */
		public String getSubstance() {
			return substance;
		}

		/**
		 * @param substance the substance to set
		 */
		public void setSubstance(String substance) {
			this.substance = substance;
		}

		/**
		 * @return the sample_result
		 */
		public String getSample_result() {
			return sample_result;
		}

		/**
		 * @param sampleResult the sample_result to set
		 */
		public void setSample_result(String sampleResult) {
			sample_result = sampleResult;
		}

		/**
		 * @return the qualifier
		 */
		public String getQualifier() {
			return qualifier;
		}

		/**
		 * @param qualifier the qualifier to set
		 */
		public void setQualifier(String qualifier) {
			this.qualifier = qualifier;
		}

		/**
		 * @return the unit_of_measurement
		 */
		public String getUnit_of_measurement() {
			return unit_of_measurement;
		}

		/**
		 * @param unitOfMeasurement the unit_of_measurement to set
		 */
		public void setUnit_of_measurement(String unitOfMeasurement) {
			unit_of_measurement = unitOfMeasurement;
		}

		/**
		 * @return the rowId
		 */
		public String getRowId() {
			return RowId;
		}

		/**
		 * @param rowId the rowId to set
		 */
		public void setRowId(String rowId) {
			RowId = rowId;
		}

	}

}