/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.data;

import java.util.ArrayList;

public class FAQSubTopicData {
	private ArrayList<FAQSubTopicItem> mFAQSubTopicItemsList;

	public FAQSubTopicData() {
		super();
		mFAQSubTopicItemsList = new ArrayList<FAQSubTopicItem>();
	}

	public ArrayList<FAQSubTopicItem> getData() {
		return mFAQSubTopicItemsList;
	}

	public void addData(FAQSubTopicItem fAQSubTopicItem) {
		mFAQSubTopicItemsList.add(fAQSubTopicItem);

	}

	public class FAQSubTopicItem {
		String SubTopicID;
		String SubTopicValue;
		String TopicID;

		/**
		 * @param subTopicID
		 * @param subTopicValue
		 * @param topicID
		 */
		public FAQSubTopicItem(String subTopicID, String subTopicValue,
				String topicID) {
			super();
			SubTopicID = subTopicID;
			SubTopicValue = subTopicValue;
			TopicID = topicID;
		}

		/**
		 * @return the subTopicID
		 */
		public String getSubTopicID() {
			return SubTopicID;
		}

		/**
		 * @return the subTopicValue
		 */
		public String getSubTopicValue() {
			return SubTopicValue;
		}

		/**
		 * @return the topicID
		 */
		public String getTopicID() {
			return TopicID;
		}

		/**
		 * @param subTopicID
		 *            the subTopicID to set
		 */
		public void setSubTopicID(String subTopicID) {
			SubTopicID = subTopicID;
		}

		/**
		 * @param subTopicValue
		 *            the subTopicValue to set
		 */
		public void setSubTopicValue(String subTopicValue) {
			SubTopicValue = subTopicValue;
		}

		/**
		 * @param topicID
		 *            the topicID to set
		 */
		public void setTopicID(String topicID) {
			TopicID = topicID;
		}

	}

}
