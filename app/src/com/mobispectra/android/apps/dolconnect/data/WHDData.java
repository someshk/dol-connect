/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.data;

import java.util.ArrayList;

import com.google.android.maps.GeoPoint;

public class WHDData {
	private ArrayList<WHDItem> mWHDDataList;

	public WHDData() {
		super();
		mWHDDataList = new ArrayList<WHDItem>();
	}

	public ArrayList<WHDItem> getWHDData() {
		return mWHDDataList;
	}

	public void addWHDData(WHDItem whdItem){
		mWHDDataList.add(whdItem);
	}

	public class WHDItem {
		int index;
		String trade_nm;
		String street_addr_1_txt;
		String city_nm;
		String st_cd;
		String zip_cd;
		String naic_cd;
		String naics_code_description;
		String findings_start_date;
		String findings_end_date;
		String flsa_violtn_cnt;
		String flsa_repeat_violator;
		String flsa_bw_atp_amt;
		String flsa_ee_atp_cnt;
		String flsa_mw_bw_atp_amt;
		String flsa_ot_bw_atp_am;
		String flsa_15a3_bw_atp_amt;
		String flsa_cmp_assd_amt;
		String flsa_cl_violtn_cnt;
		String flsa_cl_minor_cnt;
		String flsa_cl_cmp_assd_amt;
		GeoPoint point;

		public WHDItem(int index,String tradeNm, String streetAddr_1Txt, String cityNm,
				String stCd, String zipCd, String naicCd,
				String naicsCodeDescription, String findingsStartDate,
				String findingsEndDate, String flsaVioltnCnt,
				String flsaRepeatViolator, String flsaBwAtpAmt,
				String flsaEeAtpCnt, String flsaMwBwAtpAmt,
				String flsaOtBwAtpAm, String flsa_15a3BwAtpAmt,
				String flsaCmpAssdAmt, String flsaClVioltnCnt,
				String flsaClMinorCnt, String flsaClCmpAssdAmt,GeoPoint point) {
			super();
			this.index = index;
			trade_nm = tradeNm;
			street_addr_1_txt = streetAddr_1Txt;
			city_nm = cityNm;
			st_cd = stCd;
			zip_cd = zipCd;
			naic_cd = naicCd;
			naics_code_description = naicsCodeDescription;
			findings_start_date = findingsStartDate;
			findings_end_date = findingsEndDate;
			flsa_violtn_cnt = flsaVioltnCnt;
			flsa_repeat_violator = flsaRepeatViolator;
			flsa_bw_atp_amt = flsaBwAtpAmt;
			flsa_ee_atp_cnt = flsaEeAtpCnt;
			flsa_mw_bw_atp_amt = flsaMwBwAtpAmt;
			flsa_ot_bw_atp_am = flsaOtBwAtpAm;
			flsa_15a3_bw_atp_amt = flsa_15a3BwAtpAmt;
			flsa_cmp_assd_amt = flsaCmpAssdAmt;
			flsa_cl_violtn_cnt = flsaClVioltnCnt;
			flsa_cl_minor_cnt = flsaClMinorCnt;
			flsa_cl_cmp_assd_amt = flsaClCmpAssdAmt;
			this.point = point;
		}

		public int getIndex(){
			return index;
		}
		
		public String getTrade_nm() {
			return trade_nm;
		}

		public void setTrade_nm(String tradeNm) {
			trade_nm = tradeNm;
		}

		public String getStreet_addr_1_txt() {
			return street_addr_1_txt;
		}

		public void setStreet_addr_1_txt(String streetAddr_1Txt) {
			street_addr_1_txt = streetAddr_1Txt;
		}

		public String getCity_nm() {
			return city_nm;
		}

		public void setCity_nm(String cityNm) {
			city_nm = cityNm;
		}

		public String getSt_cd() {
			return st_cd;
		}

		public void setSt_cd(String stCd) {
			st_cd = stCd;
		}

		public String getZip_cd() {
			return zip_cd;
		}

		public void setZip_cd(String zipCd) {
			zip_cd = zipCd;
		}

		public String getNaic_cd() {
			return naic_cd;
		}

		public void setNaic_cd(String naicCd) {
			naic_cd = naicCd;
		}

		public String getNaics_code_description() {
			return naics_code_description;
		}

		public void setNaics_code_description(String naicsCodeDescription) {
			naics_code_description = naicsCodeDescription;
		}

		public String getFindings_start_date() {
			return findings_start_date;
		}

		public void setFindings_start_date(String findingsStartDate) {
			findings_start_date = findingsStartDate;
		}

		public String getFindings_end_date() {
			return findings_end_date;
		}

		public void setFindings_end_date(String findingsEndDate) {
			findings_end_date = findingsEndDate;
		}

		public String getFlsa_violtn_cnt() {
			return flsa_violtn_cnt;
		}

		public void setFlsa_violtn_cnt(String flsaVioltnCnt) {
			flsa_violtn_cnt = flsaVioltnCnt;
		}

		public String getFlsa_repeat_violator() {
			return flsa_repeat_violator;
		}

		public void setFlsa_repeat_violator(String flsaRepeatViolator) {
			flsa_repeat_violator = flsaRepeatViolator;
		}

		public String getFlsa_bw_atp_amt() {
			return flsa_bw_atp_amt;
		}

		public void setFlsa_bw_atp_amt(String flsaBwAtpAmt) {
			flsa_bw_atp_amt = flsaBwAtpAmt;
		}

		public String getFlsa_ee_atp_cnt() {
			return flsa_ee_atp_cnt;
		}

		public void setFlsa_ee_atp_cnt(String flsaEeAtpCnt) {
			flsa_ee_atp_cnt = flsaEeAtpCnt;
		}

		public String getFlsa_mw_bw_atp_amt() {
			return flsa_mw_bw_atp_amt;
		}

		public void setFlsa_mw_bw_atp_amt(String flsaMwBwAtpAmt) {
			flsa_mw_bw_atp_amt = flsaMwBwAtpAmt;
		}

		public String getFlsa_ot_bw_atp_am() {
			return flsa_ot_bw_atp_am;
		}

		public void setFlsa_ot_bw_atp_am(String flsaOtBwAtpAm) {
			flsa_ot_bw_atp_am = flsaOtBwAtpAm;
		}

		public String getFlsa_15a3_bw_atp_amt() {
			return flsa_15a3_bw_atp_amt;
		}

		public void setFlsa_15a3_bw_atp_amt(String flsa_15a3BwAtpAmt) {
			flsa_15a3_bw_atp_amt = flsa_15a3BwAtpAmt;
		}

		public String getFlsa_cmp_assd_amt() {
			return flsa_cmp_assd_amt;
		}

		public void setFlsa_cmp_assd_amt(String flsaCmpAssdAmt) {
			flsa_cmp_assd_amt = flsaCmpAssdAmt;
		}

		public String getFlsa_cl_violtn_cnt() {
			return flsa_cl_violtn_cnt;
		}

		public void setFlsa_cl_violtn_cnt(String flsaClVioltnCnt) {
			flsa_cl_violtn_cnt = flsaClVioltnCnt;
		}

		public String getFlsa_cl_minor_cnt() {
			return flsa_cl_minor_cnt;
		}

		public void setFlsa_cl_minor_cnt(String flsaClMinorCnt) {
			flsa_cl_minor_cnt = flsaClMinorCnt;
		}

		public String getFlsa_cl_cmp_assd_amt() {
			return flsa_cl_cmp_assd_amt;
		}

		public void setFlsa_cl_cmp_assd_amt(String flsaClCmpAssdAmt) {
			flsa_cl_cmp_assd_amt = flsaClCmpAssdAmt;
		}

		public GeoPoint getPoint() {
			return point;
		}

		public void setPoint(GeoPoint point) {
			this.point = point;
		}
		
		

	}
}