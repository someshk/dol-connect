/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.data;

import java.util.ArrayList;

public class InjuryAndIllnessData {
	private ArrayList<InjuryAndIllnessItem> mInjuryAndIllnessItemsList;

	public InjuryAndIllnessData() {
		super();
		mInjuryAndIllnessItemsList = new ArrayList<InjuryAndIllnessItem>();
	}

	public ArrayList<InjuryAndIllnessItem> getData() {
		return mInjuryAndIllnessItemsList;
	}

	public void addData(InjuryAndIllnessItem injuryAndIllnessItem){
		mInjuryAndIllnessItemsList.add(injuryAndIllnessItem);
		
	}
	
	public class InjuryAndIllnessItem {
		String ROWID;
		String COMPANY;
		String NAME2;
		String STREET;
		String CITY;
		String SIC;
		String TCR;
		String DART;
		String DAFWII;
		/**
		 * @param rOWID
		 * @param cOMPANY
		 * @param nAME2
		 * @param sTREET
		 * @param cITY
		 * @param sIC
		 * @param tCR
		 * @param dART
		 * @param dAFWII
		 */
		public InjuryAndIllnessItem(String rOWID, String cOMPANY, String nAME2,
				String sTREET, String cITY, String sIC, String tCR,
				String dART, String dAFWII) {
			super();
			ROWID = rOWID;
			COMPANY = cOMPANY;
			NAME2 = nAME2;
			STREET = sTREET;
			CITY = cITY;
			SIC = sIC;
			TCR = tCR;
			DART = dART;
			DAFWII = dAFWII;
		}
		/**
		 * @return the rOWID
		 */
		public String getROWID() {
			return ROWID;
		}
		/**
		 * @return the cOMPANY
		 */
		public String getCOMPANY() {
			return COMPANY;
		}
		/**
		 * @return the nAME2
		 */
		public String getNAME2() {
			return NAME2;
		}
		/**
		 * @return the sTREET
		 */
		public String getSTREET() {
			return STREET;
		}
		/**
		 * @return the cITY
		 */
		public String getCITY() {
			return CITY;
		}
		/**
		 * @return the sIC
		 */
		public String getSIC() {
			return SIC;
		}
		/**
		 * @return the tCR
		 */
		public String getTCR() {
			return TCR;
		}
		/**
		 * @return the dART
		 */
		public String getDART() {
			return DART;
		}
		/**
		 * @return the dAFWII
		 */
		public String getDAFWII() {
			return DAFWII;
		}
		/**
		 * @param rOWID the rOWID to set
		 */
		public void setROWID(String rOWID) {
			ROWID = rOWID;
		}
		/**
		 * @param cOMPANY the cOMPANY to set
		 */
		public void setCOMPANY(String cOMPANY) {
			COMPANY = cOMPANY;
		}
		/**
		 * @param nAME2 the nAME2 to set
		 */
		public void setNAME2(String nAME2) {
			NAME2 = nAME2;
		}
		/**
		 * @param sTREET the sTREET to set
		 */
		public void setSTREET(String sTREET) {
			STREET = sTREET;
		}
		/**
		 * @param cITY the cITY to set
		 */
		public void setCITY(String cITY) {
			CITY = cITY;
		}
		/**
		 * @param sIC the sIC to set
		 */
		public void setSIC(String sIC) {
			SIC = sIC;
		}
		/**
		 * @param tCR the tCR to set
		 */
		public void setTCR(String tCR) {
			TCR = tCR;
		}
		/**
		 * @param dART the dART to set
		 */
		public void setDART(String dART) {
			DART = dART;
		}
		/**
		 * @param dAFWII the dAFWII to set
		 */
		public void setDAFWII(String dAFWII) {
			DAFWII = dAFWII;
		}
		
		
	}

}
