/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.data;

import java.io.InputStream;

import com.mobispectra.android.apps.utils.SAXParser;

import android.content.Context;
import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.util.Xml;

public class DOLTwitterFeedIfc {

	static String text;
	static String created_at;
	static String retweet_count;
	static String screen_name;
	static String imageUrl;
	static DOLTwitterFeedData twitterFeeds = null;

	public static DOLTwitterFeedData getTwitterFeeds(Context context) {

		String URI = "http://api.twitter.com/1/statuses/user_timeline.xml?screen_name=@USDOL";
		InputStream in = SAXParser.getInputStream(URI);
		if (in != null) {
			twitterFeeds = new DOLTwitterFeedData();
			RootElement root = new RootElement("statuses");
			Element status = root.getChild("status");

			status.setEndElementListener(new EndElementListener() {
				public void end() {
					twitterFeeds.addFeeds(text, created_at, retweet_count,
							screen_name, imageUrl);
				}
			});
			status.getChild("created_at").setEndTextElementListener(
					new EndTextElementListener() {
						public void end(String body) {
							created_at = body;
						}
					});
			status.getChild("text").setEndTextElementListener(
					new EndTextElementListener() {
						public void end(String body) {
							text = body;
						}
					});
			status.getChild("retweet_count").setEndTextElementListener(
					new EndTextElementListener() {
						public void end(String body) {
							retweet_count = body;
						}
					});
			status.getChild("user").getChild("screen_name")
					.setEndTextElementListener(new EndTextElementListener() {
						public void end(String body) {
							screen_name = body;
						}
					});
			status.getChild("user").getChild("profile_image_url")
					.setEndTextElementListener(new EndTextElementListener() {
						public void end(String body) {
							imageUrl = body;
						}
					});
			try {
				Xml.parse(in, Xml.Encoding.UTF_8, root.getContentHandler());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return twitterFeeds;
	}

}
