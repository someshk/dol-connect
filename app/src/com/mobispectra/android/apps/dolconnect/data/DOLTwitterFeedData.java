/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.data;

import java.util.ArrayList;

public class DOLTwitterFeedData {
	private ArrayList<DOLTwitterFeed>  mTwitterFeedList;
	private DOLTwitterFeed mTwitterFeed;
	
	public DOLTwitterFeedData(){
		mTwitterFeedList = new ArrayList<DOLTwitterFeed>();
	}
	
	public void addFeeds(String text, String created_at, String retweet_count,String screen_name, String imageUrl){
		mTwitterFeed = new DOLTwitterFeed(text,created_at,retweet_count,screen_name,imageUrl);
		mTwitterFeedList.add( mTwitterFeed);		
	}
	
	public ArrayList<DOLTwitterFeed> getTwitterFeeds() {
		return  mTwitterFeedList;
	}

	public class DOLTwitterFeed {

		String text;
		String created_at;
		String retweet_count;
		String screen_name;
		String imageUrl;

		public DOLTwitterFeed(String text, String created_at, String retweet_count,
				String screen_name, String imageUrl) {
			this.text = text;
			this.created_at = created_at;
			this.retweet_count = retweet_count;
			this.screen_name = screen_name;
			this.imageUrl = imageUrl;
		}
		
		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public String getCreated_at() {
			return created_at;
		}

		public void setCreated_at(String created_at) {
			this.created_at = created_at;
		}
		
		public String getRetweet_count() {
			return retweet_count;
		}

		public void setRetweet_count(String retweet_count) {
			this.retweet_count = retweet_count;
		}

		public String getScreen_name() {
			return screen_name;
		}

		public void setScreen_name(String screen_name) {
			this.screen_name = screen_name;
		}

		public String getImageUrl() {
			return imageUrl;
		}

		public void setImageUrl(String imageUrl) {
			this.imageUrl = imageUrl;
		}

	}

}
