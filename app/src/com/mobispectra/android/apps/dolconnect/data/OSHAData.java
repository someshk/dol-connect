/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.data;

import java.util.ArrayList;

import android.content.Context;

import com.google.android.maps.GeoPoint;
import com.mobispectra.android.apps.dolconnect.R;

public class OSHAData {
	
	private ArrayList<OSHAItem> mOSHAItemsList;

	public OSHAData() {
		super();
		mOSHAItemsList = new ArrayList<OSHAItem>();
	}

	public ArrayList<OSHAItem> getData() {
		return mOSHAItemsList;
	}
	
	public void addData(OSHAItem oshaItem){
		mOSHAItemsList.add(oshaItem);
		
	}
	
	public class OSHAItem {
		int index;
		String activity_nr;
		String name;
		String address;
		String city;
		String state;
		String zipCode;
		String naics_code;
		String insp_type;
		String open_date;
		String penalty;
		String osha_violation_indicator;
		String serious_violations;
		String total_violations;
		String load_dt;
		GeoPoint point;

		

		public OSHAItem(int index,String activityNr, String name, String address,
				String city, String state, String zipCode, String naicsCode,
				String inspType, String openDate, String penalty,
				String oshaViolationIndicator, String seriousViolations,
				String totalViolations, String loadDt, GeoPoint point) {
			super();
			this.index = index;
			activity_nr = activityNr;
			this.name = name;
			this.address = address;
			this.city = city;
			this.state = state;
			this.zipCode = zipCode;
			naics_code = naicsCode;
			insp_type = inspType;
			open_date = openDate;
			this.penalty = penalty;
			osha_violation_indicator = oshaViolationIndicator;
			serious_violations = seriousViolations;
			total_violations = totalViolations;
			load_dt = loadDt;
			this.point = point;
		}

		public int getIndex(){
			return index;
		}
		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the address
		 */
		public String getAddress() {
			return address;
		}

		/**
		 * @return the city
		 */
		public String getCity() {
			return city;
		}

		/**
		 * @return the state
		 */
		public String getState() {
			return state;
		}

		/**
		 * @return the zipCode
		 */
		public String getZipCode() {
			return zipCode;
		}

		/**
		 * @return the penalty
		 */
		public String getPenalty(Context context) {
			String none = context.getResources().getString(R.string.none);
			if (penalty == "null") {
				return none;
			} else {
				return penalty;
			}
		}

		/**
		 * @return the point
		 */
		public GeoPoint getPoint() {
			return point;
		}

		public String getActivity_nr(Context context) {
			String none = context.getResources().getString(R.string.none);
			if (activity_nr == "null") {
				return none;
			} else {
				return activity_nr;
			}
		}


		public String getNaics_code(Context context) {
			String none = context.getResources().getString(R.string.none);
			if (naics_code == "null") {
				return none;
			} else {
				return naics_code;
			}
		}


		public String getInsp_type(Context context) {
			String none = context.getResources().getString(R.string.none);
			if (insp_type == "null") {
				return none;
			} 
			if(insp_type.equals("A")) {
				insp_type = "Accident";
			}
			if(insp_type.equals("B")) {
				insp_type = "Complaint";
			}
			if(insp_type.equals("C")) {
				insp_type= "Referral";
			}
			if(insp_type.equals("D")) {
				insp_type = "Monitoring";
			}
			if(insp_type.equals("E")) {
				insp_type = "Variance";
			}
			if(insp_type.equals("F")) {
				insp_type = "FollowUp";
			}
			if(insp_type.equals("G")) {
				insp_type = "Unprog Rel";
			}
			if(insp_type.equals("H")) {
				insp_type = "Planned";
			}
			if(insp_type.equals("I")) {
				insp_type = "Prog Related";
			}
			if(insp_type.equals("J")) {
				insp_type = "Unprog Other";
			}
			if(insp_type.equals("k")) {
				insp_type = "Prog Other";
			}
			if(insp_type.equals("K")) {
				insp_type = "Other-L";
			}
			return insp_type;
				}
		


		public String getOpen_date(Context context) {
			String none = context.getResources().getString(R.string.none);
			if (open_date == "null") {
				return none;
			} else {
				return open_date;
			}
		}

		public String getOsha_violation_indicator(Context context) {
			String none = context.getResources().getString(R.string.none);
			if (osha_violation_indicator == "null") {
				return none;
			} else {
				return osha_violation_indicator;
			}
		}

		public String getSerious_violations(Context context) {
			String none = context.getResources().getString(R.string.none);
			if (serious_violations == "null") {
				return none;
			} else {
				return serious_violations;
			}
		}


		public String getTotal_violations(Context context) {
			String none = context.getResources().getString(R.string.none);
			if (total_violations == "null") {
				return none;
			} else {
				return total_violations;
			}
		}


		public String getLoad_dt(Context context) {
			String none = context.getResources().getString(R.string.none);
			if (load_dt == "null") {
				return none;
			} else {
				return load_dt;
			}
		}
	
		
	}

}
