/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.data;

import java.util.ArrayList;

public class FAQTopicQuestionsData {
	private ArrayList<FAQTopicQuestionsItem> mFAQTopicQuestionsItemsList;

	public FAQTopicQuestionsData() {
		super();
		mFAQTopicQuestionsItemsList = new ArrayList<FAQTopicQuestionsItem>();
	}

	public ArrayList<FAQTopicQuestionsItem> getData() {
		return mFAQTopicQuestionsItemsList;
	}

	public void addData(FAQTopicQuestionsItem fAQTopicQuestionsItem) {
		mFAQTopicQuestionsItemsList.add(fAQTopicQuestionsItem);

	}

	public class FAQTopicQuestionsItem {
		String FAQID;
		String TopicID;
		String SubTopicID;
		String Question;
		String Answer;
		String SourceURL;
		String Hits;
		String DateMod;
		String FAQSource;
		String Keywords;
		String LastReviewDate;

		/**
		 * @param fAQID
		 * @param topicID
		 * @param subTopicID
		 * @param question
		 * @param answer
		 * @param sourceURL
		 * @param hits
		 * @param dateMod
		 * @param fAQSource
		 * @param keywords
		 * @param lastReviewDate
		 */
		public FAQTopicQuestionsItem(String fAQID, String topicID,
				String subTopicID, String question, String answer,
				String sourceURL, String hits, String dateMod,
				String fAQSource, String keywords, String lastReviewDate) {
			super();
			FAQID = fAQID;
			TopicID = topicID;
			SubTopicID = subTopicID;
			Question = question;
			Answer = answer;
			SourceURL = sourceURL;
			Hits = hits;
			DateMod = dateMod;
			FAQSource = fAQSource;
			Keywords = keywords;
			LastReviewDate = lastReviewDate;
		}

		/**
		 * @return the fAQID
		 */
		public String getFAQID() {
			return FAQID;
		}

		/**
		 * @return the topicID
		 */
		public String getTopicID() {
			return TopicID;
		}

		/**
		 * @return the subTopicID
		 */
		public String getSubTopicID() {
			return SubTopicID;
		}

		/**
		 * @return the question
		 */
		public String getQuestion() {
			return Question;
		}

		/**
		 * @return the answer
		 */
		public String getAnswer() {
			return Answer;
		}

		/**
		 * @return the sourceURL
		 */
		public String getSourceURL() {
			return SourceURL;
		}

		/**
		 * @return the hits
		 */
		public String getHits() {
			return Hits;
		}

		/**
		 * @return the dateMod
		 */
		public String getDateMod() {
			return DateMod;
		}

		/**
		 * @return the fAQSource
		 */
		public String getFAQSource() {
			return FAQSource;
		}

		/**
		 * @return the keywords
		 */
		public String getKeywords() {
			return Keywords;
		}

		/**
		 * @return the lastReviewDate
		 */
		public String getLastReviewDate() {
			return LastReviewDate;
		}

		/**
		 * @param fAQID
		 *            the fAQID to set
		 */
		public void setFAQID(String fAQID) {
			FAQID = fAQID;
		}

		/**
		 * @param topicID
		 *            the topicID to set
		 */
		public void setTopicID(String topicID) {
			TopicID = topicID;
		}

		/**
		 * @param subTopicID
		 *            the subTopicID to set
		 */
		public void setSubTopicID(String subTopicID) {
			SubTopicID = subTopicID;
		}

		/**
		 * @param question
		 *            the question to set
		 */
		public void setQuestion(String question) {
			Question = question;
		}

		/**
		 * @param answer
		 *            the answer to set
		 */
		public void setAnswer(String answer) {
			Answer = answer;
		}

		/**
		 * @param sourceURL
		 *            the sourceURL to set
		 */
		public void setSourceURL(String sourceURL) {
			SourceURL = sourceURL;
		}

		/**
		 * @param hits
		 *            the hits to set
		 */
		public void setHits(String hits) {
			Hits = hits;
		}

		/**
		 * @param dateMod
		 *            the dateMod to set
		 */
		public void setDateMod(String dateMod) {
			DateMod = dateMod;
		}

		/**
		 * @param fAQSource
		 *            the fAQSource to set
		 */
		public void setFAQSource(String fAQSource) {
			FAQSource = fAQSource;
		}

		/**
		 * @param keywords
		 *            the keywords to set
		 */
		public void setKeywords(String keywords) {
			Keywords = keywords;
		}

		/**
		 * @param lastReviewDate
		 *            the lastReviewDate to set
		 */
		public void setLastReviewDate(String lastReviewDate) {
			LastReviewDate = lastReviewDate;
		}

	}

}
