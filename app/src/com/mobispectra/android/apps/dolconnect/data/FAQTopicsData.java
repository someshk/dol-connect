/*
 * Copyright 2011 Mobispectra Technologies LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobispectra.android.apps.dolconnect.data;

import java.util.ArrayList;

public class FAQTopicsData {
	private ArrayList<FAQTopicsItem> mFAQTopicsItemsList;

	public FAQTopicsData() {
		super();
		mFAQTopicsItemsList = new ArrayList<FAQTopicsItem>();
	}

	public ArrayList<FAQTopicsItem> getData() {
		return mFAQTopicsItemsList;
	}
	
	public void addData(FAQTopicsItem fAQTopicsItem){
		mFAQTopicsItemsList.add(fAQTopicsItem);
		
	}
	
	public class FAQTopicsItem {
		String TopicID;
		String TopicValue;
		/**
		 * @param topicID
		 * @param topicValue
		 */
		public FAQTopicsItem(String topicID, String topicValue) {
			super();
			TopicID = topicID;
			TopicValue = topicValue;
		}
		/**
		 * @return the topicID
		 */
		public String getTopicID() {
			return TopicID;
		}
		/**
		 * @return the topicValue
		 */
		public String getTopicValue() {
			return TopicValue;
		}
		/**
		 * @param topicID the topicID to set
		 */
		public void setTopicID(String topicID) {
			TopicID = topicID;
		}
		/**
		 * @param topicValue the topicValue to set
		 */
		public void setTopicValue(String topicValue) {
			TopicValue = topicValue;
		}

	}

}

